ALTER TABLE visitors ADD COLUMN IF NOT EXISTS activated BOOLEAN default TRUE;
ALTER TABLE visitors ADD COLUMN IF NOT EXISTS overleden DATE;

UPDATE service_dayinfo set showed_up = TRUE where id > 0 and service_date < CURDATE();


CREATE OR REPLACE VIEW view_lastdate_visit AS
	SELECT distinct 
		SI.ukgr_code, SI.member_id, MAX(SI.service_date) as last_visit 
	FROM service_dayinfo  SI
		INNER JOIN visitors V ON V.id = SI.member_id and V.activated = TRUE
		WHERE SI.member_id is not NULL AND SI.member_id > 0 AND SI.service_date < CURDATE() AND SI.showed_up = TRUE  GROUP BY SI.ukgr_code, SI.member_id
		ORDER BY SI.ukgr_code ASC, last_visit ASC


-- check with : SELECT * FROM view_lastdate_visit WHERE ukgr_code = 1 AND last_visit < DATE_ADD(CURDATE(), INTERVAL -1 MONTH)


CREATE OR REPLACE VIEW view_lastdate_visit AS
	SELECT 
			A.id as member_id, 
			coalesce(B.last_visit,DATE_ADD(curdate(), INTERVAL -90 DAY)) as last_visit,
			A.ukgr_code, 
			A.gsm, 
			A.birth_date, 
			A.first_name, 
			A.last_name, 
			A.gender,
			A.email,
			A.street,
            A.picture,
            A.extension,
			A.house_number,
			A.extra_address,
			A.zipcode,
			A.city
			
	FROM visitors A 
	LEFT JOIN 
	(
	SELECT distinct 
			SI.ukgr_code, SI.member_id, MAX(SI.service_date) as last_visit 
			FROM service_dayinfo  SI
			WHERE SI.member_id is not null and SI.member_id > 0 AND SI.service_date < CURDATE() AND SI.showed_up = TRUE  
			GROUP BY SI.ukgr_code, SI.member_id
			
	) B
	ON B.member_id = A.id AND B.ukgr_code = A.ukgr_code
	WHERE A.activated = TRUE
	ORDER BY A.ukgr_code ASC;


ALTER TABLE `service_dayinfo` CHANGE `rescode` `rescode` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;


CREATE OR REPLACE VIEW  view_feestdag_times AS
    SELECT 
        A.ukgr_code AS ukgr_code,
        C.id AS id,
        A.time_id AS time_id,
        C.start_time AS start_time,
        SUBSTR(C.start_time, 1, 5) AS display_time,
        DAYOFWEEK(B.datum) AS day_number,
        B.datum AS datum,
        B.beschrijving AS beschrijving
    FROM
        ((location_feast_times A
        JOIN feestdagen B ON (B.id = A.feestdag_id AND B.ACTIVE = TRUE))
        JOIN service_time C ON (C.id = A.time_id))
    ORDER BY A.ukgr_code , B.datum , C.id;

