-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: gebedenBoekInfo
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB


CREATE OR REPLACE VIEW view_occupied AS
    SELECT 
        A.ukgr_code AS ukgr_code,
        A.service_date AS service_date,
        A.day_number AS day_number,
        A.time_id AS time_id,
        A.current_count AS current_count,
        Z.capacity AS capacity,
        Z.capacity - A.current_count AS free_spaces
    FROM
        (((SELECT DISTINCT
            A.ukgr_code AS ukgr_code,
                A.service_date AS service_date,
                A.day_number AS day_number,
                A.time_id AS time_id,
                COUNT(0) AS current_count
        FROM
            gebedenBoekInfo.service_dayinfo A
        WHERE
            A.service_date >= CURDATE()
        GROUP BY A.ukgr_code , A.service_date , A.day_number , A.time_id)) A
        JOIN gebedenBoekInfo.location Z ON (Z.ukgr_code = A.ukgr_code));
        
-- ####################################################################

CREATE OR REPLACE VIEW  view_next_week AS
    SELECT 
        CURDATE() AS next_event,
        DAYOFWEEK(CURDATE()) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 1 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 1 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 2 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 2 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 3 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 3 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 4 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 4 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 5 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 5 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 6 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 6 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 7 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 7 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 8 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 8 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 9 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 9 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 10 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 10 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 11 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 11 DAY) AS day_number
    
    UNION SELECT 
        CURDATE() + INTERVAL 12 DAY AS next_event,
        DAYOFWEEK(CURDATE() + INTERVAL 12 DAY) AS day_number;
        
-- ####################################################################

CREATE OR REPLACE VIEW  view_next_event AS
    SELECT 
        K.ukgr_code AS ukgr_code,
        K.day_number AS day_number,
        K.time_id AS time_id,
        K.next_event AS next_event,
        K.start_time AS start_time,
        K.city AS city,
        K.address AS address,
        K.capacity AS capacity,
        IF(E.current_count IS NULL,
            0,
            E.current_count) AS current_count
    FROM
        (((SELECT 
            D.ukgr_code AS ukgr_code,
                D.day_number AS day_number,
                D.time_id AS time_id,
                C.next_event AS next_event,
                D.start_time AS start_time,
                D.city AS city,
                D.address AS address,
                D.capacity AS capacity
        FROM
            (((SELECT 
            Z.ukgr_code AS ukgr_code,
                Z.city AS city,
                Z.address AS address,
                Z.capacity AS capacity,
                V.time_id AS time_id,
                V.day_number AS day_number,
                W.start_time AS start_time
        FROM
            ((gebedenBoekInfo.location_times V
        JOIN gebedenBoekInfo.service_time W ON (W.id = V.time_id))
        JOIN gebedenBoekInfo.location Z ON (Z.ukgr_code = V.ukgr_code)))) D
        JOIN (SELECT 
            CURDATE() AS next_event,
                DAYOFWEEK(CURDATE()) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 1 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 1 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 2 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 2 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 3 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 3 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 4 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 4 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 5 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 5 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 6 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 6 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 7 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 7 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 8 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 8 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 9 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 9 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 10 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 10 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 11 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 11 DAY) AS day_number
         UNION SELECT 
            CURDATE() + INTERVAL 12 DAY AS next_event,
                DAYOFWEEK(CURDATE() + INTERVAL 12 DAY) AS day_number
        ) C ON (D.day_number = C.day_number)))) K
        LEFT JOIN (SELECT DISTINCT
            A.ukgr_code AS ukgr_code,
                A.service_date AS service_date,
                A.day_number AS day_number,
                A.time_id AS time_id,
                COUNT(0) AS current_count
        FROM
            gebedenBoekInfo.service_dayinfo A
        GROUP BY A.ukgr_code , A.service_date , A.day_number , A.time_id) E ON (E.ukgr_code = K.ukgr_code
            AND E.service_date = K.next_event
            AND E.day_number = K.day_number
            AND E.time_id = K.time_id))
    ORDER BY K.ukgr_code , K.day_number , K.time_id;   
    
-- ####################################################################

CREATE OR REPLACE VIEW  view_next_dates AS
    SELECT 
        1 AS day_number,
        'Zondag' AS day_name,
        CURDATE() + INTERVAL IF(6 >= WEEKDAY(CURDATE()),
            6 - WEEKDAY(CURDATE()),
            7 - WEEKDAY(CURDATE()) + 6) DAY AS next_event
    
    UNION SELECT 
        2 AS day_number,
        'Maandag' AS day_name,
        CURDATE() + INTERVAL IF(0 >= WEEKDAY(CURDATE()),
            0 - WEEKDAY(CURDATE()),
            7 - WEEKDAY(CURDATE()) + 0) DAY AS next_event
    
    UNION SELECT 
        3 AS day_number,
        'Dinsdag' AS day_name,
        CURDATE() + INTERVAL IF(1 >= WEEKDAY(CURDATE()),
            1 - WEEKDAY(CURDATE()),
            7 - WEEKDAY(CURDATE()) + 1) DAY AS next_event
    
    UNION SELECT 
        4 AS day_number,
        'Woensadg' AS day_name,
        CURDATE() + INTERVAL IF(2 >= WEEKDAY(CURDATE()),
            2 - WEEKDAY(CURDATE()),
            7 - WEEKDAY(CURDATE()) + 2) DAY AS next_event
    
    UNION SELECT 
        5 AS day_number,
        'Donderdag' AS day_name,
        CURDATE() + INTERVAL IF(3 >= WEEKDAY(CURDATE()),
            3 - WEEKDAY(CURDATE()),
            7 - WEEKDAY(CURDATE()) + 3) DAY AS next_event
    
    UNION SELECT 
        6 AS day_number,
        'Vrijdag' AS day_name,
        CURDATE() + INTERVAL IF(4 >= WEEKDAY(CURDATE()),
            4 - WEEKDAY(CURDATE()),
            7 - WEEKDAY(CURDATE()) + 4) DAY AS next_event
    
    UNION SELECT 
        7 AS day_number,
        'Zaterdag' AS day_name,
        CURDATE() + INTERVAL IF(5 >= WEEKDAY(CURDATE()),
            5 - WEEKDAY(CURDATE()),
            7 - WEEKDAY(CURDATE()) + 5) DAY AS next_event;
            
-- ####################################################################

CREATE OR REPLACE VIEW view_location_times AS
    SELECT 
        A.ukgr_code AS ukgr_code,
        A.day_number AS day_number,
        A.time_id AS time_id,
        C.start_time AS start_time,
        SUBSTR(C.start_time, 1, 5) AS display_time,
        B.short_name AS short_name,
        B.long_name AS long_name
    FROM
        ((location_times A
        JOIN view_day_names B ON (B.day_number = A.day_number))
        JOIN service_time C ON (C.id = A.time_id));  
        
            
-- ####################################################################

CREATE OR REPLACE VIEW view_location_next_service AS
    SELECT 
        A.ukgr_code AS ukgr_code,
        A.day_number AS day_number,
        A.time_id AS time_id,
        A.start_time AS start_time,
        A.dispay_time AS dispay_time,
        A.short_name AS short_name,
        A.long_name AS long_name
    FROM
        (SELECT 
            X.ukgr_code AS ukgr_code,
                X.day_number AS day_number,
                X.time_id AS time_id,
                Z.start_time AS start_time,
                SUBSTR(Z.start_time, 1, 5) AS dispay_time,
                Y.short_name AS short_name,
                Y.long_name AS long_name
        FROM
            ((gebedenBoekInfo.location_times X
        JOIN gebedenBoekInfo.view_day_names Y ON (Y.day_number = X.day_number))
        JOIN gebedenBoekInfo.service_time Z ON (Z.id = X.time_id))
        WHERE
            DAYOFWEEK(CURDATE()) = X.day_number
                AND (CURTIME() <= Z.start_time
                OR ADDTIME(CURTIME(), '-3000.9') <= Z.start_time)
        ORDER BY X.time_id) A
    ORDER BY A.ukgr_code , A.day_number , A.start_time;
    
    
-- ####################################################################

CREATE OR REPLACE VIEW view_location_extr_times AS
    SELECT 
        B.ukgr_code AS ukgr_code,
        B.day_number AS day_number,
        C.start_time AS min_start_time,
        D.start_time AS max_start_time
    FROM
        ((((SELECT DISTINCT
            A.ukgr_code AS ukgr_code,
                A.day_number AS day_number,
                MIN(A.time_id) AS min_time_id,
                MAX(A.time_id) AS max_time_id
        FROM
            gebedenBoekInfo.location_times A
        GROUP BY A.ukgr_code , A.day_number)) B
        LEFT JOIN gebedenBoekInfo.service_time C ON (C.id = B.min_time_id))
        LEFT JOIN gebedenBoekInfo.service_time D ON (D.id = B.max_time_id));


-- ####################################################################

CREATE OR REPLACE VIEW  view_feestdag_times AS
    SELECT 
        A.ukgr_code AS ukgr_code,
        C.id AS id,
        A.time_id AS time_id,
        C.start_time AS start_time,
        SUBSTR(C.start_time, 1, 5) AS display_time,
        DAYOFWEEK(B.datum) AS day_number,
        B.datum AS datum,
        B.beschrijving AS beschrijving
    FROM
        ((location_feast_times A
        JOIN feestdagen B ON (B.id = A.feestdag_id))
        JOIN service_time C ON (C.id = A.time_id))
    ORDER BY A.ukgr_code , B.datum , C.id;


-- ####################################################################

CREATE OR REPLACE VIEW view_day_names AS
    SELECT 
        1 AS day_number,
        'Zo' AS short_name,
        'Zondag' AS long_name
    
    UNION SELECT 
        2 AS day_number,
        'Ma' AS short_name,
        'Maandag' AS long_name
    
    UNION SELECT 
        3 AS day_number,
        'Di' AS short_name,
        'Dinsdag' AS long_name
    
    UNION SELECT 
        4 AS day_number,
        'Wo' AS short_name,
        'Woensdag' AS long_name
    
    UNION SELECT 
        5 AS day_number,
        'Do' AS short_name,
        'Donderdag' AS long_name
    
    UNION SELECT 
        6 AS day_number,
        'Vr' AS short_name,
        'Vrijdag' AS long_name
    
    UNION SELECT 
        7 AS day_number,
        'Za' AS short_name,
        'Zaterdag' AS long_name;
        
                  
    
