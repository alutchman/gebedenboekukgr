=======================================1===========================================================================
CREATE OR REPLACE VIEW view_day_names AS
	SELECT 	1 as day_number, 'Zo' as short_name, 'Zondag' as long_name
	UNION SELECT 	2 as day_number, 'Ma' as short_name, 'Maandag' as long_name
	UNION SELECT 	3 as day_number, 'Di' as short_name, 'Dinsdag' as long_name
	UNION SELECT 	4 as day_number, 'Wo' as short_name, 'Woensdag' as long_name
	UNION SELECT 	5 as day_number, 'Do' as short_name, 'Donderdag' as long_name
	UNION SELECT 	6 as day_number, 'Vr' as short_name, 'Vrijdag' as long_name
	UNION SELECT 	7 as day_number, 'Za' as short_name, 'Zaterdag' as long_name;
	
SET lc_time_names =  'nl_NL';
==================================================2================================================================
 
CREATE OR REPLACE VIEW view_next_dates AS
SELECT 
    1 AS 'day_number',    
    'Zondag'    AS 'day_name',
	DATE_ADD(curdate(), INTERVAL if( 6 >= WEEKDAY(curdate()) , (6 - WEEKDAY(curdate())) ,  (7 - WEEKDAY(curdate())) + 6) DAY)  AS 'next_event'
UNION
SELECT 
    2 AS 'day_number',    
    'Maandag'    AS 'day_name',
	DATE_ADD(curdate(), INTERVAL if( 0 >= WEEKDAY(curdate()) , (0 - WEEKDAY(curdate())) ,  (7 - WEEKDAY(curdate())) + 0) DAY)  AS 'next_event'
UNION
SELECT 
    3 AS 'day_number',    
    'Dinsdag'    AS 'day_name',
	DATE_ADD(curdate(), INTERVAL if( 1 >= WEEKDAY(curdate()) , (1 - WEEKDAY(curdate())) ,  (7 - WEEKDAY(curdate())) + 1) DAY)  AS 'next_event'
UNION
SELECT 
    4 AS 'day_number',    
    'Woensadg'    AS 'day_name',
	DATE_ADD(curdate(), INTERVAL if( 2 >= WEEKDAY(curdate()) , (2 - WEEKDAY(curdate())) ,  (7 - WEEKDAY(curdate())) + 2) DAY)  AS 'next_event'
UNION
SELECT 
    5 AS 'day_number',    
    'Donderdag'    AS 'day_name',
	DATE_ADD(curdate(), INTERVAL if( 3 >= WEEKDAY(curdate()) , (3 - WEEKDAY(curdate())) ,  (7 - WEEKDAY(curdate())) + 3) DAY)  AS 'next_event'	
UNION
SELECT 
    6 AS 'day_number',    
    'Vrijdag'    AS 'day_name',
	DATE_ADD(curdate(), INTERVAL if( 4 >= WEEKDAY(curdate()) , (4 - WEEKDAY(curdate())) ,  (7 - WEEKDAY(curdate())) + 4) DAY)  AS 'next_event'		
UNION
SELECT 
    7 AS 'day_number',    
    'Zaterdag'    AS 'day_name',
	DATE_ADD(curdate(), INTERVAL if( 5 >= WEEKDAY(curdate()) , (5 - WEEKDAY(curdate())) ,  (7 - WEEKDAY(curdate())) + 5) DAY)  AS 'next_event'		
 
=================================================3=================================================================  
CREATE OR REPLACE VIEW view_next_week AS
	SELECT 
		curdate()  AS 'next_event',
		DAYOFWEEK(curdate() )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 1 DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 1 DAY) )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 2  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 2 DAY) )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 3  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 3 DAY) )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 4  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 4 DAY) )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 5  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 5 DAY) )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 6  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 6 DAY) )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 7  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 7 DAY) )  AS day_number
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 8  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 8 DAY) )  AS day_number		
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 9  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 9 DAY) )  AS day_number	
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 10  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 10 DAY) )  AS day_number	
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 11  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 11 DAY) )  AS day_number	
	UNION
	SELECT 
		DATE_ADD(curdate(), INTERVAL 12  DAY)  AS 'next_event',
		DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 12 DAY) )  AS day_number			
		;


============================================4======================================================================

CREATE OR REPLACE VIEW view_next_event AS
	SELECT 
		K.*,
		IF(E.current_count IS NULL,0, E.current_count) as current_count  
	FROM 
		(
			SELECT D.ukgr_code, D.day_number, D.time_id,  C.next_event, D.start_time ,D.city, D.address,D.capacity
			FROM
				(
					SELECT 
						curdate()  AS 'next_event',
						DAYOFWEEK(curdate() )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 1 DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 1 DAY) )  AS day_number
					UNION	
					SELECT 
						DATE_ADD(curdate(), INTERVAL 2  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 2 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 3  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 3 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 4  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 4 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 5  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 5 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 6  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 6 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 7  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 7 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 8  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 8 DAY) )  AS day_number			
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 9  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 9 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 10  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 10 DAY) )  AS day_number
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 11  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 11 DAY) )  AS day_number		
					UNION
					SELECT 
						DATE_ADD(curdate(), INTERVAL 12  DAY)  AS 'next_event',
						DAYOFWEEK(DATE_ADD(curdate(), INTERVAL 12 DAY) )  AS day_number																		
  
				) C  						
			INNER JOIN		 
				(
				   SELECT 
						Z.ukgr_code, Z.city, Z.address,Z.capacity,V.time_id,V.day_number, W.start_time
				   FROM location_times V
				   INNER JOIN service_time W ON W.id = V.time_id
				   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code		   
				)  D					

				ON D.day_number= C.day_number 
		)	
		K
	LEFT JOIN
		(
			SELECT DISTINCT
			    A.ukgr_code,		
			    A.service_date, 
			    A.day_number,						
				A.time_id, 
				COUNT(*) AS current_count
			FROM service_dayinfo A
			GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id
		) E ON E.ukgr_code = K.ukgr_code AND E.service_date = K.next_event AND E.day_number = K.day_number AND  E.time_id = K.time_id

ORDER BY 
  K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC
  
  
====================================  

SELECT B.short_name, B.long_name,  A.*  FROM 	view_next_week A
INNER JOIN view_day_names B ON B.day_number = A.day_number
ORDER BY A.day_number, A.next_event ASC 
 

  
 
=================================================5=================================================================  
CREATE OR REPLACE VIEW view_location_times AS
SELECT 
	A.ukgr_code,
	A.day_number,
	A.time_id,
	C.start_time,
	SUBSTR(C.start_time, 1, 5) as display_time ,
	B.short_name, 
	B.long_name 
	FROM  location_times A
INNER JOIN view_day_names B ON B.day_number = A.day_number
INNER JOIN service_time C ON C.id = A.time_id


======================================================6============================================================
CREATE OR REPLACE VIEW view_location_extr_times AS
SELECT B.ukgr_code, B.day_number , C.start_time as min_start_time  , D.start_time as max_start_time FROM 
(
  SELECT 
    DISTINCT A.ukgr_code, A.day_number, MIN(A.time_id) as min_time_id ,  MAX(A.time_id) as max_time_id  
  FROM location_times A GROUP BY A.ukgr_code, A.day_number
) B
LEFT JOIN service_time C ON C.id=B.min_time_id
LEFT JOIN service_time D ON D.id=B.max_time_id;

==================================================================7===================================================================================

CREATE OR REPLACE VIEW view_location_next_service AS
SELECT A.* FROM 
(
		SELECT 
			X.ukgr_code,
			X.day_number,
			X.time_id,
			Z.start_time,
			SUBSTR(Z.start_time, 1, 5) as dispay_time ,
			Y.short_name, 
			Y.long_name 
			FROM  location_times X
		INNER JOIN view_day_names Y ON Y.day_number = X.day_number
		INNER JOIN service_time Z ON Z.id = X.time_id
		WHERE  DAYOFWEEK(curdate() ) = X.day_number AND (current_time() <= Z.start_time OR ADDTIME(current_time(), "-3000.9") <= Z.start_time) 
        ORDER BY X.time_id ASC
) A 
ORDER BY A.ukgr_code ASC, A.day_number ASC, A.start_time ASC;


==================================================================================================================

SELECT A.*,IF(B.member_id IS NULL,FALSE, TRUE) as preftime   FROM view_location_times A 
LEFT JOIN `member_preference` B 
   ON B.day_number=A.day_number
 AND B.time_id=A.time_id AND B.member_id=2
where A.ukgr_code=1
ORDER by day_number ASC, time_id ASC
====================================================================================================================


ALTER TABLE service_dayinfo ADD COLUMN time_id_member INT NULL;


============= 2020-12-12 ===========================================================================================
CREATE TABLE `feestdagen` (
  id int(11) NOT NULL AUTO_INCREMENT,
  datum date NOT NULL,
  beschrijving varchar(100) NOT NULL,
  active tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
) ;

INSERT INTO feestdagen (datum, beschrijving) VALUES 
	('2020-01-01','Nieuwjaarsdag'),
	('2020-12-25','1e Kerstdag'),
	('2020-12-26','2e Kerstdag'),	
	('2021-04-05','Paasmaandag'),
	('2021-05-13','Hemelvaart'),
	('2021-05-24','Pinkstermaandag');

CREATE TABLE `location_feast_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ukgr_code` int(11) NOT NULL, 
  `time_id` int(11) NOT NULL,
  `feestdag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

	
=============================================================================================================================
CREATE OR REPLACE VIEW view_feestdag_loc AS
	SELECT 
		B.datum, B.beschrijving, A.ukgr_code, A.day_number, A.time_id, A.start_time, A.display_time ,DAYOFWEEK(B.datum)  AS day_number_act, C.short_name, C.long_name 
	FROM feestdagen B
	INNER JOIN view_day_names C ON C.day_number = DAYOFWEEK(B.datum)
	INNER JOIN view_location_times A 
		ON (A.day_number = 1) 
	ORDER BY A.ukgr_code ASC, B.datum ASC, A.ukgr_code ASC, DAYOFWEEK(B.datum) ASC, A.day_number ASC,A.time_id ASC	
	
===============2020-12-13====================================================================================
CREATE OR REPLACE VIEW view_feestdag_times AS
	SELECT 
		A.ukgr_code,
		C.id,
		A.time_id, 
		C.start_time,
		SUBSTR(C.start_time, 1, 5) as display_time,
		DAYOFWEEK(B.datum )  AS day_number,
		B.datum, 
		B.beschrijving 
	FROM location_feast_times A
	INNER JOIN feestdagen B ON B.id = A.feestdag_id 
	INNER JOIN service_time C ON C.id = A.time_id
	ORDER BY A.ukgr_code ASC,B.datum ASC, C.id ASC

CREATE OR REPLACE VIEW view_occupied AS
    SELECT A.*, Z.capacity, (Z.capacity - A.current_count) AS free_spaces FROM
    (
		SELECT DISTINCT
			A.ukgr_code,		
			A.service_date, 
			A.day_number,						
			A.time_id, 
			COUNT(*) AS current_count
		FROM service_dayinfo A
		WHERE A.service_date >= current_date()
		GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id
	) A
	INNER JOIN location Z ON Z.ukgr_code = A.ukgr_code
