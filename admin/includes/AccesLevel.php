<?php


class AccesLevel {
	private $clearance;
	private $level;
	private $levelNumber = array('admin'=>0, 'pastors'=>1, 'viewer'=>2, 'extern'=>3);
	private $redirects   = array('admin'=>'users', 'pastors'=>'bezoekers', 'viewer'=>'bezoekers', 'extern'=>'rubriek');
	
	
	function __construct($level) {
		$this->level = $level;
		$this->clearance = array_key_exists($level, $this->levelNumber) ?  $this->levelNumber[$this->level] : $levelNumber['extern'];
	}
	
	function getLevel(){
		return $this->level;
	}
	
	
	function getClearance(){
		return $this->clearance;
	}
	
		
	function getRedirect(){
		return array_key_exists($this->level, $this->redirects) ?  $this->redirects[$this->level] : 'voorPagina';
	}
}
