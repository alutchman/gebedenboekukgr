<?php


class DbData {
	public $mapped = array();
	
	
	function __construct ($data){
		foreach($data as $key=>$value) {
			$newKey = lcfirst(str_replace('_', '', ucwords($key, '_')));
			$this->mapped[$newKey] = $value;
		}
	} 
	
	
	public function __toString()  {
        return implode(',', $this->keys);
    }  
    
    
    public function __get($key) {    
		if(property_exists($this, $key)) {
			$reflection = new ReflectionProperty($this, $key);
			$reflection->setAccessible($key);
			return $reflection->getValue($this);
		}		
		
		if (array_key_exists($key, $this->mapped)) {
			return $this->mapped[$key];
		}
		
        return "";
    }



	
	
}
