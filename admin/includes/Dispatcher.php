<?php

class Dispatcher {
	private $servicePart;
	
	function __construct($servicePart){
		global $GLOBALS;
		$this->servicePart = $servicePart;
	
		$root = $GLOBALS['root'];
	}
	
	function getHref($otherPart){
		global $GLOBALS;		
		
		$hrefArray = unserialize($_SESSION['BaseService']);
		
		if ($_SERVER['REQUEST_METHOD'] === 'GET'  && isset($hrefArray[$otherPart])) {
			if ($otherPart != 'aanwezig'  &&  $otherPart != 'bezoekers' &&  $otherPart != 'planning'   &&  
				$otherPart != 'clientCard' &&  $otherPart != 'attendance') {
				return $hrefArray[$otherPart];
			}
			/*
			 * Memory function only for edit person
			 */ 
			if ($otherPart == 'bezoekers' ) {				
				if (strpos($hrefArray[$otherPart], 'update') !== false ) {
					return $hrefArray[$otherPart];
				}				
			}
			if ($otherPart == 'clientCard' ) {				
				if (strpos($hrefArray[$otherPart], 'update') !== false ) {
					return $hrefArray[$otherPart];
				}		
				if (strpos($hrefArray[$otherPart], 'planning') !== false ) {
					return $hrefArray[$otherPart];
				}	
				if (strpos($hrefArray[$otherPart], 'times') !== false ) {
					return $hrefArray[$otherPart];
				}					
			}	
			
			if ($otherPart == 'attendance' ) {				
				if (strpos($hrefArray[$otherPart], 'update') !== false ) {
					return $hrefArray[$otherPart];
				}		
			}		
			
			return $GLOBALS['admin']."/$otherPart/show";
		} else {
			return $GLOBALS['admin']."/$otherPart/show";
		}
	}
	
	function isSelected($otherPart){
		return $otherPart === $this->servicePart;
	}
	
}
