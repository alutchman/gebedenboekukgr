<?php

class ExtendedError extends Error {
	public $message;
	public $file;
	public $line;	
	public $trace;
	
	function __construct(Error $org) {
		$this->message = $org->message;
		$this->trace   = explode("\n", $org->getTraceAsString());
		$file = $org->file;
		
		
		$pos = strpos($file, '/httpdocs' );
		
		$this->file = substr($file, $pos);
		$this->line = $org->line;
	}		

}
