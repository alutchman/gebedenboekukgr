<?php


class PlanNextWeekViP {
	protected $connection;
	
	function __construct(&$connection){		
		$this->connection = $connection;		
	}
	
	
	public function setupFuture($ukgrCode, $serviceDate, $dayNumber, $nextTimeId, $uurMin ) {
		$dataByPref = array();
		$query = "SELECT A.day_number,
					B.id, B.ukgr_code, B.gsm, B.gender,
					B.street, B.house_number, B.extra_address,B.zipcode, B.city,
					B.first_name, B.last_name, 
					SUBSTRING(K.start_time , 1, 5) as start_time,
					A.time_id
				FROM member_preference A
				INNER JOIN service_time K ON K.id = A.time_id	
				INNER JOIN visitors B ON B.id = A.member_id
					WHERE 
						A.day_number = $dayNumber 
						AND A.time_id <=  $nextTimeId  
						AND B.ukgr_code = $ukgrCode 
						AND B.id NOT IN (
							SELECT coalesce(D.member_id ,-1)
							 FROM `service_dayinfo` D 
							 WHERE 
								D.ukgr_code = $ukgrCode AND 
								D.service_date = '$serviceDate' AND 
								D.time_id_member = A.time_id 			
					 ) 
			ORDER BY K.start_time DESC, B.first_name ASC, B.last_name  ASC ";								
		$preferences    = 	$this->connection->fetchAssocRows($query);		
		
		
		if (count($preferences) > 0)  {
			echo "<pre>";
			echo "Planning $serviceDate (daynumber = $dayNumber, tijd = $uurMin) ukgrCode = $ukgrCode \n";
			echo("</pre>");
		}
		
		foreach ($preferences as $prefSchedule) {
			$PreftimeId = $prefSchedule['time_id'];
			$reserveId = $prefSchedule['id'];
			
			$free = $this->addPersonVip($ukgrCode, $serviceDate, $dayNumber, $nextTimeId,   $reserveId, $PreftimeId);	
			if ($free == 0 ) break;
		}	
	}
	
	public function addPersonVip($serviceDate, $dayNumber, $timeId,   $reserveId, $PreftimeId) {
		$free = 0;
		$personSearch = $this->connection->fetchAssocRows("SELECT id, ukgr_code, first_name, last_name, email, gsm  FROM visitors WHERE id = $reserveId ");
		if (count($personSearch) > 0) {
			$person = $personSearch[0];			
			$ukgrCode    = $person['ukgr_code'];
			$qry = $this->viewNextEventFixedDateAndUkgrCode($ukgrCode, $serviceDate);
			$timesQuery = "SELECT (A.capacity - A.current_count) AS free  
							FROM ($qry) A WHERE A.ukgr_code = %d AND A.next_event='%s' AND A.time_id = %d";
			$StatTimes = $this->connection->fetchAssocRows(sprintf($timesQuery, $ukgrCode , $serviceDate, $timeId ));			
			$free = intval($StatTimes[0]['free']);
			if ($free > 0) {
				$naam     = $person['first_name'].' '.$person['last_name'];
				$rescode  = sprintf('VIP%03dX%dY%d', $person['id'], $ukgrCode,$dayNumber  );
				$email    = $person['email'];
				$gsm      = $person['gsm'];
				$memberId = $person['id'];
				$query = "INSERT INTO service_dayinfo 
								(ukgr_code, day_number, service_date, time_id, naam, rescode, gsm, email, member_id, time_id_member) VALUES 
								($ukgrCode, $dayNumber, '$serviceDate',$timeId, '$naam', '$rescode', '$gsm', '$email', $memberId, $PreftimeId)";
				$this->connection->handleInsert($query );	
				$free--;
			}			
		}	
		return $free;
	}
	
	
	private function viewNextEventFixedDateAndUkgrCode($ukgrCode, $serviceDate){
		$resultFeast = $this->connection->fetchAssocRows("SELECT * FROM feestdagen WHERE datum = '$serviceDate'");
		$tabletimes = 'location_times';
		if (count($resultFeast) > 0) {
			$tabletimes = 'view_feestdag_times';
		}

		$query = "	SELECT 
						K.*,
						IF(E.current_count IS NULL,0, E.current_count) as current_count  
					FROM 
						(
							SELECT D.ukgr_code, D.day_number, D.time_id,  C.next_event, D.start_time ,D.city, D.address,D.capacity
							FROM 
								(
								   SELECT 
										Z.ukgr_code, Z.city, Z.address,Z.capacity,V.time_id,V.day_number, W.start_time
								   FROM $tabletimes  V
								   INNER JOIN service_time W ON W.id = V.time_id
								   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code		   
								)  D	
								INNER JOIN		
								(
									SELECT 	DATE('$serviceDate') AS next_event, 	DAYOFWEEK(DATE('$serviceDate' ))  AS day_number													
				  
								) C  			
								ON D.day_number= C.day_number 
								WHERE D.ukgr_code = $ukgrCode
						)	
						K
					LEFT JOIN
						(
							SELECT DISTINCT
								A.ukgr_code,		
								A.service_date, 
								A.day_number,						
								A.time_id, 
								COUNT(*) AS current_count
							FROM service_dayinfo A
							WHERE A.ukgr_code = $ukgrCode
							GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id
							
						) E ON E.ukgr_code = K.ukgr_code AND E.service_date = K.next_event AND E.day_number = K.day_number AND  E.time_id = K.time_id

				ORDER BY 
				  K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC";
	    return $query;				
	}
		
} 


