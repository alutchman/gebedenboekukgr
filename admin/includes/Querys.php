<?php

final class Querys  {
	const TIME_OPT  = 'SELECT 
				A.id, 
				SUBSTRING(A.start_time, 1, 5) AS display_time 
			FROM `service_time` A   
			ORDER BY A.start_time ASC
			';
	const PREF_LOC_MEMBERS =
	 'SELECT  DISTINCT 
			B.ukgr_code , 
			B.day_number,
			B.long_name, 
			B.short_name, 
			COUNT(*) as aantal, 
			GROUP_CONCAT(DISTINCT B.time_id ORDER BY B.time_id ASC) as times,
			GROUP_CONCAT(DISTINCT B.display_time ORDER BY B.time_id ASC) as showtime,
			GROUP_CONCAT(B.preftime ORDER BY B.time_id ASC) as prefered
	   FROM 
			(
			SELECT A.*,IF(B.member_id IS NULL,FALSE, TRUE) as preftime   FROM view_location_times A 
						LEFT JOIN `member_preference` B 
						   ON 	B.day_number=A.day_number
								AND B.time_id=A.time_id 
								AND B.member_id = %d
						where A.ukgr_code = %d
						ORDER by day_number ASC, time_id ASC
			) B		
			WHERE B.ukgr_code = %d
			GROUP BY  B.ukgr_code, B.day_number, B.long_name,B.short_name			
		'; 	

	const AVAILABLE_ATDATE = 'SELECT 
		  DISTINCT  A.ukgr_code, A.next_event,A.short_name,
			GROUP_CONCAT( SUBSTRING(A.start_time, 1, 5) ORDER BY time_id ASC) as times,
			GROUP_CONCAT( (A.capacity - A.current_count ) ORDER BY time_id ASC) as available  
		   FROM 
		   (
			   SELECT B.*, C.short_name FROM `view_next_event`  B
			   INNER JOIN `view_day_names` C ON C.day_number=B.day_number
		   ) A
		WHERE A.ukgr_code=%d
		GROUP BY  A.ukgr_code, A.next_event,A.short_name
		ORDER BY A.next_event ASC';

    const AGENDA_PERSON = 'SELECT 
			X.next_event, SUBSTRING(Y.start_time, 1, 5) as reserved, 
				(SELECT V.times FROM 
					(
						SELECT 
							DISTINCT 
							W.ukgr_code, 
							W.next_event,
							GROUP_CONCAT(SUBSTRING(W.start_time, 1,5) ORDER BY start_time ASC SEPARATOR ", ") as times					 
						FROM view_next_event W
						WHERE W.ukgr_code = %d  AND W.current_count < W.capacity
						GROUP BY W.ukgr_code, W.next_event
					
					) V WHERE V.next_event =  X.next_event 
				
				) as my_times
				FROM view_next_week X					
			LEFT JOIN (
				SELECT 
				B.start_time,A.service_date  FROM service_dayinfo A
				INNER JOIN service_time B ON B.id=A.time_id
				WHERE A.service_date IN (SELECT next_event FROM view_next_week) AND A.ukgr_code = %d
				  AND ( (A.member_id = %d AND  A.ukgr_code = %d) OR (A.naam = "%s" AND A.ukgr_code = %d))
			
			) Y
			ON X.next_event = Y.service_date
			WHERE X.day_number != 7
			ORDER BY X.next_event ASC
			';
			
	
}
