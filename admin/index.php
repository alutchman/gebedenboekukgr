<?php
	session_start();
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	mysqli_report(MYSQLI_REPORT_STRICT);
	$timeZone = 'Europe/Amsterdam';  // +1 hours
    date_default_timezone_set($timeZone); 
    setlocale(LC_ALL, 'nl_NL');
    setlocale(LC_TIME, 'nl_NL');
	
    
    $GLOBALS['mobile'] = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    $GLOBALS['jsMobile'] = $GLOBALS['mobile']  ? "true" :"false";	
    $GLOBALS['admin']  = dirname($_SERVER['PHP_SELF']);    
    $GLOBALS['root']   = $_SERVER['DOCUMENT_ROOT'].$GLOBALS['admin'].'/';
    $GLOBALS['htmlRoot']   = 'https://'.$_SERVER['SERVER_NAME'].$GLOBALS['admin'].'/'; 
   
    $servicePart      = 'location';
    $action           = 'show';


	$paths0 =  explode('?',substr($_SERVER['REQUEST_URI'],1+strlen( $GLOBALS['admin'] )));
    $paths1 =  explode('/',$paths0[0]);
	$extraPaths = array();
    

    if ( !isset( $_SESSION['user_id']) ||  !isset( $_SESSION['login_id']) 
		||  !isset( $_SESSION['level']) ||  !isset( $_SESSION['fullname']) )  {
		 $servicePart = 'login';
		 if (count($paths1) >= 2  ) {			
			$action = $paths1[1];		
			if ($action !== 'validate'  && $action !== 'show' ) {
				$action = "show";
			}	
		 }		 
	}  else {
		
		if (count($paths1) >= 2) {
			$servicePart = $paths1[0];			
			if ( strlen(trim($paths1[1])) > 0) {			
				$action = $paths1[1];
			}
		}	else {			
			if (count($paths1) == 1 && strlen(trim($paths1[0])) > 0) {						
				$servicePart =  trim($paths1[0]);
			}		
		}	
	}
	
	if (count($paths1) > 2) {
		$extraPaths = array_slice($paths1, 2); 
	}	
	
	$GLOBALS['paths']  = $extraPaths;
	$service = ucfirst($servicePart).'Service';
	
	if (!function_exists('getallheaders')) {
		function getallheaders() {
		$headers = [];
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
		}
    }	
    	
	try {
		function autoloadFunction($class) {
			$fileToRequire = null;
			if (preg_match('/Service$/', $class)) {
			    $fileToRequire =  $GLOBALS['root'] .'services/' . $class . ".php";				
			} else {
				$fileToRequire =  $GLOBALS['root'] .'includes/' . $class . ".php";				
			}
			if ($fileToRequire == null) {
				 throw new Exception("No Class to load");
			} else if (!file_exists($fileToRequire)) {
				 throw new Exception("File not found :" . substr($fileToRequire, 37));
			}
			require_once($fileToRequire);
		}
		
		spl_autoload_register("autoloadFunction");			
		
		
		$service = new $service($servicePart, $action);
		if ($service->redirecting === false)  {
			$service->$action();
		} else {		
			//ask service is allowed
			$dataRedir = $service->getSafeRedirect();
			$service->redirect($dataRedir, 'show');
			
		}	
	} catch (Exception $e) {
		$extendedError = new ExtendedException($e);
		$serviceX = new ErrorService($extendedError);
		
			
		$serviceX->show();		
	} catch (Error $error) {
        $extendedError = new ExtendedError($error);
        $errstr  = $extendedError->message;			
		$serviceX = new ErrorService($extendedError);	
		$serviceX->show();				
	}


