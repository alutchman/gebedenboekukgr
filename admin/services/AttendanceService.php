<?php

class AttendanceService  extends BaseService {
	
	public  function getLevel() {
		return 'pastors';
	}
	
	
	public function show(){
		$this->data['lastUkgrCode']  = isset($this->session['lastUkgrCode'])  ? $this->session['lastUkgrCode']  : 1;		
		$this->data['locations'] = 
		$this->connection->fetchAssocRows('SELECT * FROM location WHERE active = true ORDER BY ukgr_code ASC');
		
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}
	
	
	public function absent(){
		$ukgrCode      = $this->request['ukgr_code'];	
		$_SESSION['lastUkgrCode'] = $ukgrCode;
		$query = "
			SELECT * 
				FROM view_lastdate_visit 
			WHERE
			 ukgr_code = $ukgrCode  and last_visit <= DATE_ADD(curdate(), INTERVAL -90 DAY)
			
			ORDER BY last_visit ASC, first_name ASC, last_name ASC";

		$this->data['noshow'] = $this->connection->fetchAssocRows($query);

		$this->renderView(__FUNCTION__);	
	}
}
