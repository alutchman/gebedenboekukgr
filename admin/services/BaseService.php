<?php

abstract class BaseService {
	CONST     VIEW_ROOT = 'views';
	CONST     EMAIL_EXPR = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
	protected $pendingAction ;
	protected $post ;
	protected $errorMessage = "";
	protected $view ;
	protected $head ;   
	protected $request = array();
	protected $session = array();
	protected $data    = array();
	protected $basedir;
	protected $globals;
	protected $mobile;
	protected $options      = [ 'cost' => 12,];
	protected $jsMobile;
	protected $paths;	
	protected $currentMenu;
	protected $connection;
	protected $accesLevelUser;
	protected $accesLevelService;
	protected $levels = array('admin', 'pastors', 'viewer', 'extern'); 
	protected $registered = array(
				'users' 		=> array('level'=>'admin'   , 'label'=>'Gebruikers'       , 'image'=>'keyAdmin.png'), 
				'location' 		=> array('level'=>'pastors' , 'label'=>'Locaties'         , 'image'=>'location_gear.png'), 	
				'feestdag' 		=> array('level'=>'pastors' , 'label'=>'Feestdagen'        , 'image'=>'christmas_star_icon.png'), 	
				'bezoekers'		=> array('level'=>'extern'  , 'label'=>'Bezoekers'        , 'image'=>'clients.png'), 
				'planning' 		=> array('level'=>'viewer'  , 'label'=>'Planning'         , 'image'=>'calendar_today.png'), 	
				'clientCard'	=> array('level'=>'pastors' , 'label'=>'Bezoekers Info'   , 'image'=>'usercard_01.png'), 	
				'attendance'    => array('level'=>'pastors' , 'label'=>'Absentie Info'    , 'image'=>'absent.png'), 	
				'voorPagina' 	=> array('level'=>'extern'  , 'label'=>'Voor Pagina'      , 'image'=>'page_white_edit.png'),
				'rubriek' 		=> array('level'=>'extern'  , 'label'=>'Rubriek'          , 'image'=>'documents_settings_folder.png'), 
				);
	
	public    $redirecting = false;
	
	public abstract function getLevel();
	
	
	public function show() {
		$this->renderView(__FUNCTION__, 'baseTemplate');		
	}	

	
	function __construct($servicePart, $pendingAction = 'show'){
		global $GLOBALS;
		$this->pendingAction = $pendingAction;
		$this->globals = $GLOBALS;		
		$this->paths = $this->globals['paths'];
		
		$this->basedir = $servicePart;
		$this->request = isset($_REQUEST) ? $_REQUEST : array();  
		$this->session = isset($_SESSION) ? $_SESSION : array();
		$this->post    = isset($_POST)    ? $_POST    : array();
		$this->head    = getallheaders();
		
		if (isset($this->registered[$servicePart])) {
			$this->currentMenu = $this->registered[$servicePart];	
		}		
		
		if (isset($this->session['level'])) {
			$this->accesLevelUser = new AccesLevel($this->session['level'] );
		}
		
		$this->mobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
		$this->jsMobile = $this->mobile ? "true" :"false";	 
		
		if ($servicePart != 'error') {
			$this->connection = new Connection();		

			$this->accesLevelService = new AccesLevel($this->getLevel());
			
			if ($servicePart != 'login'  &&   $this->accesLevelUser->getClearance() > $this->accesLevelService->getClearance()){
				$this->redirecting = true;
			}

		}
		
		$hrefArray = array();
		if (isset($_SESSION['BaseService'])) {
			$hrefArray = unserialize($_SESSION['BaseService']);
		}
		
		if ($_SERVER['REQUEST_METHOD'] === 'GET') {
			$hrefArray[$servicePart] = $_SERVER['REQUEST_URI'];				
		} else {
		  unset($hrefArray[$servicePart]);	
		}	
		
		$_SESSION['BaseService'] = serialize($hrefArray);			
	}
	
	public function getConnection(){
		return $this->connection;
	}
	
	public function getSafeRedirect(){
		return  $this->accesLevelUser->getRedirect();
	}
	
	public function getServiceClearance() {
		return $this->accesLevelService->getClearance();
	}
	
	public function getUserClearance() {
		if ($this->accesLevelUser != null) {
			return $this->accesLevelUser->getClearance();
		} else {
			return 100;
		}		
	}
	
	
	public function canAccess(){
		return $this->getUserClearance() <= $this->getServiceClearance();	
	}	
	
	 
		
	
	public function renderView($view, $template = null) {		
		$viewBase = $this->globals['root'].self::VIEW_ROOT;
		$pathToView = $this->file_build_path($viewBase,$this->basedir)."/$view.phtml";
		
		if (!file_exists($pathToView )) {
			$pathToView = $this->file_build_path($viewBase,"mijnUkgr")."/$view.phtml";
		}
		
		header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');		
		
		$this->data['referer']	   = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "" ;
		extract($this->data);
		
		if ($template != null) {
			$pathToTemplate = $this->file_build_path($viewBase)."/$template.phtml";
			include($pathToTemplate);	
		} else {
			include($pathToView);		
		}			
	}
	
	public function includeScript($phpScript) {
		return $this->globals['root'].$phpScript;
	}
	
	public function printAction($action ='show', $extraPaths =''){	
		echo($this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.DIRECTORY_SEPARATOR.$extraPaths);
	}
	
		
	public function getActionURL($action = 'show', $extraPaths =''){	
		return $this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.DIRECTORY_SEPARATOR.$extraPaths;
	}
	
	public function printActionWithPaths($action, $removeLast = false) {
		if ($removeLast) {
			$orgPaths = $this->paths;
			array_pop($orgPaths);
			$postPaths = implode(DIRECTORY_SEPARATOR, $orgPaths );
			echo($this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.DIRECTORY_SEPARATOR.$postPaths);			
		} else {
			$postPaths = implode(DIRECTORY_SEPARATOR, $this->paths);
			echo($this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.DIRECTORY_SEPARATOR.$postPaths);			
		}

	}
	
	public function getHrefServiceAction($service, $action = 'show', $postPaths = '') {
		return $newUrl = $this->globals['admin'].DIRECTORY_SEPARATOR.$service.DIRECTORY_SEPARATOR.$action.DIRECTORY_SEPARATOR.$postPaths;
	}
	
	public function getNextAction($action, $postUrl = '/') {
		return $this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.$postUrl;
	}
	
	public function redirectWithPaths($service, $action, $removeLast = false){
		if ($removeLast) {
			$orgPaths = $this->paths;
			array_pop($orgPaths);
			$postPaths = implode(DIRECTORY_SEPARATOR, $orgPaths );
			$newUrl =  $this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.DIRECTORY_SEPARATOR.$postPaths;	
			header("Location: $newUrl");
		} else {		
			$postPaths = implode(DIRECTORY_SEPARATOR, $this->paths);
			$newUrl = $this->globals['admin'].DIRECTORY_SEPARATOR.$service.DIRECTORY_SEPARATOR.$action.DIRECTORY_SEPARATOR.$postPaths;
			header("Location: $newUrl");		
		}
	}
	
	
	public function redirect($service, $action = 'show', $postParams = '/'){
		$newUrl = $this->globals['admin'].DIRECTORY_SEPARATOR.$service.DIRECTORY_SEPARATOR.$action.$postParams;
		header("Location: $newUrl");
	}
	
	public function redirectLocal($action = 'show', $postParams = '/'){
		$newUrl = $this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.$postParams;
		header("Location: $newUrl");
	}
	
	private function file_build_path(...$segments) {
		return join(DIRECTORY_SEPARATOR, $segments);
	}
	
		
	public function printImageSrc($src){	
		echo($this->globals['admin'].DIRECTORY_SEPARATOR.'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$src);
	}
	
	public function fetchImageSrc($src){	
		return $this->globals['admin'].DIRECTORY_SEPARATOR.'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$src;
	}
	
	
	
	protected function fetchChurchLocations($searchUkgrCode = null){
		$where = "";
		if ($searchUkgrCode != null) {
			$where = "WHERE ukgr_code = $searchUkgrCode";
		}
		$this->data['locations'] = $this->connection->fetchAssocRows("SELECT * FROM location $where ORDER BY ukgr_code ASC");
	}
	
	protected function processPicture($fileObject){
		$filenaam = basename($fileObject["name"]);
		$ext      = pathinfo($fileObject["name"], PATHINFO_EXTENSION);
		
		if ($fileObject['error'] !== UPLOAD_ERR_OK) { 				
			$imageResponse = array();
			$imageResponse['data'] = null;
			$imageResponse['size'] = 0;
			$imageResponse['name'] = $filenaam;
			$imageResponse['ext']  = $ext;		
			$imageResponse['error']  = $this->codeToMessage($fileObject['error']);				
			return $imageResponse; 
		}
        
		if (intval($fileObject["size"]) > (1024*1024*10)) {
			$this->errorMessage =  "Uw plaatje vereist te veel geheugen: " . $fileObject["size"];
		    return false; 
		}		
		
		if ($ext !== 'gif' && $ext !== 'png' && $ext !== 'jpg' && $ext !== 'jpeg') {
			$this->errorMessage = "File met extensie $ext geupload (jpg,png of gif verwacht)!";
			return false; 
		}
		
		$fileData    = file_get_contents($fileObject['tmp_name']);
		$fileEncoded = base64_encode($fileData);
		
		$imageResponse = array();
		$imageResponse['data'] = $fileEncoded;
		$imageResponse['size'] = intval($fileObject["size"]);
		$imageResponse['name'] = $filenaam;
		$imageResponse['ext']  = $ext;
		
		//cleanup
		unlink($fileObject['tmp_name']);
		return 	$imageResponse ;
	}	
	
	public function clientFetchImageByPersonId(){
		header('Content-Type: application/json');
		$data2Use  = null;
		$extension = 'png';
		
		$personId = $this->request['personId'];
		$query = sprintf("SELECT * FROM visitors WHERE id = %d", $personId); 
		$userDataList = $this->connection->fetchAssocRows($query );
		if (count($userDataList) == 1) {
			$userRecord = $userDataList[0];	
			if ($userRecord['picture']  != null && $userRecord['extension'] != null)  {		
				$data2Use = $userRecord['picture'];
				$extension = $userRecord['extension'];
			} else {
				$data2UseRAW = file_get_contents($this->globals['root'].'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'basePhoto.png');
				$data2Use = base64_encode($data2UseRAW);				
			}			
		}
		
		if (strtolower($extension) == 'jpg')  {
			$extension = 'jpeg';
		}
		
		$data =  $data2Use  != null ? "data:image/$extension;base64,$data2Use"  : null;			
		$response = array('searchID'=>$this->request['searchID'],'extension'=>$extension,'data'=>$data);
	    
		echo json_encode($response);
	}	
		
	protected function codeToMessage($code)  {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "File upload stopped by extension";
                break;

            default:
                $message = "Unknown upload error";
                break;
        }
        return $message;
    }	
    
    protected function fetchDetailedData($queryRAW, $params = array()) {		
		return $this->connection->fetchDetailedData($queryRAW, $params);	
	}
	
	protected function printIdImageDummy(){
		$extension = 'png';
		$data2UseRAW = file_get_contents($this->globals['root'].'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'basePhoto.png');
		$data2Use = base64_encode($data2UseRAW);	
		
		echo "data:image/$extension;base64,$data2Use" ;
	}
	
	
	protected function sendMail($emailFrom, $fromName, $emailTo, $toName, $subject,  $message){
		$mail = new PHPMailer();			
		
		$mail->IsSMTP();                       // telling the class to use SMTP

		$mail->SMTPDebug = 0;                  
		// 0 = no output, 1 = errors and messages, 2 = messages only.

		$mail->SMTPAuth = true;                // enable SMTP authentication
		$mail->SMTPSecure = "tls";              // sets the prefix to the servier
		$mail->Host = $this->globals['smtpHost'] ;               // sets Gmail as the SMTP server
		$mail->Port = 587;                     // set the SMTP port for the GMAIL

		$mail->Username = $this->globals['smtpUser']; 
		$mail->Password = $this->globals['smtpPassword'];     

		$mail->CharSet = 'windows-1250';
		$mail->SetFrom ($emailFrom, $fromName);
		$mail->Subject = $subject;
		$mail->IsHTML(true);

		$mail->Body = $message ; 
		// you may also use $mail->Body = file_get_contents('your_mail_template.html');

		$mail->AddAddress ($emailTo, $toName);     
		// you may also use this format $mail->AddAddress ($recipient);
		
		return $mail->Send();		
		
	}
	
	protected function getStream($view, $template = null){		
		ob_start(); // begin collecting output

        $viewBase = $this->globals['root'].self::VIEW_ROOT;
		$pathToView = $this->file_build_path($viewBase,$this->basedir)."/$view.phtml";
		
		if (!file_exists($pathToView )) {
			$pathToView = $this->file_build_path($viewBase,"admin")."/$view.phtml";
		}	
	
		extract($this->data);
		
		if ($template != null) {
			$pathToTemplate = $this->file_build_path($viewBase)."/$template.phtml";
			include($pathToTemplate);	
		} else {
			include($pathToView);		
		}			

		$result = ob_get_clean();
		
		return $result;
	}
	
	/**
	 * $message = $this->getStream('emailref', 'email');
	 */ 
	protected function mailExample($emailTo, $toName, $subject, $message){		
		$message = $this->getStream('emailref', 'email');			
				
		$emailFrom = 'info@ukgr.nl';
		$fromName = 'Beheer Ukgr';
		
		$mailResult = $this->sendMail($emailFrom, $fromName, $emailTo, $toName, $subject,  $message);
		return $mailResult;
	}
	
		
}
