<?php

class BezoekersService  extends BaseService {
	public function getLevel() {
		return 'extern';
	}
	
	public function handleAdd(){
		$accesLevelItem = new AccesLevel('viewer');
		if ($this->accesLevelUser->getClearance() > $accesLevelItem->getClearance()){
			return false;
		}
		if ( isset($this->post['ukgr_code'])  && isset($this->post['day_number'])  && isset($this->post['event_date'])  && 
			isset($this->post['time_id'])  && isset($this->post['naam'])  && isset($this->post['gsm'])  && 
			isset($this->post['email'])  && isset($this->session['digshow'])   ) {
			
			$ukgrCode        = $this->post['ukgr_code'];
			$dayNumber       = $this->post['day_number'];
			$service_date    = $this->post['event_date'];
			$tijdstip        = intval($this->post['time_id']);
			$naam            = trim($this->post['naam']);
			$code            = $this->session['digshow'];		
			$currentEmail    = trim($this->post['email']);
			$telefoonNummer  = $this->post['gsm'];			
			$timeSlotCheck = $this->checkBeforeADD($ukgrCode, $service_date , $tijdstip);
			if (!$timeSlotCheck->full) {
				if (strlen($currentEmail) == 0) {
						$query = "INSERT INTO service_dayinfo (ukgr_code, day_number, service_date, time_id, naam, rescode, gsm)  
								 values ($ukgrCode , $dayNumber, '$service_date', $tijdstip, '$naam', '$code', '$telefoonNummer' )";
						$this->connection->handleInsert($query );					
					return true;					
				} else {
					$query = "INSERT INTO service_dayinfo (ukgr_code, day_number, service_date, time_id, naam, rescode, email, gsm)  
							 values ($ukgrCode, $dayNumber , '$service_date', $tijdstip, '$naam', '$code', '$currentEmail', '$telefoonNummer' )";	
					$this->connection->handleInsert($query );	
					return true;	
				}		
			}		
		}
		return false;
	}
	
			
	public function show(){
		$this->handleAdd();
		
		$this->data['lastUkgrCode']  = isset($this->session['lastUkgrCode'])  ? $this->session['lastUkgrCode']  : 1;
		$this->data['lastTimeId']    = isset($this->session['lastTimeId'])    ? $this->session['lastTimeId']    : 0;
		$this->data['lastEventDate'] = isset($this->session['lastEventDate']) ? $this->session['lastEventDate'] : date("Y-m-d");
		$this->data['lastDayNumber'] = isset($this->session['lastDayNumber']) ? $this->session['lastDayNumber'] : 1;

		if (isset($this->session['lastEventDate'])) {
			$serviceDate = $this->session['lastEventDate'];
			$timeToUse   = "12:00";
			$dateSrc  = $serviceDate ." $timeToUse CET";
			$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
			$this->data['lastDisplayDate'] = date_format($dateTime, 'd-m-Y');				
		} else {
			$this->data['lastDisplayDate'] = date("d-m-Y");
		}
		
		
		$this->data['locations'] = $this->connection->fetchAssocRows('SELECT * FROM location WHERE active = true ORDER BY ukgr_code ASC');
		$this->data['services']  = $this->connection->fetchAssocRows('SELECT * FROM `view_next_dates` ORDER BY next_event ASC');
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	
	public function dienst(){
		if (!isset($this->session['lastUkgrCode'])) {
			$this->redirect('bezoekers');
		}
		$ukgrCode = $this->session['lastUkgrCode'];
		

		$upcomming = $this->connection->fetchAssocRows("SELECT * FROM view_feestdag_times WHERE ukgr_code = $ukgrCode AND   
										datum = CURDATE() AND ADDTIME(start_time,'00:30:00') > curtime()  LIMIT 1");		
		
		if (count($upcomming) == 0)  {
			$upcomming = $this->connection->fetchAssocRows("SELECT * FROM view_location_next_service WHERE ukgr_code = $ukgrCode LIMIT 1");		
		}		

		if (count($upcomming) == 1)  {
			$_SESSION['lastTimeId'] = $upcomming[0]['time_id'];
			$_SESSION['lastEventDate'] = date("Y-m-d");
		} else {
			$dayNumberFind = $this->connection->fetchAssocRows("SELECT DAYOFWEEK(curdate() ) AS day_number");
			$dayNumber = intval($dayNumberFind[0]['day_number']);
			/*
			 * On Saturday we show sunday with the first service selected
			 */ 
			if ($dayNumber  == 7) {
				$dateFinder = $this->connection->fetchAssocRows("SELECT DATE_ADD(CURDATE(),INTERVAL 1 DAY) AS nextDay");
				$_SESSION['lastEventDate'] = $dateFinder[0]['nextDay'];				
				$query = "SELECT * FROM `view_location_times` WHERE ukgr_code=$ukgrCode AND day_number = 1 ORDER BY time_id  ASC LIMIT 1";
				$timeFinder = $this->connection->fetchAssocRows($query);
				$_SESSION['lastTimeId'] = $timeFinder[0]['time_id'];
			}
		}
		$this->redirect('bezoekers');
	}
	
		
	public function printMe(){		
		$ukgrCode  = $this->globals['paths'][0];
		$dayNumber = $this->globals['paths'][2];
		$eventDate = $this->globals['paths'][1];
		$timeId    = $this->globals['paths'][3];
		$query = "SELECT A.* FROM `service_dayinfo` A 
					WHERE A.ukgr_code = $ukgrCode AND A.service_date = '$eventDate' AND A.time_id = $timeId ORDER BY naam ASC";
		$this->data['bezoekers'] = $this->connection->fetchAssocRows($query);				
		$locations = $this->connection->fetchAssocRows("SELECT * FROM location WHERE ukgr_code = $ukgrCode");
		$times = $this->connection->fetchAssocRows("SELECT * FROM service_time WHERE id = $timeId");
		
		$dateSrc  = $eventDate .' 12:00 CET';
		$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
		$this->data['event_data']  = date_format($dateTime, 'd-m-yy');	
		$this->data['location']    = $locations[0];
		$this->data['displayTime'] = substr($times[0]['start_time'], 0, 5);		
		
		$query = "SELECT * FROM `location` 	WHERE ukgr_code = $ukgrCode";
		$locResult = $this->connection->fetchAssocRows($query);
		
		$query = "SELECT count(*) as aantal FROM `service_dayinfo` 
					WHERE ukgr_code=$ukgrCode AND service_date='$eventDate' and time_id=$timeId
					GROUP BY ukgr_code, service_date, time_id";
					
		$aantal = 0;			
        $lResult = $this->connection->fetchAssocRows($query);
        if (count($lResult) == 1) {
			$aantal  = intval($lResult[0]['aantal']);
		}
		$this->data['request'] = array('bezet' =>  $aantal, 'max' => intval($locResult[0]['capacity']) );
		
		$this->renderView(__FUNCTION__, 'printTemplate');
	}	
	
	
	public function times(){
		$ukgrCode = $this->request['ukgr_code'];
		$dayNumber = $this->request['day_number'];	
		$event_date = $this->request['event_date'];	
		$_SESSION['lastUkgrCode'] = $ukgrCode;		
		$_SESSION['lastDayNumber'] = $dayNumber;
		
		$this->data['times'] = array();
		$query0 = "SELECT * FROM feestdagen WHERE datum = '$event_date' and active = TRUE ";

		$result = $this->connection->fetchAssocRows($query0);
		if (count($result) == 1) {
			$query = 
				"SELECT A.* 
				   FROM `view_feestdag_times` A      				
					WHERE A.ukgr_code = $ukgrCode AND A.datum='$event_date'   ORDER BY A.start_time ASC";
			$this->data['times'] = $this->connection->fetchAssocRows($query);			
		
		} else {
			$query = 
				"SELECT B.* 
				   FROM `location_times` A
				   INNER JOIN   `service_time` B ON B.id = A.time_id 			      				
					WHERE A.ukgr_code = $ukgrCode AND A.day_number=$dayNumber   ORDER BY B.start_time ASC";
			$this->data['times'] = $this->connection->fetchAssocRows($query);			
			
		}

		if (count($this->data['times'])  == 0 ) {
			$this->renderView('cleanData');
		} else {
			$this->data['lastTimeId']    = isset($this->session['lastTimeId'])    ? $this->session['lastTimeId']    : 0;
			$this->renderView(__FUNCTION__);
		}		
	}
	
	
	public function overzicht(){			
		$ukgrCode = $this->request['ukgr_code'];
		$dayNumber = $this->request['day_number'];
		$eventDate = $this->request['event_date'];
	
		$timeId = $this->request['time_id']; 
		$query = "SELECT A.*,B.start_time FROM `service_dayinfo` A 
		             INNER JOIN   `service_time` B ON B.id = A.time_id 	
					WHERE A.ukgr_code = $ukgrCode AND A.service_date = '$eventDate' AND A.time_id = $timeId ORDER BY naam ASC";

		
		$this->data['bezoekers'] = $this->connection->fetchAssocRows($query);
		$query = "SELECT count(*) as aantal FROM `service_dayinfo` 
					WHERE ukgr_code=$ukgrCode AND service_date='$eventDate' and time_id=$timeId
					GROUP BY ukgr_code, service_date, time_id";
					
		
		$_SESSION['lastUkgrCode'] = $ukgrCode;
		$_SESSION['lastEventDate'] = $this->request['event_date'];
		$_SESSION['lastTimeId'] = $this->request['time_id'];	
		$_SESSION['lastDayNumber'] = $this->request['day_number'];
		$dateSrc  = $this->request['event_date'] .' 12:00 CET';
		$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
		$_SESSION['lastDisplayDate']  = date_format($dateTime, 'd-m-yy');					                        
		$_SESSION['lastDayName']  = strftime ( '%A', $dateTime->getTimestamp());	
		$_SESSION['lastShowTime'] = $this->request['show_time'];
				
		$accesLevelItem = new AccesLevel('viewer');
		$this->data['canCheck']  =  ($this->accesLevelUser->getClearance() <= $accesLevelItem->getClearance());
					
		$this->renderView(__FUNCTION__);
	}
	
	
	
	
	public function stats(){
		$ukgrCode  = $this->request['ukgr_code'];
		$dayNumber = $this->request['day_number'];
		$eventDate = $this->request['event_date'];
		$timeId    = $this->request['time_id'];
		
		
		$query = "SELECT * FROM `location` 	WHERE ukgr_code = $ukgrCode";
		$locResult = $this->connection->fetchAssocRows($query);
		
		$query = "SELECT count(*) as aantal FROM `service_dayinfo` 
					WHERE ukgr_code=$ukgrCode AND service_date='$eventDate' and time_id=$timeId
					GROUP BY ukgr_code, service_date, time_id";
					
		$aantal = 0;			
        $lResult = $this->connection->fetchAssocRows($query);
        if (count($lResult) == 1) {
			$aantal  = intval($lResult[0]['aantal']);
		}
		
		$this->data['request'] =  $this->request;
		$this->data['request']['bezet'] = $aantal;
		$this->data['request']['max'] =  intval($locResult[0]['capacity']) ;
		
		$_SESSION['lastUkgrCode'] = $this->request['ukgr_code'];
		$_SESSION['lastEventDate'] = $this->request['event_date'];
		$_SESSION['lastTimeId'] = $this->request['time_id'];	
		$_SESSION['lastDayNumber'] = $this->request['day_number'];
		$dateSrc  = $this->request['event_date'] .' 12:00 CET';
		$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
		$_SESSION['lastDisplayDate']  = date_format($dateTime, 'd-m-yy');					                        
		$_SESSION['lastDayName']  = strftime ( '%A', $dateTime->getTimestamp());	
		$_SESSION['lastShowTime'] = $this->request['show_time'];		
		
		
		$this->renderView(__FUNCTION__);
	}
	
	
	public function statsTimed(){
    	$ukgrCode  = $this->session['lastUkgrCode'];
		$dayNumber = $this->session['lastDayNumber'];
		$eventDate = $this->session['lastEventDate'];
		$timeId    = $this->session['lastTimeId'];

		$query = "SELECT * FROM `location` 	WHERE ukgr_code = $ukgrCode";
		$locResult = $this->connection->fetchAssocRows($query);
		
		$query = "SELECT count(*) as aantal FROM `service_dayinfo` 
					WHERE ukgr_code=$ukgrCode AND service_date='$eventDate' and time_id=$timeId
					GROUP BY ukgr_code, service_date, time_id";
					
		$aantal = 0;			
        $lResult = $this->connection->fetchAssocRows($query);
        if (count($lResult) == 1) {
			$aantal  = intval($lResult[0]['aantal']);
		}
		$max = intval($locResult[0]['capacity']);
		$vrij = $max - $aantal;
		$response = array('bezet'=>$aantal,'max'=>$max, 'vrij'=>$vrij);
	
		echo json_encode($response);	

	}	
	
	public function meldAanwezigheid() {
		$personId = $this->request['person_id'];
		$admin    = $this->globals['admin'];
		
		try {
			$this->connection->handleUpdate(sprintf( "UPDATE service_dayinfo SET showed_up=TRUE WHERE id = %d ", $personId ));  
			echo "<img src=\"$admin/cssJsLibs/images/tick.png\" height=\"18\" />";
		} catch (Exception $e) {
			echo "Exception";
		} catch (Error $error) {
			echo "Error";
		}
	}

	public function jumpto(){
		if (count($this->globals['paths']) < 4) {
			throw new Exception("Ongeldige invoer.");
		}
		$ukgrCode  = $this->globals['paths'][0];
		$dayNumber = intval(trim($this->globals['paths'][1]));
		$serviceDate = $this->globals['paths'][2];
		$timeId = intval($this->globals['paths'][3]);
		
		
		$_SESSION['lastUkgrCode'] = $ukgrCode;
		$_SESSION['lastEventDate'] = $serviceDate ;
		$_SESSION['lastTimeId'] = $timeId;	
		$_SESSION['lastDayNumber'] = $dayNumber;	
		
		
	    $this->redirect('bezoekers');
	}


	
	public function removeUser(){
		$accesLevelItem = new AccesLevel('viewer');
		if ($this->accesLevelUser->getClearance() <= $accesLevelItem->getClearance()){		
			$delId = $this->paths[0];
			$query = 'DELETE FROM service_dayinfo WHERE id = '.$delId;
			$this->connection->handleDelete($query );		
		}
		
		$this->redirect('bezoekers');
	}
		
	
	private function checkBeforeADD($ukgrCode, $service_date, $time_id ){
		$query0 = "SELECT * FROM feestdagen WHERE datum = '$service_date' and active = TRUE";

		$result = $this->connection->fetchAssocRows($query0);
		$isFeestDag = (count($result) == 1);
		$feestdagParam = $isFeestDag ? 'J' : 'N';
		
		
		$tableLoc = "location_times";
		if ($isFeestDag) {
			$tableLoc = "view_feestdag_times";
		} 		
		
		
		$query = "SELECT *, '$feestdagParam' AS is_feestdag FROM (		
						SELECT 
							K.*,
							IF(E.current_count IS NULL,0, E.current_count) as current_count  
						FROM 
							(
								SELECT D.ukgr_code, D.day_number, D.time_id,  C.next_event, D.start_time ,D.city, D.address,D.capacity
								FROM 
									(
									   SELECT 
											Z.ukgr_code, Z.city, Z.address,Z.capacity,V.time_id,V.day_number, W.start_time
									   FROM $tableLoc V
									   INNER JOIN service_time W ON W.id = V.time_id
									   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code		
									   ORDER BY  V.time_id ASC  
									)  D	
									INNER JOIN		
									(
										SELECT DATE('$service_date')  AS 'next_event',	DAYOFWEEK(DATE('$service_date') )  AS day_number
					  
									) C  			
									ON D.day_number= C.day_number 
								ORDER BY D.ukgr_code ASC, D.time_id ASC,C.next_event ASC,  D.time_id ASC
							)	
							K
						LEFT JOIN
							(
								SELECT DISTINCT
									A.ukgr_code,		
									A.service_date, 
									A.day_number,						
									A.time_id, 
									COUNT(*) AS current_count
								FROM service_dayinfo A
								GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id
								) E ON E.ukgr_code = K.ukgr_code AND E.service_date = K.next_event AND E.day_number = K.day_number AND  E.time_id = K.time_id

					ORDER BY 
					  K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC		
		) A 
		WHERE 
			A.ukgr_code = $ukgrCode and A.next_event='$service_date' and A.time_id = $time_id
		ORDER BY 	A.next_event ASC, A.time_id ASC
			";


		$locresult = $this->connection->fetchAssocRows( $query );

		return new TimeSlot($locresult[0]);	
	}		
	
	
}
