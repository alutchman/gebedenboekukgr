<?php

class ClientCardService  extends BaseService {
	
	public  function getLevel() {
		return 'pastors';
	}
	
	public function show() {
		$this->data['lastUkgrCode']  = isset($this->session['lastUkgrForPersonCards'])  ? $this->session['lastUkgrForPersonCards']  : 1;		
		$this->fetchChurchLocations();	
		
		$defaultPage = 1;
		if (isset($this->session['lastUkgrForPersonCards']) && isset($this->session['lastPageForPersonCards'])) {
			if (intval($this->session['lastUkgrForPersonCards']) == intval($this->data['lastUkgrCode'])) {
				$defaultPage = $this->session['lastPageForPersonCards'];
			}
		}
		$this->data['startAtPage'] = $defaultPage;
		
		$this->renderView(__FUNCTION__, 'baseTemplate');			
	}
	
	
	public function showPref(){
		if (isset($this->request['startWith']) && strlen(trim($this->request['startWith'])) <= 1) {
			$_SESSION['lastStartWith'] = strtoupper(trim($this->request['startWith']));
			$_SESSION['lastPageForPersonCards'] = 1;
		}
		$this->redirect($this->basedir);
	}
	
	
	public function visitorPage(){
		$ukgrCode = $this->request['ukgr_code'];
		
		$letterQuery = "SELECT distinct UPPER(substr(first_name,1,1)) AS letter 
						FROM visitors 
						WHERE ukgr_code = $ukgrCode
						group by  UPPER(substr(first_name,1,1)) ORDER BY letter";
		$this->data['letters'] =  $this->connection->fetchAssocRows($letterQuery );
		
		if (count($this->data['letters']) > 0)  {			
			if (isset($_SESSION['lastUkgrForPersonCards']) && intval($_SESSION['lastUkgrForPersonCards']) !=  intval($ukgrCode )) {
				$lastStartWith = $this->data['letters'][0]['letter'] ;	
				$_SESSION['lastStartWith'] = $lastStartWith;
				$this->request['nexPage'] = 1;
			} else {
				$lastStartWith = isset($this->session['lastStartWith'])  ? $this->session['lastStartWith']  : $this->data['letters'][0]['letter'] ;		
			} 				
		} else {
			$lastStartWith = "A";
		}
		
		$_SESSION['lastUkgrForPersonCards'] = $ukgrCode;
		$this->data['lastUkgrCode']  = $ukgrCode ;
		$this->data['lastStartWith'] = $lastStartWith;		
		
		$filter = " AND first_name like '%' ";
		if (strlen($lastStartWith) >= 1) {
			$letter = substr($lastStartWith,0,1);
			$letterLower = strtolower($letter);
			$filter = sprintf(" AND (first_name like '%s' OR first_name like '%s')", $letter.'%', $letterLower.'%');
		}		
		
		
		$result = $this->connection->fetchAssocRows("SELECT COUNT(*) AS total FROM visitors WHERE ukgr_code = $ukgrCode $filter");
		
		$modulo = 12;
		
		$total =  intval($result[0]['total']);
		$this->data['total'] = $total;		
		$pages = ceil($total/$modulo);
		$this->data['pages'] = $pages;	
		
		$defaultPage = 1;		
		$page = isset($this->request['nexPage']) ? $this->request['nexPage'] : $defaultPage ;		
		
		$_SESSION['lastPageForPersonCards'] = $page;

		
		$this->data['currentPage'] = intval($page);		
		$offset = ($page-1) * $modulo;		
		$order = "first_name ASC, last_name ASC";
		$this->data['visitors'] = $this->connection->fetchAssocRows(
			sprintf("SELECT * FROM visitors WHERE ukgr_code = %d %s ORDER BY %s  LIMIT %d,%d", $ukgrCode ,$filter,$order, $offset, $modulo));
	    $this->renderView(__FUNCTION__);	
	}
	
	private function saveForupdate($personId) {
		if (isset($this->post['first_name'])    && isset($this->post['last_name'])  
				 && isset($this->post['email'])     && isset($this->post['birth_date'])  
				 && isset($this->post['password1']) && isset($this->post['password2'])
				 && isset($this->post['ukgr_code'])
				 && isset($this->post['useDate']) && isset($_FILES['picture'])   ) {
				$processedPerson = false; 	
				$fileResult = $this->processPicture($_FILES['picture']);		
				
				if(!preg_match(self::EMAIL_EXPR, trim($this->post['email']))) {
					$this->errorMessage .= "Ongeldige email opgegeven";
				} else if ($fileResult !== false) {
					$password1 = trim($this->post['password1']);
					$password2 = trim($this->post['password2']);
					if (strlen($password1) > 0 &&  strlen($password2) > 0 && $password1 !== $password2)  {
						$this->errorMessage .= "Uw wachtwoorden zijn niet gelijk.\n";
					} else {						
						if (strlen($password1) > 0) {
							$this->updateUserWithPasswrdNoImage($personId);	
							$processedPerson = true;
						}	else if ($fileResult['data'] != null  && $fileResult['size'] > 0 ) {
							$this->updateUserNoPasswrdWithImage($fileResult, $personId);
							$processedPerson = true;
							
						} else if ((strlen($password1) > 0) && $fileResult['data'] != null  && $fileResult['size'] > 0 ) {
							//update image update pw
							$this->updateUserWithPasswrdWithImage($fileResult, $personId);
							$processedPerson = true;
						} else {
							$this->updateUserNoPasswrdOrImage($personId);
							$processedPerson = true;
						}		
					}				
			}	
			if ($processedPerson) {
				$_SESSION['lastUkgrCode'] = $this->post['ukgr_code'];
				$this->redirect($this->basedir);
			}
		}
	}
	
	 
	
	public function update(){
		if (count($this->paths) < 1  ){
			$this->redirect($this->basedir);
			return;
		}	
		$personId = intval($this->paths[0]);	
		
		$this->saveForupdate($personId);
		
		
		$query = sprintf("SELECT * FROM visitors WHERE id = %d", $personId); 
		$userDataList = $this->connection->fetchAssocRows($query );
		if (count($userDataList) == 0) {
			$this->redirect($this->basedir);
			return;
		}
		$userData = $userDataList[0];	
		$userInfo = new UserInfo();
		$userInfo->init($userData); 
		$this->data['userInfo'] = $userInfo->userRecord;
		$this->fetchChurchLocations();
	
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}
	
	public function times(){
		if (count($this->paths) < 1  ){
			$this->redirect($this->basedir);
			return;
		}	
		$personId = intval($this->paths[0]);	
				
		if (isset($this->post['updateRequired'])) {
			$this->savePreferences($personId);
			$this->redirect($this->basedir);			
		}		
		$query = sprintf("SELECT * FROM visitors WHERE id = %d", $personId); 
		$userDataList = $this->connection->fetchAssocRows($query );
		if (count($userDataList) == 0) {
			$this->redirect($this->basedir);
			return;
		}
		$userData = $userDataList[0];	
		$userInfo = new UserInfo();
		$userInfo->init($userData); 
		$this->data['userInfo'] = $userInfo->userRecord;		
	    $this->data['prefs'] = $this->fetchDetailedData(Querys::PREF_LOC_MEMBERS,  
									array($personId, $userInfo->userRecord['ukgr_code'], 
									$userInfo->userRecord['ukgr_code'])); 		
		$this->data['times'] = $this->fetchDetailedData(Querys::TIME_OPT);	
		
		$this->fetchChurchLocations($userInfo->userRecord['ukgr_code']);		
		
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}
		
	
	public function planning(){
		if (count($this->paths) < 1  ){
			$this->redirect($this->basedir);
			return;
		}	
		
		$personId = intval($this->paths[0]);
		$query = sprintf("SELECT * FROM visitors WHERE id = %d", $personId); 
		$userDataList = $this->connection->fetchAssocRows($query );
		if (count($userDataList) == 0) {
			$this->redirect($this->basedir);
			return;
		}		
		
		$userData = $userDataList[0];	
		$userInfo = new UserInfo();
		$userInfo->init($userData); 		
		if (isset($this->post['updateRequired'])) {
			$this->saveTimesForMember($userInfo->userRecord);
			$this->redirect($this->basedir);
			return;
		}	
		
		$this->data['userInfo'] = $userInfo->userRecord;				
		$this->getFromLocationAndPersonNew($userInfo->userRecord);
				
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}

	

	public function addRadioGroupList($grouplist, $name, $currentValue = '', $labeClass = '') {
		foreach($grouplist as $item) {
			$itemClass = array_key_exists("class",$item) ? $item['class']  : '';
			$this->addRadioGroup($name, $item['value'], $item['title'], $currentValue, $labeClass, $itemClass);
		}
	}
	
	public function addRadioGroup($name, $value, $title, $currentValue = '', $labeClass = '', $itemClass = ''){
		$useClass = '';
		if ($labeClass !== '') {
			if ($itemClass !== '') {
				$useClass = "class=\"$labeClass $itemClass\"";				
			} else {
				$useClass = "class=\"$labeClass\"";
			}			
		}
		
		$checked = ($value === $currentValue) ? " checked" : "";
		
		?>
		<label <?php echo($useClass); ?>>
			<input type="radio"  name="<?php echo($name); ?>"  value="<?php echo($value); ?>" required <?php echo $checked; ?>/>
			<span style="float:left"><?php echo($title); ?></span>
		</label>		
		<?php		
	}	
	
	
	protected function printIdImage($userRecord){
		$data2Use  = null;
		$extension = 'png';
		if ($userRecord != null) {		  
		  if ($userRecord['picture']  != null && $userRecord['extension'] != null)  {		
			  $data2Use = $userRecord['picture'];
			  $extension = $userRecord['extension'];
		  }
		}
		if ($data2Use == null)  {
			$data2UseRAW = file_get_contents($this->globals['root'].'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'basePhoto.png');
			$data2Use = base64_encode($data2UseRAW);
		}		
		
		echo "data:image/$extension;base64,$data2Use" ;
	}
	
	
	
	private function updateUserWithPasswrdWithImage($fileResult, $personId) {
		$pwd_hashed = password_hash($password1, PASSWORD_BCRYPT, $this->options);
		$initialData['picture'] = $fileResult['data'];
		$initialData['extension'] = $fileResult['ext'];
		$this->data['updatePicture']  = true;
		
		$query = "UPDATE visitors SET
			ukgr_code = %d,
			password = '%s',
			email = '%s', 
			gsm   = '%s', 
			birth_date = '%s',
			first_name = '%s',
			last_name  = '%s',
			zipcode    = '%s',
			house_number  = %d,
			extra_address = '%s',
			street = '%s',
			city   = '%s',
			gender   = '%s',
			picture     = '%s',
			extension   = '%s'
			WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $pwd_hashed,
					$this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'], 
					$fileResult['data'], $fileResult['ext'], $personId);		
									
		$this->connection->handleUpdate($queryExec);			
	}
	
	private function updateUserNoPasswrdWithImage($fileResult, $personId) {
		$initialData['picture'] = $fileResult['data'];
		$initialData['extension'] = $fileResult['ext'];
		$this->data['updatePicture']  = true;
		
		$query = "UPDATE visitors SET
			ukgr_code = %d,
			email = '%s', 
			gsm   = '%s', 
			birth_date = '%s',
			first_name = '%s',
			last_name  = '%s',
			zipcode    = '%s',
			house_number  = %d,
			extra_address = '%s',
			street = '%s',
			city   = '%s',
			gender   = '%s',
			picture     = '%s',
			extension   = '%s'
			WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'], 
					$fileResult['data'], $fileResult['ext'], $personId);		
									
		$this->connection->handleUpdate($queryExec);	
	}
	
	private function updateUserWithPasswrdNoImage($personId){
		$pwd_hashed = password_hash(trim($this->post['password1']), PASSWORD_BCRYPT, $this->options);
		$query = "UPDATE visitors SET
						ukgr_code = %d,
						password = '%s',
						email = '%s', 
						gsm   = '%s', 
						birth_date = '%s',
						first_name = '%s',
						last_name  = '%s',
						zipcode    = '%s',
						house_number  = %d,
						extra_address = '%s',
						street = '%s',
						city   = '%s',
						gender   = '%s'
						WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $pwd_hashed,
					$this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'], $personId);		
			
		$this->connection->handleUpdate($queryExec);		
		
	}	
	
	private function updateUserNoPasswrdOrImage($personId){
		$overleden = isset($this->post['overleden']) && strlen($this->post['overleden']) > 0 ? 
						$this->post['overleden'] = "'".$this->post['overleden']."'" : "NULL";
						
		$deactiveren = 	$this->post['activated'] === 'FALSE' &&  $this->post['activated_before'] === 'TRUE';		
		if (isset($this->post['overleden']) && strlen($this->post['overleden']) > 0 ) {
			$this->post['activated'] = 'FALSE';
			$deactiveren = true;
		}	
		
		$query = "UPDATE visitors SET
			ukgr_code = %d,
			email = '%s', 
			gsm   = '%s', 
			birth_date = '%s',
			first_name = '%s',
			last_name  = '%s',
			zipcode    = '%s',
			house_number  = %d,
			extra_address = '%s',
			street = '%s',
			city   = '%s',
			gender   = '%s',
			overleden = %s,
			activated = %s
			WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'],$overleden,$this->post['activated'], $personId);		
			
		$this->connection->handleUpdate($queryExec);	
		
		if ($deactiveren ) {
			//verwijder preferenties voor planning en boekingen inde toekomst
			$this->connection->handleUpdate("DELETE FROM service_dayinfo WHERE member_id= $personId and service_date > CURDATE()");	
			$this->connection->handleUpdate("DELETE FROM member_preference where member_id =  $personId ");
		}			
	}
	
	
	private function getFromLocationAndPersonNEW($userRecord){
		$personId =  $userRecord['id'];
		$ukgrCode = $userRecord['ukgr_code'];
		$dateToday = new DateTime();
		$interval = new DateInterval('P1D');
		$dateTomorrow= (new DateTime())->add( $interval );;

		$dayNum1 = 1+intval($dateToday->format('w'));
		$dayNum2 = 1+intval($dateTomorrow->format('w'));

		$datToUse = $dayNum1 == 7 ? $dateTomorrow : $dateToday;
		$service_date = $datToUse->format("Y-m-d");		
		
		$dataRange = $this->getDateRangeForSelectedDate($service_date);
		$query = " SELECT A.*, B.id, B.time_id,B.showed_up,B.time_id_member, E.start_time, Q.time_id as pref_time_id, W.start_time as pref_start_time FROM (		
						$dataRange	
		
					) A
					 INNER JOIN member_preference Q ON Q.member_id = $personId AND 
					 (
					     (Q.day_number = A.day_number AND A.next_event NOT IN (SELECT datum FROM feestdagen WHERE active = TRUE)) OR
					     (
					       Q.day_number = 1 AND A.next_event IN (SELECT datum FROM feestdagen WHERE active = TRUE)
					       AND Q.time_id = (SELECT MIN(time_id) FROM member_preference WHERE day_number = 1 AND member_id = $personId)
					     
					     )					     
					 ) 					 
					 INNER JOIN service_time W ON W.id = Q.time_id
					 LEFT JOIN service_dayinfo B 
						ON B.service_date = A.next_event AND 
							B.time_id_member = Q.time_id AND 
							B.member_id = %d AND 
							B.ukgr_code = %d
					 LEFT JOIN service_time E ON E.id = B.time_id
				 
					 ORDER BY A.next_event ASC,Q.time_id ASC, E.start_time ASC";	
		$weekTimes  = $this->connection->fetchAssocRows( sprintf($query,$personId, $ukgrCode));			
		foreach($weekTimes as &$weekTime) {
			$weekTime['timeSlots'] = $this->getOptionsForLocation($ukgrCode, $weekTime['next_event']);	
		}
		$this->data['weekTimes'] = $weekTimes;	 					 
		
	}	
	
	private function getOptionsForLocation($ukgrCode, $service_date){			
		$query0 = "SELECT * FROM feestdagen WHERE datum = '$service_date' and active = TRUE";
		$result = $this->connection->fetchAssocRows($query0);
		$isFeestDag = (count($result) == 1);
		$feestdagParam = $isFeestDag ? 'J' : 'N';

		$tableLoc = "(SELECT * FROM location_times WHERE ukgr_code = $ukgrCode)";
		if ($isFeestDag) {
			$tableLoc = "(SELECT * FROM view_feestdag_times WHERE datum = '$service_date' AND ukgr_code = $ukgrCode)";
		} 		
		
		$this->timeSlot = array();		
		$query = "SELECT *,'$feestdagParam' AS is_feestdag FROM (		
						SELECT 
							K.*,
							IF(E.current_count IS NULL,0, E.current_count) as current_count  
						FROM 
							(
								SELECT D.ukgr_code, D.day_number, D.time_id,  C.next_event, D.start_time ,D.city, D.address,D.capacity
								FROM 
									(
									   SELECT 
											Z.ukgr_code, Z.city, Z.address,Z.capacity,V.time_id,V.day_number, W.start_time
									   FROM $tableLoc V
									   INNER JOIN service_time W ON W.id = V.time_id
									   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code		
									   ORDER BY  V.time_id ASC  
									)  D	
									INNER JOIN		
									(
										SELECT DATE('$service_date')  AS 'next_event',	DAYOFWEEK(DATE('$service_date') )  AS day_number
					  
									) C  			
									ON D.day_number= C.day_number 
								ORDER BY D.ukgr_code ASC, D.time_id ASC,C.next_event ASC,  D.time_id ASC
							)	
							K
						LEFT JOIN
							(
								SELECT DISTINCT
									A.ukgr_code,		
									A.service_date, 									
									A.time_id, 
									COUNT(*) AS current_count
								FROM service_dayinfo A
								GROUP BY A.ukgr_code,A.service_date, A.time_id
								) E ON E.ukgr_code = K.ukgr_code AND E.service_date = K.next_event  AND  E.time_id = K.time_id

					ORDER BY 
					  K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC		
		) A 
		WHERE 
			A.ukgr_code = $ukgrCode and A.next_event='$service_date'
		ORDER BY 	A.next_event ASC, A.time_id ASC
			";

		$locresult = $this->connection->fetchAssocRows( $query );
		foreach($locresult as $row) {
			$this->timeSlot[] = new TimeSlot($row);
		}
		return $this->timeSlot;	
	}	
	
	
	
	private function getDateRangeForSelectedDate($service_date) {
		$selections = array();
		$selections[] = "SELECT DATE('$service_date')  AS 'next_event',	DAYOFWEEK(DATE('$service_date') )  AS day_number";
		for ($delta = 1; $delta <= 42; $delta++) {
		  $selections[] = "SELECT DATE_ADD(DATE('$service_date'), INTERVAL $delta DAY)  AS 'next_event',	DAYOFWEEK(DATE_ADD(DATE('$service_date'), INTERVAL $delta DAY) )  AS day_number";
		} 
		$data = implode(" UNION ", $selections)		;
		
		return $data ;		
	}
		
	
	private function checkBeforeADD($ukgrCode, $service_date, $time_id ){
		$query0 = "SELECT * FROM feestdagen WHERE datum = '$service_date' and active = TRUE";
		$result = $this->connection->fetchAssocRows($query0);
		$isFeestDag = (count($result) == 1);
		$feestdagParam = $isFeestDag ? 'J' : 'N';
		
		$tableLoc = "(SELECT * FROM location_times WHERE ukgr_code = $ukgrCode)";
		if ($isFeestDag) {
			$tableLoc = "(SELECT * FROM view_feestdag_times WHERE datum = '$service_date' AND ukgr_code = $ukgrCode)";
		} 		
		
		
		$query = "SELECT *,'$feestdagParam' AS is_feestdag FROM (		
						SELECT 
							K.*,
							IF(E.current_count IS NULL,0, E.current_count) as current_count  
						FROM 
							(
								SELECT D.ukgr_code, D.day_number, D.time_id,  C.next_event, D.start_time ,D.city, D.address,D.capacity
								FROM 
									(
									   SELECT 
											Z.ukgr_code, Z.city, Z.address,Z.capacity,V.time_id,V.day_number, W.start_time
									   FROM $tableLoc  V
									   INNER JOIN service_time W ON W.id = V.time_id
									   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code		
									   ORDER BY  V.time_id ASC  
									)  D	
									INNER JOIN		
									(
										SELECT DATE('$service_date')  AS 'next_event',	DAYOFWEEK(DATE('$service_date') )  AS day_number
					  
									) C  			
									ON D.day_number= C.day_number 
								ORDER BY D.ukgr_code ASC, D.time_id ASC,C.next_event ASC,  D.time_id ASC
							)	
							K
						LEFT JOIN
							(
								SELECT DISTINCT
									A.ukgr_code,		
									A.service_date, 
									A.day_number,						
									A.time_id, 
									COUNT(*) AS current_count
								FROM service_dayinfo A
								GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id
								) E ON E.ukgr_code = K.ukgr_code AND E.service_date = K.next_event AND E.day_number = K.day_number AND  E.time_id = K.time_id

					ORDER BY 
					  K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC		
		) A 
		WHERE 
			A.ukgr_code = $ukgrCode and A.next_event='$service_date' and A.time_id = $time_id
		ORDER BY 	A.next_event ASC, A.time_id ASC
			";

		$locresult = $this->connection->fetchAssocRows( $query );
		return new TimeSlot($locresult[0]);	
	}		
	
	
	private function saveTimesForMember($userRecord) {
		$ukgrCode  = $userRecord['ukgr_code'];
		$naam     = $userRecord['first_name'].' '.$userRecord['last_name'];		
		$email    = $userRecord['email'];
		$gsm      = $userRecord['gsm'];
		$memberId = $userRecord['id'];
		
				
		foreach($this->post['person'] as $Itemdate => &$personTime) {	
			if (array_key_exists('time_id', $personTime)){
				foreach($personTime['time_id'] as $prefTime=>$time_id) {
					$dayNumberAdd = $personTime['day_number'];
					$timeSlotCheck = $this->checkBeforeADD($ukgrCode, $Itemdate , $time_id);
					$rescode  = sprintf('VIP%03dX%dY%d', $userRecord['id'], $ukgrCode, $dayNumberAdd);
										
					if (!$timeSlotCheck->full) {			  
						if (strlen(trim($personTime['id'][$prefTime])) == 0) { 					
						    $preCheck = "SELECT * FROM service_dayinfo 
											WHERE service_date='$Itemdate'AND member_id= $memberId AND time_id = $time_id AND time_id_member = $prefTime";
							$result = $this->connection->fetchAssocRows( $preCheck);	
							if (count($result) == 0) {
								$query = "INSERT INTO service_dayinfo 
										(ukgr_code, day_number, service_date, time_id, naam, rescode, gsm, email, member_id, time_id_member) VALUES 
										($ukgrCode, $dayNumberAdd, '$Itemdate',$time_id, '$naam', '$rescode', '$gsm', '$email', $memberId, $prefTime)";
								$this->connection->handleInsert($query );								
							} else {
								//already exists so do no update
								
							}	
						} else { 
							//Updated Times for person
							$personIdEvent = intval($personTime['id'][$prefTime]);
							$query = "UPDATE service_dayinfo SET time_id = $time_id, time_id_member = $prefTime WHERE id = $personIdEvent";
							$this->connection->handleUpdate($query);    
						}							
					}
				}
			}
			if (array_key_exists('del_id', $personTime)){
				foreach($personTime['del_id'] as $prefTime=>$delId) {
					$query = 'DELETE FROM service_dayinfo WHERE id = '.$delId;
					$this->connection->handleDelete($query );					
				}
			}
		}
	}
		
		
	private function savePreferences($personId){
		if (isset($this->post['updateRequired'])) {
			$query = "DELETE FROM member_preference WHERE member_id = ".$personId ;
			$this->connection->handleUpdate($query);			
			if (isset($this->post['prefered'])) {
				$valueList = array();
				foreach($this->post['prefered'] as $dayNumber => $timeIdList) {
					foreach($timeIdList as $time_id => $value) {
						$valueList[] = sprintf("(%d, %d, %d)", $personId, $dayNumber, $time_id); 
					}					
				}
				$valueJoined = implode(',', $valueList);
				
				$query = "INSERT INTO member_preference (member_id, day_number, time_id) VALUES $valueJoined";
				$this->connection->handleInsert($query);
			}
		}
	}
}	
