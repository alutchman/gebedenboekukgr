<?php

class FeestdagService  extends BaseService {
	
	public function getLevel() {
		return 'pastors';
	}
	
	
	public function show() {		
		$query = "SELECT * FROM feestdagen ORDER BY  datum ASC";
		$this->data['altDays'] = $this->connection->fetchAssocRows($query);	
		$this->renderView(__FUNCTION__, 'baseTemplate');		
	}
	
	
	public function add(){
		$this->data['feestdagEvent'] = date("Y-m-d");
		$this->data['lastDisplayDateFeest'] = date("d-m-Y");
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}
	
	public function saveAdd(){
		$this->renderView(__FUNCTION__, 'baseTemplate');	
		
	}
	
	
	public function saveEdit(){
		$id           = $this->post['id'];
		$datum        = $this->post['datum'];
		$beschrijving = $this->post['beschrijving'];
		$active       = isset($this->post['active'])  ? "TRUE" : "FALSE";
		
		$query = "UPDATE feestdagen SET datum='$datum', beschrijving='$beschrijving', active = $active WHERE id = $id";
		$this->connection->handleUpdate($query);
		$getTime = time();
		$this->redirectLocal('show','/'.$getTime);
	}
	
		
	public function edit(){
		if (count($this->paths) < 1  ){
			$this->redirect($this->basedir);
			return;
		}	
		$query = sprintf("SELECT * FROM feestdagen WHERE id = %d ", $this->paths[0]);
		$this->data['editItem']  = $this->connection->fetchAssocRows($query)[0];	
		
		$this->data['feestdagEvent'] = $this->data['editItem']['datum'];
		$timeToUse   = "12:00";
		$dateSrc  = $this->data['feestdagEvent']." $timeToUse CET";
		$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
		$this->data['lastDisplayDateFeest'] = date_format($dateTime, 'd-m-Y');
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}
	
	public function locaties(){
		if (count($this->paths) < 1  ){
			$this->redirect($this->basedir);
			return;
		}			
		$this->data['locations'] = $this->connection->fetchAssocRows('SELECT * FROM location WHERE active = true ORDER BY ukgr_code ASC');
		$this->data['feestdagID'] = $this->paths[0];
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}

}
