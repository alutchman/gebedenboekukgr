<?php

class LocationService  extends BaseService {
	private $email_from = "info@ukgr.nl";
	private $days = array('Zondag','Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag');
	
	public function getLevel() {
		return 'pastors';
	}	
	
	public function show(){
		$query = "SELECT B.short_name, B.long_name,  A.*  FROM 	view_next_dates A
					INNER JOIN view_day_names B ON B.day_number = A.day_number
					WHERE A.day_number != 7
					ORDER BY A.next_event ASC ";
					
		$this->data['dayHeader'] = 	$this->connection->fetchAssocRows($query);						
		$this->fetchUkgrLocations();		
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}	
	
	
	public function edit(){
		$ukgrCode = $this->globals['paths'][0];
		$locresult = $this->connection->fetchAssocRows('SELECT * FROM location WHERE ukgr_code = '.$ukgrCode );
		if (count($locresult ) == 0 ) {
			 throw new Exception("Locatie onbekend voor ukgr code $ukgrCode");
		} 
		$this->data['location'] = new Location($locresult[0]);
		
		$this->data['timesPerDayAtLocation'] = array();
		foreach ($this->days as $index=>$dayName) {
			$dayNumber = $index + 1;
			$query = "SELECT 
				A.id, 
				A.start_time, 
				IF(B. id   IS NULL,false, true) as ischecked 
			FROM `service_time` A
			LEFT JOIN `location_times` B 
			ON B.ukgr_code = $ukgrCode AND B.day_number=$dayNumber  AND B.time_id=A.id ORDER BY A.start_time ASC";
			$date4qry = $this->connection->fetchAssocRows($query);	
			$this->data['timesPerDayAtLocation'][] = array(
			  'dayValue' => $dayName,
			  'dayNumber' => $dayNumber,
			  'data' => $date4qry
			);
		} 
	
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	public function add() {
		$query = 'SELECT MAX(ukgr_code) AS last_id FROM location';
		$locresult = $this->connection->fetchAssocRows($query);
		$startCode = 1;
		if (count($locresult) == 1) {
			$startCode = 1 + intval($locresult[0]['last_id']);
		} 
		$this->data['startCode'] = $startCode;
		
		
		$query = "SELECT 
			A.id, 
			A.start_time, 
			IF(B. id   IS NULL,false, true) as ischecked 
		FROM `service_time` A
		LEFT JOIN `location_times` B ON B.ukgr_code = $startCode AND B.time_id=A.id ORDER BY A.start_time ASC";
		$this->data['diensten']  =  $this->connection->fetchAssocRows($query);	
		
		
		$this->data['timesPerDayAtLocation'] = array();
		foreach ($this->days as $index=>$dayName) {
			$dayNumber = $index + 1;
			$query = "SELECT 
				A.id, 
				A.start_time, 
				IF(B. id   IS NULL,false, true) as ischecked 
			FROM `service_time` A
			LEFT JOIN `location_times` B 
			ON B.ukgr_code = $startCode AND B.day_number=$dayNumber  AND B.time_id=A.id ORDER BY A.start_time ASC";
			$date4qry = $this->connection->fetchAssocRows($query);	
			$this->data['timesPerDayAtLocation'][] = array(
			  'dayValue' => $dayName,
			  'dayNumber' => $dayNumber,
			  'data' => $date4qry
			);
		} 
		
		
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	public function handleAdd(){				
		if (strlen(trim($this->post['city'])) > 0  && strlen(trim($this->post['address'])) > 0 && strlen(trim($this->post['capacity'])) > 0)  {
			$ukgrCode = $this->post['ukgr_code'];		
		
			$query = "INSERT INTO `location` (ukgr_code, city, address, capacity) VALUES (%d, '%s', '%s', %d) ";
			$queryAdd = sprintf($query, $ukgrCode,  $this->post['city'], $this->post['address'], $this->post['capacity']); 

			$this->connection->handleInsert($queryAdd );
			 
			
			//insert times in table 	
			foreach ($this->days as $index=>$dayName) {
				$dayNumber = $index + 1;
				$newTimes  = $this->post['dayTimes'][$dayNumber];
				
				foreach ($newTimes as $newTime)  {
					$query = "INSERT INTO `location_times` (ukgr_code, day_number, time_id) VALUES ($ukgrCode, $dayNumber, $newTime) ";
					$this->connection->handleInsert($query );					
				}
			}			
					
		} else {
			$posData = print_r($this->post, TRUE); 
			throw new Exception("Ongeldige invoer. \n<pre>$posData</pre> ");
		}		

		$this->redirect('location', 'show');

	}
	
	
	public function handleEdit(){
		if (strlen(trim($this->post['city'])) > 0  
				&& strlen(trim($this->post['address'])) > 0 				
				&& strlen(trim($this->post['capacity'])) > 0)  {
			$ukgrCode = $this->post['ukgr_code'];	
			
			$active = isset($this->post['active'])	? 'TRUE' : 'FALSE';	
			
			foreach ($this->days as $index=>$dayName) {
				$dayNumber = $index + 1;
				$query = "SELECT * FROM `location_times`  WHERE ukgr_code = $ukgrCode AND day_number = $dayNumber";
				$prevTimes = $this->connection->fetchAssocRows($query);					
				$newTimes  = $this->post['dayTimes'][$dayNumber];
				foreach ($prevTimes as $prevTime)  {
				   $dataFound = false;
				   foreach ($newTimes as $newTime)  {
					   if ($newTime == $prevTime['time_id']) {
						   $dataFound = true;
						   break;
					   }
				   }
				   if (! $dataFound) {
					   $delId = $prevTime['id'];
					   $query = "DELETE FROM `location_times` WHERE id = $delId ";
					   $this->connection->handleDelete($query);
				   }		   	
				}	
				foreach ($newTimes as $newTime)  {
					$dataFound = false;
					foreach ($prevTimes as $prevTime)  {
						if ($newTime == $prevTime['time_id']) {
						   $dataFound = true;
						   break;
					   }				
					}
					if (!$dataFound) {
					   $query = "INSERT INTO `location_times` (ukgr_code, day_number, time_id) VALUES ($ukgrCode, $dayNumber, $newTime) ";
					   $this->connection->handleInsert($query );
					}
				}			
			}		

			$query = sprintf("UPDATE location SET city='%s', address='%s', capacity=%d, active=%s WHERE id=%d",
							 $this->post['city'], $this->post['address'], $this->post['capacity'],  $active, $this->post['id']);
			$this->connection->handleUpdate($query);		
		} else {
			$posData = print_r($this->post, TRUE); 
			throw new Exception("Ongeldige invoer. \n<pre>$posData</pre> ");
		}

		$this->redirect('location', 'show');
	}
	
	public function handleFeestdagenEdit(){
		$ukgrCode = $this->post['ukgr_code'];	
		$timeData = $this->post['dayTimes'];	

		$query = "SELECT * FROM feestdagen WHERE active = TRUE ORDER BY datum ASC";
		$feestDagInfo = $this->connection->fetchAssocRows($query );

		//echo("<pre>");
		//print_r($this->post);
		//exit;

		foreach ($feestDagInfo as &$feestDagData) {
			$DateLastTime = $feestDagData['datum'];	 	
			$dateSrc      = $DateLastTime.' 12:00 CET';
			$dateTime     = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 		
			$dayNumber    = 1+intval(strftime ( '%w', $dateTime->getTimestamp()));
			$feestDagId   = $feestDagData['id'];		
			
			$query = "SELECT * FROM `location_feast_times`  WHERE ukgr_code = $ukgrCode AND feestdag_id = $feestDagId";
			$prevTimes = $this->connection->fetchAssocRows($query);					
			$newTimes  = array_key_exists($feestDagId, $this->post['dayTimes']) ? $this->post['dayTimes'][$feestDagId] : array() ;
			foreach ($prevTimes as $prevTime)  {
			   $dataFound = false;
			   foreach ($newTimes as $newTime)  {
				   if ($newTime == $prevTime['time_id']) {
					   $dataFound = true;
					   break;
				   }
			   }
			   if (!$dataFound) {
				   $delId = $prevTime['id'];
				   $query = "DELETE FROM `location_feast_times` WHERE id = $delId ";
				   $this->connection->handleDelete($query);
			   }		   	
			}
			foreach ($newTimes as $newTime)  {
				$dataFound = false;
				foreach ($prevTimes as $prevTime)  {
					if ($newTime == $prevTime['time_id']) {
					   $dataFound = true;
					   break;
				   }				
				}
				if (!$dataFound) {
				   $query = "INSERT INTO `location_feast_times` (ukgr_code, time_id, feestdag_id)
								VALUES ($ukgrCode, $newTime, $feestDagId) ";
				   $this->connection->handleInsert($query );
				}
			}						
		
	    }		
		$this->redirect('location', 'show');
	}
	
	public function church(){
		if (count($this->globals['paths']) < 2) {
			throw new Exception("Ongeldige invoer.");
		}
		$ukgrCode  = $this->globals['paths'][0];
		$dayNumber = intval(trim($this->globals['paths'][1]));
		
		if (strlen($dayNumber) == 0) {
			throw new Exception("De dag is niet opgegeven.");
		}
		
		if (intval($dayNumber) < 1 ||   intval($dayNumber) > 7 ) {
			throw new Exception("Ongeldige dag opgegeven.");
		}
		
		$locresult = $this->connection->fetchAssocRows('SELECT * FROM location WHERE ukgr_code = '.$ukgrCode );
		if (count($locresult ) == 0 ) {
			 throw new Exception("Locatie onbekend voor ukgr code $ukgrCode");
		} 
		
		$this->data['location'] = new Location($locresult[0]);
	
		$this->initDuoDates();
		
		$nextevent = $this->data['nextevent'];
			
		$this->getDateRangeForSelectedDate($ukgrCode, $nextevent);
	
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	public function feestdagen(){
		$ukgrCode = $this->globals['paths'][0];
		$locresult = $this->connection->fetchAssocRows('SELECT * FROM location WHERE ukgr_code = '.$ukgrCode );
		if (count($locresult ) == 0 ) {
			 throw new Exception("Locatie onbekend voor ukgr code $ukgrCode");
		} 
		$this->data['location'] = $locresult[0];
		
		$query = "SELECT * FROM feestdagen WHERE active = TRUE ORDER BY datum ASC";
		$feestDagInfo = $this->connection->fetchAssocRows($query );
		
		
		
		
		$this->data['timesPerDayAtLocation'] = array();
		foreach ($feestDagInfo as &$feestDagData) {
			$DateLastTime = $feestDagData['datum'];	 	

			$dateSrc  = $DateLastTime.' 12:00 CET';
			$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
			$feestDagData['display_date'] = date_format($dateTime, 'd-m-yy');	
			$dayNumber = 1+intval(strftime ( '%w', $dateTime->getTimestamp()));
			$feestDagData['dayNumber'] = $dayNumber;
			$feestDagData['dayName'] = strftime ( '%A', $dateTime->getTimestamp());
		    $feestDagId = $feestDagData['id'];
		    
		   
			$query = "SELECT 
							A.id, 
							A.start_time, 
							IF(B. id   IS NULL,false, true) as ischecked 
						FROM `service_time` A
						LEFT JOIN `location_feast_times` B 
						ON B.ukgr_code = $ukgrCode AND 
						   feestdag_id = $feestDagId AND B.time_id=A.id ORDER BY A.start_time ASC";
			
			$date4qry = $this->connection->fetchAssocRows($query);	
			 
			$this->data['timesPerDayAtLocation'][] = array(
			  'feestDagData' => $feestDagData,
			  'dayNumber' => $dayNumber,
			  'data' => $date4qry
			);
		} 
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	private function getDateRangeForSelectedDate($ukgrCode, $service_date) {
		$selections = array();
		for ($index = -1; $index <= 12; $index++) {
			$delta = 7 * $index;
			$selections[] = "SELECT DATE_ADD(DATE('$service_date'), INTERVAL $delta DAY)  AS 'next_event',	DAYOFWEEK(DATE_ADD(DATE('$service_date'), INTERVAL $delta DAY) )  AS day_number";
		} 
		$query = implode(" UNION ", $selections)		;
		
		$this->data['moreDaysInfo']  = $this->connection->fetchAssocRows($query );		
		
	
		foreach($this->data['moreDaysInfo'] as &$dataForDate) {
			$itemDate = $dataForDate['next_event'];
			$itemDayNumber = $dataForDate['day_number'];
			
			$query = "SELECT A.start_time, A.display_time, A.time_id ,X.capacity, IF(E.current_count IS NULL,0, E.current_count) as current_count  
							FROM view_location_times A 
							INNER JOIN location X ON X.ukgr_code = A.ukgr_code
							LEFT JOIN (
									SELECT DISTINCT
										A.ukgr_code,		
										A.service_date, 
										A.day_number,						
										A.time_id, 
										COUNT(*) AS current_count
									FROM service_dayinfo A									
									GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id
									
								) E ON E.ukgr_code = A.ukgr_code AND E.service_date = DATE('$itemDate') AND E.day_number = $itemDayNumber AND  E.time_id = A.time_id		
					WHERE A.ukgr_code = $ukgrCode AND A.day_number = $itemDayNumber 
				    ORDER BY A.time_id ASC
					";			
		    $dataForDate['data'] = $this->connection->fetchAssocRows($query );		
		}		
		$query = "SELECT DAYOFWEEK(DATE('$service_date'))  AS day_number ";		
		$resultDatum = $this->connection->fetchAssocRows($query);
		$dayNumber=  $resultDatum[0]['day_number'];
		$this->data['day_number'] = $dayNumber ;
		
	  	$this->data['dateHeader']  = $this->connection->fetchAssocRows("SELECT * FROM view_location_times 
														WHERE ukgr_code =  $ukgrCode AND day_number = $dayNumber ORDER BY time_id ASC" );		
	}	
	
	private function initDuoDates(){
		$dayNumber = intval(trim($this->globals['paths'][1]));		
		
		
		$resultDatum = $this->connection->fetchAssocRows("SELECT * FROM `view_next_dates` WHERE day_number = $dayNumber ");
		$this->data['DatumInfo'] = $resultDatum[0];		
		$day_number = (1 + intval(date('N'))) % 7;		
		$today = getdate();
		$hour  = $today['hours'];
		$realDate = $resultDatum[0]['next_event'];
		$this->data['DatumInfo'] = $resultDatum[0];

		$dateSrc  = $realDate .' 12:00 CET';
		$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
		$DateDisplay = date_format($dateTime, 'd-m-Y');						                        
		$dayValue = strftime ( '%A', $dateTime->getTimestamp());
				
		$this->data['headerDateTime'] = $this->data['DatumInfo']['day_name']; 
		$this->data['dayNumber'] = $dayNumber;		
		$this->data['nextevent'] = $this->data['DatumInfo']['next_event']; 
	}
	
	private function fetchUkgrLocations(){
		$this->data['locations'] = array();		
		
		$locresult = $this->connection->fetchAssocRows('SELECT * FROM location');
		foreach($locresult as $row) {
			  $this->data['locations'][]  = new Location($row);
		}
	}
	
	public function sendWeekMail($naam, $email, $gsm, $ukgrCode){
		$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
		 
		if(strlen(trim($email)) == 0 || !preg_match($email_exp, trim($email))) {
			return false;
		}		
		
		$query = "SELECT A.*,C.start_time FROM `service_dayinfo` A
							INNER JOIN view_next_week B ON B.next_event = A.service_date 
							INNER JOIN service_time C ON C.id = A.time_id 
							WHERE A.naam = '$naam'	AND A.gsm='$gsm' ORDER BY A.service_date ASC";	
		$weekResults = 	$this->connection->fetchAssocRows($query );		
		
		if (count($weekResults) == 0) {
			return;
		}		
		$itemCollector = array();
		foreach($weekResults as $dayResult) {
			$serviceDate = $dayResult['service_date'];
			$timeToUse   = substr($dayResult['start_time'], 0, 5 );
			$dateSrc  = $serviceDate ." $timeToUse CET";
			$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
			$DateDisplay = date_format($dateTime, 'd-m-Y');						                        
			$dayValue = strftime ('%A  %e %B %Y', $dateTime->getTimestamp()). " [$timeToUse] ";
			
			$startTime  = "$dayValue $DateDisplay";
			
			$itemResult = file_get_contents($this->globals['root']."/resources/dayItem.html");	
			$itemResult = str_replace('#STARTTIME', $dayValue, $itemResult);			
			$itemResult = str_replace('#RESERVERINGSCODE', $dayResult['rescode'], $itemResult);
			$itemResult = str_replace('#INTERNALID', $dayResult['id'], $itemResult);
			$itemCollector[] = $itemResult;
		}
		$inhoud = implode("\n", $itemCollector );					
				
		$locresult = $this->connection->fetchAssocRows("SELECT * FROM location WHERE ukgr_code = $ukgrCode")[0];	
		        
        $email_message = file_get_contents($this->globals['root']."/resources/template.html");		
		$email_message = str_replace('#TITLE', "Uw Reservering voor de diensten van de UKGR", $email_message);		
		$email_message = str_replace('#CITY', $locresult['city'], $email_message);
		$email_message = str_replace('#ADDRESS', $locresult['address'], $email_message);
		$email_message = str_replace('#FULLNAME', $naam, $email_message);
		$email_message = str_replace('#TELEFOON', $gsm, $email_message);
		$email_message = str_replace('#INHOUD', $inhoud, $email_message);
		 
		// create email headers
		$headers = "From: " . strip_tags($this->email_from) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($this->email_from) . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$result = @mail($email, "Reservering voor UKGR diensten", $email_message, $headers);  
		return $result;		
	}	
	
}
