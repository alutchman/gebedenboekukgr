<?php

class LoginService  extends BaseService {
	public $errorMessage = "";
	public $user = "";
	
	public function getLevel() {
		return 'viewer';
	}
	 
	
	public function show(){
		$this->renderView(__FUNCTION__, 'loginTemplate');
	}
	
	public function validate(){		
		if (isset($this->post['username']) && isset($this->post['password'])  && isset($this->post['captcha']) ) {
			$this->user = $this->post['username'];			
			$captchaSession = $this->session['digit'];
			$captchaPost = strtoupper($this->post['captcha']); 
			$capchaOK = false;

			$this->error_message = "";

			if($captchaSession === md5(trim($captchaPost)))  {
				$capchaOK = true;
			}		
		    if (!$capchaOK) {
				$this->errorMessage = "Ongeldige Captcha.";
			} else {
				$query = sprintf("SELECT * FROM users WHERE login_id = '%s'", $this->post['username']); 
				$userDataList = $this->connection->fetchAssocRows($query );
				if (count($userDataList) != 1) {
					$this->errorMessage = "Gebruiker onbekend of <br /> ongeldig wachtwoord.";
				} else {
					$userData = $userDataList[0];			
				
					if (password_verify($this->post['password'], $userData['password'])) {
					   //echo 'Password is valid!';
					
					   $_SESSION['user_id']  = $userData['id'];
					   $_SESSION['login_id'] = $this->post['username'];
					   $_SESSION['level']    = $userData['level'];
					   $_SESSION['fullname'] = $userData['fullname']; 
					   $this->redirect('location','show');					 
					} else {
						$this->errorMessage = "Gebruiker onbekend of <br /> ongeldig wachtwoord.";
					}
				}		
			}
		}		
		$this->renderView('show', 'loginTemplate');		
	}
	
	
	public function logout(){
		session_unset();
		session_destroy();
		$_SESSION = array();
		$this->redirect('login', 'show');
	}
	
}
