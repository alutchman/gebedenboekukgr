<?php



class PlanningService  extends BaseService {
	
	public  function getLevel() {
		return 'viewer';
	}
	
	
	public function show(){
		$this->data['lastUkgrCode']  = isset($this->session['lastUkgrCode'])  ? $this->session['lastUkgrCode']  : 1;		
		$this->data['locations'] = 
		$this->connection->fetchAssocRows('SELECT * FROM location WHERE active = true ORDER BY ukgr_code ASC');
		
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}
	
	public function dates(){
		$result = $this->connection->fetchAssocRows("SELECT DATE_ADD(CURDATE(),INTERVAL 7 DAY) AS now_nextweek");
		$defaultDate = $result[0]['now_nextweek'];
		$this->data['nextEventDate'] = isset($this->session['nextEventDate']) ? $this->session['nextEventDate'] : date($defaultDate);
				
		$result = $this->connection->fetchAssocRows(sprintf("SELECT DAYOFWEEK('%s')  AS day_number", $this->data['nextEventDate']));
		$dayNumber = $result[0]['day_number'];
		
		$this->data['planningDayNumber'] = $dayNumber;
		
		$lastUkgrCode = $this->request['ukgr_code'];
					
		$datesFromPHPView = $this->getDateFromPHPView();		
		
		$query = "SELECT A. * FROM ($datesFromPHPView) A WHERE 
						A.next_event >= current_date() 
						AND (day_number != 7 OR A.next_event IN (SELECT datum FROM feestdagen)) 
						AND 
							(
								day_number IN (SELECT distinct day_number FROM location_times where ukgr_code=$lastUkgrCode GROUP BY ukgr_code, day_number)
								OR
								day_number IN 
									(
										SELECT distinct day_number FROM view_feestdag_times 
											where 
												ukgr_code = $lastUkgrCode AND datum = A.next_event
											GROUP BY ukgr_code, day_number
									
									)			    
							)			
						ORDER BY A.next_event ASC LIMIT 16";				

		$this->data['dates'] = $this->connection->fetchDetailedData($query);
		$this->renderView(__FUNCTION__);	
	
	}
	
	
	private function getDateFromPHPView() {
		$selections = array();
		$iterQuery = "SELECT DATE_ADD(curdate(), INTERVAL %d DAY)  AS 'next_event',	DAYOFWEEK(DATE_ADD(curdate(), INTERVAL %d DAY) )  AS day_number";
		for ($delta = 0; $delta <= 20; $delta++) {
		  $selections[] = sprintf($iterQuery, $delta , $delta);
		} 
		$data = implode(" UNION ", $selections)		;
		
		return $data ;		
	}
	
	
	private function viewNextEventFixedDateAndUkgrCode($ukgrCode, $serviceDate){
		$resultFeast = $this->connection->fetchAssocRows("SELECT * FROM feestdagen WHERE datum = '$serviceDate' AND active = TRUE ");
		$tabletimes = 'location_times';
		
		$isFeestDag = (count($resultFeast) == 1);
		$feestdagParam = $isFeestDag ? 'J' : 'N';

		if (count($resultFeast) > 0) {
			$tabletimes = 'view_feestdag_times';

		}


		$query = "	SELECT 
						K.*,
						IF(E.current_count IS NULL,0, E.current_count) as current_count 

					FROM 
						(
							SELECT D.ukgr_code, D.day_number, D.time_id,  C.next_event, D.start_time ,D.city, D.address,D.capacity
							FROM 
								(
								   SELECT 
										Z.ukgr_code, Z.city, Z.address,Z.capacity,V.time_id,V.day_number, W.start_time
								   FROM $tabletimes  V
								   INNER JOIN service_time W ON W.id = V.time_id
								   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code	
								)  D	
								INNER JOIN		
								(
									SELECT 	DATE('$serviceDate') AS next_event, 	DAYOFWEEK(DATE('$serviceDate' ))  AS day_number													
								) C  			
								ON D.day_number= C.day_number 
								WHERE D.ukgr_code = $ukgrCode
						)	
						K
					LEFT JOIN
						(
							SELECT DISTINCT
								A.ukgr_code,		
								A.service_date, 
								A.day_number,						
								A.time_id, 
								COUNT(*) AS current_count
							FROM service_dayinfo A
							WHERE A.ukgr_code = $ukgrCode
							GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id						
						) E ON E.ukgr_code = K.ukgr_code AND E.service_date = K.next_event AND E.day_number = K.day_number AND  E.time_id = K.time_id
				ORDER BY 
				  K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC";
	    return $query;				

	}

	
	
	public function addFromPrev(){
		$accesLevelItem = new AccesLevel('viewer');
		if (count($this->paths) < 2  || $this->accesLevelUser->getClearance() > $accesLevelItem->getClearance()){
			$this->redirect($this->basedir);
			return;
		}			
		
		$reserveId = intval($this->paths[0]);	
		$newDate   = trim($this->paths[1]);	
        $personSearch = $this->connection->fetchAssocRows("SELECT A.* FROM `service_dayinfo` A  WHERE id = $reserveId");
		if (count($personSearch) > 0) {
			$person = $personSearch[0];

			$ukgrCode    = $person['ukgr_code'];
			$dayNumber   = $person['day_number'];			
			$serviceDate = $newDate ;
			$timeId      = $person['time_id'];		
			$qry = $this->viewNextEventFixedDateAndUkgrCode($ukgrCode, $serviceDate);	
			$timesQuery = "SELECT (A.capacity - A.current_count) AS free  
							FROM ($qry) A WHERE A.ukgr_code = %d AND A.next_event='%s' AND A.time_id = %d";				
			$StatTimes = $this->connection->fetchAssocRows(sprintf($timesQuery, $ukgrCode , $serviceDate, $timeId ));			
			$free = intval($StatTimes[0]['free']);
			
			if ($free > 0) {
				$naam = $person['naam'];
				$rescode = $person['rescode'];
				$email = trim($person['email']) == '' ? null : $person['email'];
				$gsm = $person['gsm'];
				$memberId = intval(trim($person['member_id'])); 
				
				$query = "";
				
				if ($email == null && $memberId == 0) {
					$query = "INSERT INTO service_dayinfo 
								(ukgr_code, day_number, service_date, time_id, naam, rescode, gsm) VALUES 
								($ukgrCode, $dayNumber, '$serviceDate',$timeId, '$naam', '$rescode', '$gsm')";
							
				} if ($email != null && $memberId == 0) {
					$query = "INSERT INTO service_dayinfo 
								(ukgr_code, day_number, service_date, time_id, naam, rescode, gsm, email) VALUES 
								($ukgrCode, $dayNumber, '$serviceDate',$timeId, '$naam', '$rescode', '$gsm', '$email')";
							
				} else if ($email == null && $memberId != 0) {
					$query = "INSERT INTO service_dayinfo 
								(ukgr_code, day_number, service_date, time_id, naam, rescode, gsm, member_id) VALUES 
								($ukgrCode, $dayNumber, '$serviceDate',$timeId, '$naam', '$rescode', '$gsm', $memberId)";
				} else {
					$query = "INSERT INTO service_dayinfo 
								(ukgr_code, day_number, service_date, time_id, naam, rescode, gsm, email, member_id) VALUES 
								($ukgrCode, $dayNumber, '$serviceDate',$timeId, '$naam', '$rescode', '$gsm', '$email', $memberId)";
				}
				$this->connection->handleInsert($query );		
			}
			
		}
		$this->redirect($this->basedir);
	}
	
	public function addFromVisitorPref(){
		$accesLevelItem = new AccesLevel('viewer');
		if (count($this->paths) < 5  || $this->accesLevelUser->getClearance() > $accesLevelItem->getClearance()){
			$this->redirect($this->basedir);
			return;
		}			
		
		$reserveId     = intval($this->paths[0]);	
		$serviceDate   = trim($this->paths[1]);	
		$dayNumber     = intval($this->paths[2]);	
		$timeId        = intval($this->paths[3]);	
		$PreftimeId    = intval($this->paths[4]);	
		
		$planNextWeekViP  = new PlanNextWeekViP	($this->connection);
		$planNextWeekViP->addPersonVip($serviceDate, $dayNumber, $timeId,   $reserveId, $PreftimeId);			
		$this->redirect($this->basedir);		
	}

	
	private function viewNextEvent($event_date, $ukgrCode){
		$locTimeTable = 'SELECT * FROM  location_times';
		$query0 = "SELECT * FROM feestdagen WHERE datum = '$event_date' and active = TRUE ";
		$result = $this->connection->fetchAssocRows($query0);
		if (count($result) == 1) {
			$ControleSql = "SELECT COUNT(*) as aantal FROM view_feestdag_times WHERE datum = '$event_date' AND ukgr_code = $ukgrCode";
		  	$times4Search = $this->connection->fetchAssocRows($ControleSql);
			$aantal = $times4Search[0]['aantal'];		

			if ($aantal == 0) {
				$href = $this->getHrefServiceAction('location', 'feestdagen', $ukgrCode);
				throw new Exception("Geen tijden ingesteld voor de geslecteerde feestdag. Pas dat nu <a href=\"$href\">hier</a> aan.");
			}
			$locTimeTable = "SELECT ukgr_code,day_number, time_id FROM view_feestdag_times 
								WHERE ukgr_code = $ukgrCode and datum = '$event_date'";
		}
				
		$datesFromPHPView = $this->getDateFromPHPView();
		$query = "	SELECT 
						K.*,
						IF(E.current_count IS NULL,0, E.current_count) as current_count  
					FROM 
						(
							SELECT D.ukgr_code, D.day_number, D.time_id,  C.next_event, D.start_time ,D.city, D.address,D.capacity
							FROM 
								(
								   SELECT 
										Z.ukgr_code, Z.city, Z.address,Z.capacity,V.time_id,V.day_number, W.start_time
								   FROM ($locTimeTable) V
								   INNER JOIN service_time W ON W.id = V.time_id
								   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code		   
								)  D	
								INNER JOIN		
								(
									$datesFromPHPView																
				  
								) C  			
								ON D.day_number= C.day_number 
						)	
						K
					LEFT JOIN
						(
							SELECT DISTINCT
								A.ukgr_code,		
								A.service_date, 
								A.day_number,						
								A.time_id, 
								COUNT(*) AS current_count
							FROM service_dayinfo A
							GROUP BY A.ukgr_code,A.service_date,  A.day_number, A.time_id
						) E ON E.ukgr_code = K.ukgr_code AND E.service_date = K.next_event AND E.day_number = K.day_number AND  E.time_id = K.time_id

				ORDER BY 
				  K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC";
	    return $query;				
	}
	
	
	public function times(){
		$nextEventDate = $this->request['nextEventDate'];	
		$ukgrCode      = $this->request['ukgr_code'];	

		$_SESSION['lastUkgrCode'] = $ukgrCode;
		$_SESSION['nextEventDate'] = $nextEventDate;
		try {
			$aQuert = $this->viewNextEvent($nextEventDate, $ukgrCode );	
								
			$timesQuery = "SELECT A.time_id, substring(A.start_time, 1,5) as time_of_event, (A.capacity - A.current_count) AS free  
								FROM ($aQuert) A WHERE A.ukgr_code = %d AND A.next_event='%s' ORDER BY A.time_id ASC";
			$this->data['times'] = $this->connection->fetchAssocRows(sprintf($timesQuery, 
				$ukgrCode , $nextEventDate));
				
			$firstTime = 	$this->data['times'][0]['time_id'];
			$this->data['lastPlanningTimeId']  = isset($this->session['lastPlanningTimeId'])  ? $this->session['lastPlanningTimeId']  : $firstTime ;	
			$this->renderView(__FUNCTION__);
		}  catch (Exception $e) {
			$extendedError = new ExtendedException($e);
			echo $extendedError->getMessage();
		} 	
	}
	
	
	public function overzicht(){
		$capacity      = intval($this->request['capacity']);	
		$nextTimeId    = $this->request['nextTimeId'];	
		$nextEventDate = $this->request['nextEventDate'];	
		$ukgrCode      = $this->request['ukgr_code'];
		$_SESSION['lastPlanningTimeId'] = $nextTimeId;
		
	

		$query = "SELECT A.* FROM `service_dayinfo` A 
					WHERE A.ukgr_code = $ukgrCode AND A.service_date = '$nextEventDate' AND A.time_id = $nextTimeId    ORDER BY A.naam ASC";
		$this->data['bezoekers'] = $this->connection->fetchAssocRows($query);		
				
		$dateSrc     = $nextEventDate.' 12:00 CET';
		$dateTime    = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
		$itemDate    = date_format($dateTime, 'd-m-Y');	
		$lastDayName = strftime ( '%A', $dateTime->getTimestamp());
		
		$this->data['current_event']    = "$lastDayName $itemDate" ;
		$this->data['planningDate']     = $nextEventDate;
		$this->data['planningTimeId']   = $nextTimeId;
		$this->data['planningUkgrCode'] = $ukgrCode;
		
		$this->data['available'] = $capacity - count($this->data['bezoekers']) ;
		$query = "SELECT DAYOFWEEK(DATE('$nextEventDate'))  AS day_number ";		
		$resultDatum = $this->connection->fetchAssocRows($query);
		$dayNumber = $resultDatum[0]['day_number'];
		
		//For specials days look at Sunday....
		$query0 = "SELECT * FROM feestdagen WHERE datum = '$nextEventDate' ";
		$result = $this->connection->fetchAssocRows($query0);
		if (count($result) == 1) {
			$dayNumber = 1;
		}
		
		// === initialize		
		$this->data['fromPrevVisit'] = array();
		$this->data['preferences'] = array();
        if ($this->data['available'] > 0)  {
			$prevWeekData = $this->fetchLastWeekVisitors($nextTimeId, $nextEventDate, $ukgrCode);
			$this->data['fromPrevVisit'] = 	$prevWeekData['data'];
			$this->data['prev_event']    = $prevWeekData['show_date'];
			
			
			$dataByPref = array();
			$query = "SELECT A.day_number,
							B.id, B.ukgr_code, B.gsm, B.gender,
							B.street, B.house_number, B.extra_address,B.zipcode, B.city,
							B.first_name, B.last_name, 
							SUBSTRING(K.start_time , 1, 5) as start_time,
							A.time_id
						FROM member_preference A
						INNER JOIN service_time K ON K.id = A.time_id	
						INNER JOIN visitors B ON B.id = A.member_id
							WHERE 
								A.day_number = $dayNumber 
								AND A.time_id <=  $nextTimeId  
								AND B.ukgr_code = $ukgrCode 
								AND B.id NOT IN (
									SELECT IF(D.member_id IS NULL,-1, D.member_id)
									 FROM `service_dayinfo` D 
									 WHERE 
										D.ukgr_code = $ukgrCode AND 
										D.service_date = '$nextEventDate' AND 
										D.time_id_member = A.time_id 			
							 ) 
					ORDER BY K.start_time DESC, B.first_name ASC, B.last_name  ASC ";								
			$this->data['preferences']    = 	$this->connection->fetchAssocRows($query);		
		}
		$this->renderView(__FUNCTION__);
	}
	
	private function fetchLastWeekVisitors($nextTimeId, $nextEventDate, $ukgrCode){
		$result = $this->connection->fetchAssocRows(
			sprintf("SELECT DATE_ADD(date('%s'),INTERVAL -7 DAY) AS prev_date", $nextEventDate));
			
     	$DateLastTime = $result[0]['prev_date'];	 	
	
		$dateSrc  = $DateLastTime.' 12:00 CET';
		$dateTime = new DateTime($dateSrc, new DateTimeZone('Europe/Amsterdam')); 				
		$itemDate = date_format($dateTime, 'd-m-yy');	
		$lastDayName = strftime ( '%A', $dateTime->getTimestamp());
		$showDate = "$lastDayName $itemDate";
		
		$query = "SELECT B.* FROM `service_dayinfo` B 
					WHERE 
						B.ukgr_code = $ukgrCode AND 
						B.service_date = '$DateLastTime' AND 
						B.time_id = $nextTimeId  AND
						B.member_id IS NULL AND
						B.naam NOT IN 
						(
							SELECT A.naam FROM `service_dayinfo` A 
							 WHERE 
								A.ukgr_code = $ukgrCode AND 
								A.service_date = '$nextEventDate' AND 
								A.time_id = $nextTimeId 
						
						)
						ORDER BY B.naam ASC";										
		return array('data'=>$this->connection->fetchAssocRows($query), 'show_date'=>$showDate);	
	}

}
