<?php

class RubriekService  extends BaseService {
	public function getLevel() {
		return 'extern';
	}	

	
	public function show(){		
		$this->data['rubrieken'] = $this->connection->fetchAssocRows('SELECT * FROM load_items order by sequence ASC');
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	

	public function edit(){
		$editID = $this->globals['paths'][0];
		$list = $this->connection->fetchAssocRows('SELECT * FROM load_items WHERE id = '.$editID );
		if (count($list ) == 0 ) {
			 throw new Exception("Rubriek met ID =  $editID is niet aangetroffen.");
		} 
		$this->data['rubriek'] = $list[0];
		
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	
	public function handleEdit(){
		$active = isset($this->post['active'])	? 'TRUE' : 'FALSE';			
		
		$this->connection->handleUpdate(sprintf("UPDATE load_items SET active=%s,  title='%s', sequence=%d WHERE id=%d", 
								$active, trim($this->post['title']), $this->post['sequence'], $this->post['id']));		
		$this->redirect($this->basedir, 'show');
		
	}
	
		
	public function add(){	
		$list = $this->connection->fetchAssocRows('SELECT MAX(sequence) AS next_sequence FROM load_items ' );
		if (count($list) > 0 ) {
			$this->post['sequence'] = 1 + $list[0]['next_sequence'];
		} 
		
		
		$this->data['errors']  = false;
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
		
	public function handleAdd(){
		$this->post['active'] = isset($this->post['active'])	? 'TRUE' : 'FALSE';			
		$load_group = strtoupper(trim($this->post['load_group']));
		$list = $this->connection->fetchAssocRows("SELECT * FROM load_items WHERE load_group = '$load_group '" );
		if (count($list ) > 0 ) {
			$this->data['errors']  = "Groep moet uniek zijn en '$load_group' blijkt reeds te bestaan.";
			$this->renderView('add', 'baseTemplate');
			return;
		} 	
		
		$this->connection->handleInsert(sprintf("INSERT INTO load_items (load_group, load_type, title, sequence, active)  VALUES('%s','%s', '%s', %d , %s)", 
				$load_group, trim($this->post['load_type']), trim($this->post['title']), $this->post['sequence'], $this->post['active']));		
		$this->redirect($this->basedir, 'show');
	}
	
	public function items(){
		$media_group = $this->globals['paths'][0];
		$this->data['media_group'] = $media_group;
		$this->data['items'] = $this->connection->fetchAssocRows("SELECT * FROM media_data WHERE media_group = '$media_group' ORDER BY sequence ASC " );
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	
	public function edititem(){
		$editID = $this->globals['paths'][0];
		$list = $this->connection->fetchAssocRows('SELECT * FROM media_data WHERE id = '.$editID );
		if (count($list ) == 0 ) {
			 throw new Exception("Media met ID =  $editID is niet aangetroffen.");
		} 
		$this->data['item'] = $list[0];
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	public function handleItemEdit(){
		$active = isset($this->post['active'])	? 'TRUE' : 'FALSE';			
		$query = sprintf("UPDATE media_data SET active=%s,  title='%s', 
												sequence=%d, source = '%s', anchor = '%s' WHERE id=%d", 
								$active, trim($this->post['title']), $this->post['sequence'], $this->post['source'],
								 $this->post['anchor'], $this->post['id']);
		$this->connection->handleUpdate($query );		
		$this->redirect($this->basedir, 'items/'.$this->post['media_group']);
	}
	
	
	public function additem(){
		$media_group = $this->globals['paths'][0];
		$this->data['media_group'] = $media_group;
		
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	
	public function handleItemAdd(){
		$active = isset($this->post['active'])	? 'TRUE' : 'FALSE';		
		$media_group = $this->globals['paths'][0];	
		
		
		$query = sprintf("INSERT INTO media_data (media_group, active, title, sequence, source, anchor) 
											values ('%s', %s, '%s', %d, '%s', '%s')", 
								$media_group, $active, trim($this->post['title']), 
								$this->post['sequence'], $this->post['source'],	 $this->post['anchor']);
		$this->connection->handleUpdate($query );		
		$this->redirect($this->basedir, 'items/'.$media_group);
		
		
	}
	
	
	
}
