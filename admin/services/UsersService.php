<?php

class UsersService  extends BaseService {
	CONST DEFAULT_PW = 'Welkom01';
	/*
	 * initial password = Welkom01
	 * $orgPassword = "Welkom01";
	 * $pwd_hashed = password_hash($passwordOrg,  PASSWORD_DEFAULT);
	 * if (password_verify($this->post['password'], $userData['password'])) {
	 *	   echo "Password verified!";
	 * 	}
	 * 
	 */
	
	protected $errorMsg  = "";
	
	
	public function getLevel() {
		return 'pastors';
	}
	 

	public function show(){
		if (isset($this->post['personToReset'])) {
			$delKeys = array_keys($this->post['personToReset']);
			if (count($delKeys) == 1)  {
				$delId = $delKeys [0];
				$this->redirect('users','reset','/'.$delId);	
				return;
			}
		}	
		$this->data['users'] =  $this->connection->fetchAssocRows("SELECT * FROM users ");		
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}	
	
	public function reset(){		
		if (count($this->globals['paths']) == 0  || strlen(trim($this->globals['paths'][0])) == 0  ) {
			throw new Exception("Ongeldige bewerking uitgevoerd.");
		}
		$resetId = intval($this->globals['paths'][0]);		
		
		if ($_SESSION['level'] == 'admin' ) {	
			
			$pwd_hashed = password_hash(self::DEFAULT_PW,  PASSWORD_DEFAULT);
			$query = "UPDATE users SET  password='%s'  WHERE id=%d";
			$queryExec = sprintf($query, $pwd_hashed, $resetId);
			$this->connection->handleUpdate($queryExec);		
		}
		$this->redirect('users','show');	
	}
	
	
	public function personAdd(){	
		if ($_SESSION['level'] == 'admin') {
			$this->post['login_id'] = '';	
			$this->post['fullname'] = '';	
			$this->post['password1'] = '';	
			$this->post['password2'] = '';	
			$this->post['level'] = 'admin';	
			$this->renderView(__FUNCTION__, 'baseTemplate');			
		}else {
			$this->redirect('users','show', '/?access=DENIED');	
		}

	}
	
	public function personAddHandle(){
		extract($this->post);	
		if (strlen(trim($login_id)) == 0)  {
			$this->errorMsg .= "Login ID is niet ingevuld.\n";
		}
		if (strlen(trim($fullname)) == 0)  {
			$this->errorMsg .= "Uw Naam is niet ingevuld.\n";
		}
		if (strlen(trim($password)) == 0)  {
			$this->errorMsg .= "Uw 1e wachtwoord is niet ingevuld.\n";
		}
		if (strlen(trim($password2)) == 0)  {
			$this->errorMsg .= "Uw 2e wachtwoord is niet ingevuld.\n";
		}	
		
		if (strlen(trim($password)) != 0  && strlen(trim($password2)) != 0   && $password !== $password2)  {
			$this->errorMsg .= "Uw wachtwoorden zijn niet gelijk.\n";
		}
		if (strlen($this->errorMsg) > 0) {
			$this->renderView('personAdd', 'baseTemplate');
			return;
		}
		$users =  $this->connection->fetchAssocRows("SELECT * FROM users where login_id='$login_id'");	
		if (count($users) > 0) {
			$this->errorMsg .= "Login ID is al in gebruik. \n";
			$this->renderView('personAdd', 'baseTemplate');
			return;
		}	
		$query = "INSERT INTO users 
			(login_id, password, fullname, level) 
			values('%s', '%s', '%s', '%s')";
		try {
			$pwd_hashed = password_hash($password,  PASSWORD_DEFAULT);
			$queryExec = sprintf($query, $login_id, $pwd_hashed, $fullname, $level);			
			$this->connection->handleInsert($queryExec);
			$this->show();
		} catch (Exception $e) {
			$extendedError = new ExtendedException($e);
			$this->errorMsg .= $extendedError->message;
			$this->renderView('personAdd', 'baseTemplate');
		}				
	}
	
	
	public function personEdit(){
		if (count($this->globals['paths']) == 0  || strlen(trim($this->globals['paths'][0])) == 0  ) {
			throw new Exception("Ongeldige bewerking uitgevoerd.");
		}
		$loginId = intval($this->globals['paths'][0]);
		if ($loginId == 0 )  {
			throw new Exception("Ongeldige bewerking uitgevoerd. (Login id 0 is niet toegestaan)");
		}
		
		$users   =  $this->connection->fetchAssocRows("SELECT * FROM users where id=$loginId");	
		if (count($users) == 0)  {
			throw new Exception("Ongeldige bewerking uitgevoerd. (Login id $loginId is niet aangetroffen)");
		}	
		$userData = $users[0];
		
		if (($_SESSION['level'] == 'admin' && $userData['login_id'] != 'admin') ||  $loginId == $_SESSION['user_id']) {

			$this->post['id']        = $userData['id'];
			$this->post['login_id']  = $userData['login_id'];	
			$this->post['fullname']  = $userData['fullname'];		
			$this->post['password1'] = '';	
			$this->post['password2'] = '';	
			$this->post['level'] = $userData['level'];
			$this->renderView(__FUNCTION__, 'baseTemplate');			
		} else {
			$this->redirect('users','show', '/?access=DENIED');	
		}

	}
	
	
	public function personEditHandle(){
		$this->post['referrer']	   = $_SERVER['HTTP_REFERER'] ;		
		
		extract($this->post);	
		
		if (strlen(trim($login_id)) == 0)  {
			$this->errorMsg .= "Login ID is niet ingevuld.\n";
		}
		if (strlen(trim($fullname)) == 0)  {
			$this->errorMsg .= "Uw Naam is niet ingevuld.\n";
		}	
		
		if (strlen(trim($password)) != 0  && strlen(trim($password2)) != 0   && $password !== $password2)  {
			$this->errorMsg .= "Uw wachtwoorden zijn niet gelijk.\n";
		}
		if (strlen($this->errorMsg) > 0) {
			$this->renderView('personEdit', 'baseTemplate');
			return;
		}
		$users =  $this->connection->fetchAssocRows("SELECT * FROM users where login_id='$login_id'  AND id != $id");	
		if (count($users) > 0) {
			$this->errorMsg .= "Login ID is al in gebruik. \n";
			$this->renderView('personEdit', 'baseTemplate');
			return;
		}		
		$updateWachtWoord = strlen(trim($password)) != 0  && strlen(trim($password2)) != 0 ;
		try {
			if ($updateWachtWoord) {
				$pwd_hashed = password_hash($password,  PASSWORD_DEFAULT);
				$query = "UPDATE users SET login_id='%s', fullname='%s', password='%s', level='%s'  WHERE id=%d";
				$queryExec = sprintf($query, $login_id, $fullname, $pwd_hashed, $level, $id);			
				$this->connection->handleUpdate($queryExec);				
			} else {
				$query = "UPDATE users SET login_id='%s', fullname='%s', level='%s'  WHERE id=%d";
				$queryExec = sprintf($query, $login_id, $fullname, $level, $id);			
				$this->connection->handleUpdate($queryExec);	
					
			}
			if ($id === $_SESSION['user_id']) {					
				$_SESSION['fullname'] = $fullname; 
				$_SESSION['login_id'] = $login_id; 
				$_SESSION['level'] = $level; 
			}		
			$this->redirect('users','show');	
		} catch (Exception $e) {
			$extendedError = new ExtendedException($e);
			$this->errorMsg .= $extendedError->message;
			$this->renderView('personEdit', 'baseTemplate');
		}					
	}
	
}
