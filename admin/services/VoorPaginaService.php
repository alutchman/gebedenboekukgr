<?php

class VoorPaginaService  extends BaseService {
	public function getLevel() {
		return 'extern';
	}
		

	
	public function show(){		
		$this->data['pageParts'] = $this->connection->fetchAssocRows('SELECT * FROM pageparts');
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	public function edit(){
		$editID = $this->globals['paths'][0];
		$list = $this->connection->fetchAssocRows('SELECT * FROM pageparts WHERE id = '.$editID );
		if (count($list ) == 0 ) {
			 throw new Exception("Geen Date voor ID =  $editID");
		} 
		$this->data['pagePart'] = $list[0];
		
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	

	public function handleEdit(){
		$this->connection->handleUpdate(sprintf("UPDATE pageparts SET data='%s' WHERE id=%d", trim($this->post['data']), $this->post['id']));		
		$this->redirect($this->basedir, 'show');
	}
	
	
	public function add(){	
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}
	
	
	public function handleAdd(){
		$this->connection->handleInsert(sprintf("INSERT INTO pageparts (tag, data)  VALUES('%s','%s')", trim($this->post['tag']), trim($this->post['data'])));		
		$this->redirect($this->basedir, 'show');
	}
	
}
