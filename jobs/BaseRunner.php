<?php


abstract class BaseRunner {
	protected $connection;
	protected $debug = false;
	private $logFile;
	private $resourceStd;
	
	function __construct(&$connection){		
		$this->connection = $connection;
		
		$this->logFile = dirname(dirname(__FILE__)).'/jobs/logs/PlanNextWeekViP.log';	
		
		touch($this->logFile);
		$this->resourceStd = fopen($this->logFile, 'a');	
		$this->log("====================================================================================================");
	}
	
	function __destruct(){
		fclose($this->resourceStd);
		chmod($this->logFile, 0755);
	}
	
	
	public function log($data2log) {
		$date = date('Y-m-d H:i:s');
		fwrite($this->resourceStd, "$date: $data2log\n"); 
	}
	
	
	
}
