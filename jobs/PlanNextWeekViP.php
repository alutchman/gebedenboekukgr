<?php

require_once('BaseRunner.php');

class PlanNextWeekViP extends BaseRunner {
	
	public function setupFuture($ukgrCode, $capacity, $serviceDate, $dayNumber, $nextTimeId, $uurMin ) {
		$dataByPref = array();
		

		$query = "SELECT 
					B.id, B.ukgr_code, B.gsm, B.gender,
					B.street, B.house_number, B.extra_address,B.zipcode, B.city,
					B.first_name, B.last_name, 
					SUBSTRING(K.start_time , 1, 5) as start_time,
					A.time_id,A.day_number
				FROM member_preference A
				INNER JOIN visitors B ON B.id = A.member_id						
				INNER JOIN service_time K ON K.id = A.time_id	
					WHERE 
						A.day_number = $dayNumber 
						AND B.activated = TRUE
						AND (A.time_id <=  $nextTimeId  )
						AND B.ukgr_code = $ukgrCode 
						AND B.id NOT IN (
											SELECT coalesce(D.member_id ,-1)
											 FROM `service_dayinfo` D 
											 WHERE 
												D.ukgr_code = $ukgrCode AND 
												D.service_date = '$serviceDate' AND 
												D.time_id_member >= A.time_id 			
										) 
							
			ORDER BY K.start_time DESC, B.first_name ASC, B.last_name  ASC ";			
		if ($this->debug) {
		  $this->log($query);
		}					
		$preferences    = 	$this->connection->fetchAssocRows($query);		
		
		
		$gereserveerd = 0;		
		foreach ($preferences as $prefSchedule) {
			$PreftimeId = $prefSchedule['time_id'];
			$reserveId = $prefSchedule['id'];
			$bezet = $this->getOccupied($ukgrCode, $serviceDate, $nextTimeId);
			$free = $capacity - $bezet;
			if ($free == 0 ) break;
			$resultAdd = $this->addPersonVip($ukgrCode, $serviceDate, $dayNumber, $nextTimeId,   $reserveId, $PreftimeId);
			if ($resultAdd !== false) {
				$gereserveerd++;		
			}						
		}
		
		if (count($preferences) > 0)  {
			$this->log("Planning $serviceDate (daynumber = $dayNumber, tijd = $uurMin) ukgrCode = $ukgrCode met geslaagde reserveringen: $gereserveerd");
		}
			
	}
	
	public function addPersonVip($ukgrCode, $serviceDate, $dayNumber, $timeId,   $reserveId, $PreftimeId) {
		$personSearch = $this->connection->fetchAssocRows("SELECT id, ukgr_code, first_name, last_name, email, gsm  FROM visitors WHERE id = $reserveId ");
		if ($this->debug) {
			$this->log(print_r($personSearch, true));	
		}
		
		if (count($personSearch) > 0) {
			$person = $personSearch[0];			
			$ukgrCode    = $person['ukgr_code'];
			$naam     = $person['first_name'].' '.$person['last_name'];
			$rescode  = sprintf('VIP%03dX%dY%d', $person['id'], $ukgrCode,$dayNumber  );
			$email    = $person['email'];
			$gsm      = $person['gsm'];
			$memberId = $person['id'];
			$query = "INSERT INTO service_dayinfo 
							(ukgr_code, day_number, service_date, time_id, naam, rescode, gsm, email, member_id, time_id_member) VALUES 
							($ukgrCode, $dayNumber, '$serviceDate',$timeId, '$naam', '$rescode', '$gsm', '$email', $memberId, $PreftimeId)";
				
			if ($this->debug) {
			  $this->log($query);	
			}					
					
			return $this->connection->handleInsert($query );			
		}	
		return false;
	}
	
	
	private function getOccupied($ukgrCode, $serviceDate, $timeId) {
		$query = "SELECT COUNT(*) as bezet FROM service_dayinfo WHERE ukgr_code = $ukgrCode AND service_date = '$serviceDate' AND time_id = $timeId";
		$StatTimes = $this->connection->fetchAssocRows($query);		
		return intval($StatTimes[0]['bezet']); 	
	}
	
} 


