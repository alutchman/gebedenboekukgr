<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	mysqli_report(MYSQLI_REPORT_STRICT);
	$timeZone = 'Europe/Amsterdam';  // +1 hours
    date_default_timezone_set($timeZone); 
    setlocale(LC_ALL, 'nl_NL');
    setlocale(LC_TIME, 'nl_NL');
    $date = date('Y-m-d H:i:s');
 
    
	try {
        require_once(dirname(dirname(__FILE__)).'/admin/includes/Connection.php');
        require_once(dirname(dirname(__FILE__)).'/admin/includes/ExtendedException.php');
        require_once(dirname(dirname(__FILE__)).'/admin/includes/ExtendedError.php');
        

		require_once('PlanNextWeekViP.php');

		$connection = new Connection();		
		$planNextWeekViP  = new PlanNextWeekViP($connection);
		
		$result = $connection->fetchAssocRows('SELECT DATE_ADD(CURDATE(),INTERVAL 7 DAY) AS now_nextweek');
		$nowNextWeek = $result[0]['now_nextweek'];
		
     	$result = $connection->fetchAssocRows(sprintf("SELECT DAYOFWEEK('%s')  AS day_number", $nowNextWeek));
		$dayNumber = $result[0]['day_number'];
		
		$feestdag = false;
		
		$query0 = "SELECT * FROM feestdagen WHERE datum = '$nowNextWeek' and active = TRUE ";
		$result = $connection->fetchAssocRows($query0);
		$feestdag = count($result) == 1;

		
		$churches = $connection->fetchAssocRows('SELECT ukgr_code,capacity FROM location WHERE active = true ORDER BY ukgr_code ASC');
		$tijdData = $connection->fetchAssocRows("SELECT ADDTIME(CURTIME(),'1:00:00') as huidigeTijd");
		$tijdStipNu = $tijdData[0]['huidigeTijd'];
		
		$errstr = "$date: Job uitgevoerd voor $nowNextWeek";
		$planNextWeekViP->log($errstr);			
		
		
		foreach($churches as $church) {
			$ukgrCode = $church['ukgr_code'];
			$capacity = intval($church['capacity']);
			
			if ($feestdag) {
				$ControleSql = "SELECT time_id, start_time FROM view_feestdag_times WHERE datum = '$nowNextWeek' AND ukgr_code = $ukgrCode ORDER BY time_id ASC";
			} else {
				$ControleSql = "SELECT time_id, start_time FROM view_location_times WHERE day_number = $dayNumber AND ukgr_code = $ukgrCode ORDER BY time_id ASC";
			}				
			$tijden  = $connection->fetchAssocRows($ControleSql);

			foreach($tijden as $tijdStip) {
				$uurMin = $tijdStip['start_time']; 			
				$planNextWeekViP->setupFuture($church['ukgr_code'], $capacity, $nowNextWeek, $dayNumber, $tijdStip['time_id'], $uurMin  );							
			}
		}
		
	} catch (Exception $e) {
		$extendedError = new ExtendedException($e);	
		$errstr  = $extendedError->message;			
		$planNextWeekViP->log($errstr);	
		
	} catch (Error $error) {
        $extendedError = new ExtendedError($error);
        $errstr  = $extendedError->message;	
        $planNextWeekViP->log($errstr);	
	}

	/*
	 * 
	 * SELECT * FROM `service_dayinfo` WHERE ukgr_code =1 and `service_date`= DATE_ADD(CURDATE(),INTERVAL 7 DAY) and `time_id_member` is not null
	 * DELETE FROM `service_dayinfo` WHERE ukgr_code =1 and `service_date`= DATE_ADD(CURDATE(),INTERVAL 7 DAY) and `time_id_member` is not null
	 * /
	
