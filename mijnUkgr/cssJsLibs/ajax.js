var specialKeysGSM = new Array();
specialKeysGSM.push(8); //Backspace
specialKeysGSM.push(43); //Plus


var specialKeysNUMBERS = new Array();
specialKeysNUMBERS.push(8); //Backspace

function IsNumericNUMBER(e) {
	var keyCode = e.which ? e.which : e.keyCode
	var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeysNUMBERS.indexOf(keyCode) != -1);
	return ret;
}



function IsGSMNumeric(e) {
	var keyCode = e.which ? e.which : e.keyCode
	var ret = ((keyCode >= 48 && keyCode <= 57) || (e.shiftKey && keyCode==43) || specialKeysGSM.indexOf(keyCode) != -1);
	
	return ret;
}
	


$(document).ready(function(){
	$('form :input:text:visible:not(input[class*=filter]):first').focus();
});
