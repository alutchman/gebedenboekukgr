var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
		return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
	},
	any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	},
	Desktop :function() {
		return (!isMobile.Android() && isMobile.BlackBerry() && isMobile.iOS() && isMobile.Opera() && isMobile.Windows());
	}
};		

var mobile = isMobile.any();
var pageWidth;


function changeVideoSource(){
	var divOuter = $("div.audioOUTER");
	var selctInDiv = divOuter.find('select[name=selectChannel]');		
	var element = selctInDiv.find('option:selected'); 
	if (!element) {
	   return;	
	}
	var streamUrl = element.attr("value"); 		
	var href      = element.attr("user-href");
	var title     = element.attr("user-title");
	var type      = element.attr("user-type");
	
	
	var anchorx = divOuter.find("a.orgSite");		
	anchorx.attr('href', href);
	if (href.length > 0) {
		anchorx.show();
	} else {
		anchorx.hide();
	}	
	
  
	var video       = $('#player-tv')[0];
	video.pause();
	video.autoplay  = true;
	
	var isM38u      = streamUrl.search(".m3u8") > 0;	
	
	if (isM38u && !mobile && Hls.isSupported()) {		
		var hls = new Hls();
		hls.loadSource(streamUrl);
		hls.attachMedia(video);
		hls.on(Hls.Events.MANIFEST_PARSED, function () {
			video.play();
		});
	}  else {	
		$('#player-tv').html ("<source src=\""+streamUrl+"\" type=\""+type+"\" ></source>");
		video.load();
		video.play();
	}		
}

function getPageSize() {
  pageWidth = $('div.innerBody').width();
}

	
function updateMobileSize(){
	getPageSize();
	var limitedX = pageWidth;
	var limitedY = Math.ceil(limitedX * 1080/1920);
	$('#player-tv').css('width', limitedX+'px');
	$('#player-tv').css('height', limitedY+'px');
}	


$(window).on('resize', function() {		
	getPageSize();
	updateMobileSize();
	return false;
});


$("div.audioOUTER").find('select[name=selectChannel]').on('change', function() {
	changeVideoSource();	

});

$('#player-tv')[0].addEventListener('loadedmetadata', function () {
   video.play();
});
	




