<?php 
class Connection {	
	private $conn ;
	
	function __construct() {
		global $GLOBALS;		
		$this->conn = new mysqli($GLOBALS['SERVERNAME'], $GLOBALS['USERNAME'], $GLOBALS['PASSWORD'], $GLOBALS['DBNAME']);
		// Check connection
		if ($this->conn->connect_error) {
		  $this->conn = null;
		} else {
			$this->conn->set_charset('utf8');
		}
	}
	
	function __destruct(){
		if ($this->conn != null) {			
			$this->conn->close();
			$this->conn = null;
		}		
	}
	
	public function fetchAssocRows($sql){
		$result = array();		
		$resultSql = $this->conn->query($sql);		
		if ($resultSql == false ) {
			throw new Exception($this->conn->error ."<br/>\n for query $sql");
		}					
		while($row = $resultSql->fetch_assoc()) {
			$result[] = $row;
		}		
		return $result;
	}
	
	public function fetchDetailedData($queryRAW, $params = array()) {		
		$query = vsprintf($queryRAW, $params);			
			
		$resultSql  = $this->conn->query($query);	
		if ($resultSql == false ) {
			throw new Exception($this->conn->error ."\n for query $query");
		}						
		$fields     = $resultSql->fetch_fields();	
		$properties = array();
		
		$fieldNames = array();
		
		foreach($fields as $value) {
			$newKey = lcfirst(str_replace('_', '', ucwords($value->name, '_')));
			$properties[] = $newKey;
			$fieldNames[] = $value->name;
		}
		
		$result = array('data'=>array(),  'fields'=>$fieldNames, 'properties'=>$properties);		
			
		while($row = $resultSql->fetch_assoc()) {
			$result['data'][] = new DbData($row);
		}		
		return $result;
	}

	function handleInsert($sqlInsert) {
		 if ($this->conn->query($sqlInsert) === TRUE) { 
			 return $this->conn->insert_id;  
		 } else {
			 throw new Exception($this->conn->error ."\n for query $sqlInsert");
		 }
	}
	
	function handleDelete($sql) {
		if ($this->conn->query($sql) === TRUE) {
		  return true;
		} else {
		  throw new Exception($this->conn->error ."\n for query $sql");
		}
	}
	
	function handleUpdate($sql){
		if ($this->conn->query($sql) === TRUE) {
		  return true;
		} else {
		  throw new Exception($this->conn->error ."\n for query $sql");
		}
	}
	
	
		
	public function getData($query, $className = null){
		$result = fetchAssocRows($query);
		
		
		return $result;
		
	}
	
}
