<?php

class Location {
	public  $id;
	public  $ukgrCode;
	public  $city;
	public  $address;
	public  $capacity;
	public  $active;
	
	function __construct($row) {
		$this->id = $row["id"];
		$this->ukgrCode = intval($row["ukgr_code"]);
		$this->city = $row["city"];
		$this->address = $row["address"];
		$this->capacity = $row["capacity"];
		$this->active = $row["active"];
	}

}
