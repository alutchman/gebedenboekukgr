<?php

final class Querys  {
	const TIME_OPT  = 'SELECT 
				A.id, 
				SUBSTRING(A.start_time, 1, 5) AS display_time 
			FROM `service_time` A   
			ORDER BY A.start_time ASC
			';
	const PREF_LOC_MEMBERS =
	 'SELECT  DISTINCT 
			B.ukgr_code , 
			B.day_number,
			B.long_name, 
			B.short_name, 
			COUNT(*) as aantal, 
			GROUP_CONCAT(DISTINCT B.time_id ORDER BY B.time_id ASC) as times,
			GROUP_CONCAT(DISTINCT B.display_time ORDER BY B.time_id ASC) as showtime,
			GROUP_CONCAT(B.preftime ORDER BY B.time_id ASC) as prefered
	   FROM 
			(
			SELECT A.*,IF(B.member_id IS NULL,FALSE, TRUE) as preftime   FROM view_location_times A 
						LEFT JOIN `member_preference` B 
						   ON 	B.day_number=A.day_number
								AND B.time_id=A.time_id 
								AND B.member_id = %d
						where A.ukgr_code = %d
						ORDER by day_number ASC, time_id ASC
			) B		
			WHERE B.ukgr_code = %d
			GROUP BY  B.ukgr_code, B.day_number, B.long_name,B.short_name			
		'; 	

	const AVAILABLE_ATDATE = 'SELECT 
		  DISTINCT  A.ukgr_code, A.next_event,A.short_name, A.selcolor,
			GROUP_CONCAT( SUBSTRING(A.start_time, 1, 5) ORDER BY time_id ASC) as times,
			GROUP_CONCAT( (A.capacity - A.current_count ) ORDER BY time_id ASC) as available  
		   FROM 
		   (
			   SELECT B.*, C.short_name FROM 
			   (
			      %s
			   
			   )  B
			   INNER JOIN `view_day_names` C ON C.day_number=B.day_number
		   ) A
		WHERE A.ukgr_code=%d
		GROUP BY  A.ukgr_code, A.next_event,A.short_name, A.selcolor
		ORDER BY A.next_event ASC';
		
		
	
	const VIEW_NEXT_EVENT = 'SELECT 
				K.next_event, K.day_number, K.ukgr_code, K.capacity, K.time_id, K.start_time, K.selcolor, 
				IF(K.current_count IS NULL,0, K.current_count) as current_count  
			FROM 
				(
					SELECT 
						C.next_event, 
						C.day_number, 
						IF(R.datum IS NULL, 1, 2 ) AS selcolor, 
						IF(R.datum IS NULL, Q.ukgr_code, R.ukgr_code ) AS ukgr_code, 
						IF(R.datum IS NULL, Q.capacity, R.capacity ) AS capacity, 
						IF(R.datum IS NULL, Q.time_id, R.time_id ) AS time_id, 
						IF(R.datum IS NULL, Q.start_time, R.start_time ) AS start_time,
						IF(R.datum IS NULL, W1.current_count, W2.current_count ) AS current_count
					FROM
					(
						%s															
	  
					) C  
				    LEFT JOIN 
				    (
				        SELECT 
						     Z.ukgr_code, Z.capacity, V.time_id, V.day_number, W.start_time
						   FROM location_times V
						   INNER JOIN service_time W ON W.id = V.time_id
						   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code			   
				    
				    )   Q  ON Q.ukgr_code = %d  AND Q.day_number = C.day_number 
				    LEFT JOIN view_occupied W1 ON W1.ukgr_code = Q.ukgr_code AND W1.service_date = C.next_event AND Q.time_id = W1.time_id
				    LEFT JOIN 
				    (
				        SELECT 
								Z.ukgr_code, Z.capacity,V.time_id,V.day_number, W.start_time, V.datum
						   FROM view_feestdag_times V
						   INNER JOIN service_time W ON W.id = V.time_id 
						   INNER JOIN location Z ON Z.ukgr_code = V.ukgr_code			   
				    
				    )   R  ON R.ukgr_code = %d    AND C.next_event = R.datum
				    LEFT JOIN view_occupied W2 ON W2.ukgr_code = R.ukgr_code AND W2.service_date = R.datum AND R.time_id = W2.time_id
				)	
				K
    	  ORDER BY K.ukgr_code ASC, K.day_number ASC,  K.time_id ASC';	

    const AGENDA_PERSON = 'SELECT 
				distinct	
				A.next_event, 
				A.day_number,
				group_concat(distinct substring(W.start_time,1,5) order by W.start_time ASC) AS pref_time_display,
				group_concat(distinct substring(E.start_time,1,5) order by E.start_time ASC) AS real_time_display
			FROM
			(
				%s
			)   A 
			INNER JOIN member_preference B ON B.member_id = %d AND B.day_number = A.day_number 
			INNER JOIN visitors V ON V.id = B.member_id = B.member_id 
			INNER JOIN service_time W ON W.id = B.time_id
			LEFT JOIN service_dayinfo Z ON Z.service_date = A.next_event AND Z.member_id=B.member_id AND Z.ukgr_code= V.ukgr_code 
			LEFT JOIN service_time E ON E.id = Z.time_id
			GROUP BY  A.next_event,     A.day_number
			ORDER BY  A.next_event ASC ';
			
	
}
