<?php

class TimeSlot {

	public  $eventDate;
	public  $capacity;
	public  $timeId;
	public  $startTime;
	public  $currentCount;
	public  $info;
	
	function __construct($row) {		
		$this->eventDate = intval($row["next_event"]);
		$this->capacity = intval($row["capacity"]);
		$this->currentCount = intval($row["current_count"]);
		$empty = $this->capacity - $this->currentCount;
		$this->info = "&nbsp; ($empty plaatsen vrij)";
		$this->timeId = $row["time_id"];
		$this->startTime = substr($row["start_time"], 0, 5);
		
	}
	
}
