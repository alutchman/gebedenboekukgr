<?php
	session_start();

	$GLOBALS['bible'] = "https://".$_SERVER['SERVER_NAME']."/Bible/";
    $GLOBALS['mijnUkgr']  = dirname($_SERVER['PHP_SELF']);    
    $GLOBALS['root']   = $_SERVER['DOCUMENT_ROOT'].$GLOBALS['mijnUkgr'].'/';
    $GLOBALS['htmlRoot']   = 'https://'.$_SERVER['SERVER_NAME'].$GLOBALS['mijnUkgr'].'/';
    $paths0 =  explode('?',substr($_SERVER['REQUEST_URI'],1+strlen( $GLOBALS['mijnUkgr'] )));
    $paths1 =  explode('/',$paths0[0]);
    
    $initialExtra = strlen($paths0[0]) > 1 ? implode('/', $paths1) : '';     
    $callURL = $GLOBALS['mijnUkgr'].'/'. $initialExtra ;
    
    if(!isset($_SESSION['screen_width']) && !isset($_SESSION['screen_height'])  && 
       !isset($_REQUEST['width']) && !isset($_REQUEST['height'])){
		echo '<script type="text/javascript">window.location = "' . $callURL . '?width="+screen.width+"&height="+screen.height;</script>';
		exit;   
	} else if(isset($_GET['width']) && isset($_GET['height'])) {
		$_SESSION['screen_width'] = $_GET['width'];
		$_SESSION['screen_height'] = $_GET['height'];
		header('Location: ' . $callURL);
		exit;
	} 

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	mysqli_report(MYSQLI_REPORT_STRICT);
	$timeZone = 'Europe/Amsterdam';  // +1 hours
    date_default_timezone_set($timeZone); 
    setlocale(LC_ALL, 'nl_NL');
    setlocale(LC_TIME, 'nl_NL');

    $servicePart       = 'start';
    $action            = 'show';	
	$extraPaths = array();
	
	if (count($paths1) > 2) {
		$extraPaths = array_slice($paths1, 2); 
		if ($extraPaths[count($extraPaths)-1] == '') {
			array_pop($extraPaths);			
		}
	}	
	
	$GLOBALS['paths']  = $extraPaths;	
    

    if ( !isset( $_SESSION['userInfo']) )  {
		 $servicePart = 'login';
		 if (count($paths1) >= 2  ) {			
			$action = $paths1[1];	
			if ($action !== 'show' && $action !== 'register'  && $action !== 'validate' && $action !== 'reset' && $action !== 'recover'
					&& $action !== 'passwordset'  && $action !== 'savenewpassword') {
				$action = 'show';
			}
		 }		 
	}  else {		
		if (count($paths1) >= 2) {
			$servicePart = $paths1[0];			
			if ( strlen(trim($paths1[1])) > 0) {			
				$action = $paths1[1];
			}
		}	else {			
			if (count($paths1) == 1 && strlen(trim($paths1[0])) > 0) {						
				$servicePart =  trim($paths1[0]);
			}		
		}	
	}
	

	$service = ucfirst($servicePart).'Service';
	if (!function_exists('getallheaders')) {
		function getallheaders() {
		$headers = [];
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
		}
    }		
	
	try {		
		require_once(dirname($_SERVER['DOCUMENT_ROOT']).'/private/defs.php');		
		$GLOBALS['smtpHost'] = $smtpHost;
		$GLOBALS['smtpUser'] = $smtpUser;
		$GLOBALS['smtpPassword'] = $smtpPassword;
		$GLOBALS['SERVERNAME'] = $SERVERNAME;
		$GLOBALS['USERNAME'] = $USERNAME;
		$GLOBALS['PASSWORD'] = $PASSWORD;
		$GLOBALS['DBNAME'] = $DBNAME;
		
		
		function autoloadFunction($class) {
			$fileToRequire = null;
			if (preg_match('/Service$/', $class)) {
			    $fileToRequire =  $GLOBALS['root'] .'services/' . $class . ".php";				
			} else {
				$fileToRequire =  $GLOBALS['root'] .'includes/' . $class . ".php";				
			}
			if ($fileToRequire == null) {
				 throw new Exception("No Class to load");
			} else if (!file_exists($fileToRequire)) {
				 throw new Exception("File not found :" . substr($fileToRequire, 37));
			}
			require_once($fileToRequire);
		}
		
		spl_autoload_register("autoloadFunction");			
		
		$service = new $service($servicePart);
		$service->$action();
	} catch (Exception $e) {
		$extendedError = new ExtendedException($e);
		$serviceX = new ErrorService($extendedError);
		
			
		$serviceX->show();		

	} catch (Error $error) {
        $extendedError = new ExtendedError($error);
        $errstr  = $extendedError->message;			
		$serviceX = new ErrorService($extendedError);	
		$serviceX->show();				
	}



