<?php


class AgendaService extends MijnUkgrService{

	public function show(){
		extract($this->userRecord);
		
		$this->data['weekTimes'] = $this->getFromLocationAndPersonNEW($this->userRecord);
		$this->renderView(__FUNCTION__, 'baseTemplate');	
	}
	
	private function getDateRangeForSelectedDate($service_date) {
		$selections = array(); 
		$selections[] = "SELECT DATE('$service_date')  AS 'next_event',	DAYOFWEEK(DATE('$service_date') )  AS day_number";
		for ($delta = 1; $delta <= 42; $delta++) {
		  $selections[] = "SELECT DATE_ADD(DATE('$service_date'), INTERVAL $delta DAY)  AS 'next_event',	DAYOFWEEK(DATE_ADD(DATE('$service_date'), INTERVAL $delta DAY) )  AS day_number";
		} 
		$data = implode(" UNION ", $selections)		;
		return $data ;		
	}		

	
	private function getFromLocationAndPersonNEW($userRecord){
		$personId =  $userRecord['id'];
		$ukgrCode = $userRecord['ukgr_code'];
		$dateToday = new DateTime();
		$interval = new DateInterval('P1D');
		$dateTomorrow= (new DateTime())->add( $interval );;

		$dayNum1 = 1+intval($dateToday->format('w'));
		$dayNum2 = 1+intval($dateTomorrow->format('w'));

		$datToUse = $dayNum1 == 7 ? $dateTomorrow : $dateToday;
		$service_date = $datToUse->format("Y-m-d");		
		
		$dataRange = $this->getDateRangeForSelectedDate($service_date);
		$query = " SELECT A.*, B.id, B.time_id,B.showed_up,B.time_id_member, E.start_time, Q.time_id as pref_time_id, W.start_time as pref_start_time FROM (		
						$dataRange	
		
					) A
					 INNER JOIN member_preference Q ON Q.member_id = $personId AND 
					 (
					     (Q.day_number = A.day_number AND A.next_event NOT IN (SELECT datum FROM feestdagen WHERE active = TRUE)) OR
					     (
					       Q.day_number = 1 AND A.next_event IN (SELECT datum FROM feestdagen WHERE active = TRUE)
					       AND Q.time_id = (SELECT MIN(time_id) FROM member_preference WHERE day_number = 1 AND member_id = $personId)
					     
					     )					     
					 ) 					 
					 INNER JOIN service_time W ON W.id = Q.time_id
					 LEFT JOIN service_dayinfo B 
						ON B.service_date = A.next_event AND 
							B.time_id_member = Q.time_id AND 
							B.day_number = Q.day_number  AND 
							B.member_id=%d AND 
							B.ukgr_code= %d
					 LEFT JOIN service_time E ON E.id = B.time_id

					 ORDER BY A.next_event ASC,Q.time_id ASC, E.start_time ASC";	
		return $this->connection->fetchAssocRows( sprintf($query,$personId, $ukgrCode));			
	}
}
