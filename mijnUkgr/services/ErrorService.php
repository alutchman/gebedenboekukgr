<?php

class ErrorService  extends MijnUkgrService {
	public $errorMessage = "";
	public $errorObject;
	
	
	function __construct($errorObject){
		parent::__construct('error');
		$this->errorObject = $errorObject;
	}
		 
	public function show(){
		$this->renderView('message', 'errorTemplate');
	}
	
	public function getLevel() {
		return 'viewer';
	}
	 

	
}
