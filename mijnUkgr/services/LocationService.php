<?php

class LocationService  extends MijnUkgrService {
	private $email_from = "info@ukgr.nl";
	private $days = array('Zondag','Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag');
	
	
	public function show(){		
		$this->savePreferences();					
		$this->fetchUkgrLocations($this->userRecord['ukgr_code']);		
		$this->data['prefs'] = $this->fetchDetailedData(Querys::PREF_LOC_MEMBERS,  
									array($this->userRecord['id'], $this->userRecord['ukgr_code'], 
									$this->userRecord['ukgr_code'])); 		
		$this->data['times'] = $this->fetchDetailedData(Querys::TIME_OPT);
		
		$ukgrCode =  $this->userRecord['ukgr_code'];
		$dataRange = $this->getDateRangeForSelectedDate();
		//$viewNextEvent = sprintf(Querys::VIEW_NEXT_EVENT,$ukgrCode, $dataRange);		
		$viewNextEvent = sprintf(Querys::VIEW_NEXT_EVENT, $dataRange,$ukgrCode,$ukgrCode);		
		$this->data['available'] = $this->fetchDetailedData(Querys::AVAILABLE_ATDATE, array($viewNextEvent , $ukgrCode));
		$this->renderView(__FUNCTION__, 'baseTemplate');		
	}	
	
	private function getDateRangeForSelectedDate() {
		$dateToday = new DateTime();
		$interval = new DateInterval('P1D');
		$dateTomorrow= (new DateTime())->add( $interval );

		$dayNum1 = 1+intval($dateToday->format('w'));
		$dayNum2 = 1+intval($dateTomorrow->format('w'));

		$datToUse = $dayNum1 == 7 ? $dateTomorrow : $dateToday;
		$service_date = $datToUse->format("Y-m-d");			
		
		$selections = array(); 
		$selections[] = "SELECT DATE('$service_date')  AS 'next_event',	DAYOFWEEK(DATE('$service_date') )  AS day_number";
		while (count($selections) < 15) {			
		    $datToUse->add($interval);
			$dayNum1 = 1+intval($datToUse->format('w'));
			if ($dayNum1 == 7) continue;			
			$service_date = $datToUse->format("Y-m-d");	
			$selections[] = "SELECT DATE('$service_date')  AS 'next_event',	DAYOFWEEK(DATE('$service_date') )  AS day_number";	
		} 
		$data = implode(" UNION ", $selections)		;
		return $data ;		
	}	
	
	
	private function savePreferences(){
		if (isset($this->post['updateRequired'])) {
			$query = "DELETE FROM member_preference WHERE member_id = ".$this->userRecord['id'] ;
			$this->connection->handleUpdate($query);
			
			if (isset($this->post['prefered'])) {
				$valueList = array();
				foreach($this->post['prefered'] as $dayNumber => $timeIdList) {
					foreach($timeIdList as $time_id => $value) {
						$valueList[] = sprintf("(%d, %d, %d)", $this->userRecord['id'], $dayNumber, $time_id); 
					}					
				}
				$valueJoined = implode(',', $valueList);
				
				$query = "INSERT INTO member_preference (member_id, day_number, time_id) VALUES $valueJoined";
				$this->connection->handleInsert($query);
			}
		}
	}	
}
