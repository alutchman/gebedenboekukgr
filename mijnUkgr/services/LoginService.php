<?php

class LoginService  extends MijnUkgrService {
	CONST DEFAULT_PW = 'Ukgr#WeesGezegend';
	
	
	public $errorMessage = "";
	public $email        = "";
	public $ukgr_code    = 0;
	public $firstName    = "";
	public $lastName     = "";
	public $birthDate    = "";
	public $gsm          = "";
	public $street       = "";
	public $postcode     = "";
	public $numberAdd    = "";
	public $number       = "";
	public $city         = "";	
	public $gender       = 'X';
		
	
	public function getLevel() {
		return 'viewer';
	}
	 
	
	public function show(){
		if (isset($this->session['email'])) {
			$this->email = $this->session['email'];		
		}
		$this->renderView(__FUNCTION__, 'loginTemplate');
	}
	
	public function reset(){		
		if (isset($this->post['email'])) {	
			$query = sprintf("SELECT * FROM visitors WHERE email = '%s'", $this->post['email']); 
			$userDataList = $this->connection->fetchAssocRows($query );
			if (count($userDataList) == 1) {
				$token = $this->getRandom(18);
				$query = sprintf("UPDATE visitors SET resetuser = '%s' WHERE email = '%s'", $token, $this->post['email']); 
			    $this->connection->handleUpdate($query);	
			    $this->data['url'] =  $this->globals['htmlRoot'].$this->basedir.DIRECTORY_SEPARATOR.'recover'.DIRECTORY_SEPARATOR.sha1($token).DIRECTORY_SEPARATOR. base64_encode($this->post['email']) ;
			    
				$message = $this->getStream('emailref', 'email');			
				
				$emailFrom = 'info@ukgr.nl';
				$fromName = 'Beheer Ukgr';
				$emailTo = $this->post['email'];
				$toName =  $userDataList[0]['first_name'].' '.$userDataList[0]['last_name'];
				$subject = 'Wachtwoord herstellen';
				
				//  ============  Tijdelijke blok  =====================
				/*
				if ($_SERVER['SERVER_ADDR'] == '185.135.241.87') {
					$headers = "From: info@ukgr.nl\n";
					$headers .= "Reply-To: info@ukgr.nl\n";
					$headers .= "MIME-Version: 1.0\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\n";
					if (mail($this->post['email'], $subject, $message, $headers)) {
						$this->renderView('email_verzonden', 'loginTemplate');	
					} else {
						$this->renderView('emailerror', 'loginTemplate');	
					}
					return;
				}
				*/ 
				//  ============  Tijdeliijke blok  EINDE ===============
				
				$mailResult = $this->sendMail($emailFrom, $fromName, $emailTo, $toName, $subject,  $message);
				if(!$mailResult) {
					$this->renderView('emailerror', 'loginTemplate');	
				} else {
					$this->renderView('email_verzonden', 'loginTemplate');	
				}				
			} else {
				$this->renderView('email_verzonden', 'loginTemplate');	
			}			
		} else {
			$this->renderView(__FUNCTION__, 'loginTemplate');	
		}		
	}
	
	public function savenewpassword() {
		if (!isset($this->post['password']) || !isset($this->post['password2']) || !isset($this->post['email']) || !isset($this->post['useDate']) ) {
			$this->redirect('show');	
		}
		$password1 = $this->post['password'];
		$password2 = $this->post['password2'];
		$email     = $this->post['email'];
		
		$query = sprintf("SELECT * FROM visitors WHERE email = '%s'", $this->post['email']); 
		$userDataList = $this->connection->fetchAssocRows($query );
		if (count($userDataList) == 1) {
			$userToReset = $userDataList[0];
			$birthDatePassed = $this->post['useDate'];
			$birthDateRequired = $userToReset['birth_date'];
			if (strcmp($birthDateRequired, $birthDatePassed) === 0  && strcmp($password1, $password2) === 0 ){		
				$pwd_hashed = password_hash($password1, PASSWORD_BCRYPT, $this->options);	
				$query = "UPDATE visitors SET password = '%s' WHERE email = '%s' ";
				$queryExec = sprintf($query, $pwd_hashed , $email);	
				$this->connection->handleUpdate($queryExec);	
				
				$userInfo = new UserInfo();
				$userInfo->init($userToReset); 
				$_SESSION['userInfo']  = serialize($userInfo);						
				$this->redirect('start');	
			} else if (strcmp($birthDateRequired, $birthDatePassed) === 0  && strcmp($password1, $password2) !== 0 ){	
				$this->renderView('passwordResetError', 'loginTemplate');	
				return;
			} else if (strcmp($birthDateRequired, $birthDatePassed) !== 0  && strcmp($password1, $password2) === 0 ){	
				$this->renderView('invalideReset', 'loginTemplate');	
				return;
			}
		}
		$this->redirect('login','show');
	}
	
	public function recover() {
		if (count($this->paths) >= 2){			
			$hashedToken = $this->paths[0];
			$this->data['email'] = base64_decode(trim($this->paths[1]));
			$query = sprintf("SELECT * FROM visitors WHERE email = '%s'", $this->data['email']); 
			$userDataList = $this->connection->fetchAssocRows($query );
			if (count($userDataList) == 1) {
				$userToReset = $userDataList[0];
				
				$query = sprintf("UPDATE visitors SET resetuser = NULL WHERE email = '%s'", $this->data['email']); 
				$this->connection->handleUpdate($query);	
				
				if (strcmp(sha1($userToReset['resetuser']), $hashedToken) === 0 ) {
					$this->renderView(__FUNCTION__, 'loginTemplate');	
					return;	
				}						
			}	
		}
		$this->redirect('login','show');
	}

	
	public function register(){
		$this->initRegistration();
		if (isset($this->post['first_name'])    && isset($this->post['last_name'])  
			 && isset($this->post['email'])     && isset($this->post['birth_date'])  
			 && isset($this->post['password1'])  && isset($this->post['password2'])
			 && isset($this->post['useDate'])   ) {				//&& isset($_FILES['picture'])
			
			$fileResult = $this->processPicture($_FILES['picture']);
			
			if(!preg_match(self::EMAIL_EXPR, trim($this->email))) {
				$this->errorMessage .= "Ongeldige email opgegeven";
			} else if ($fileResult !== false) {
				$password1 = trim($this->post['password1']);
				$password2 = trim($this->post['password2']);
				if ($password1 !== $password2)  {
					$this->errorMessage .= "Uw wachtwoorden zijn niet gelijk.\n";
				} else {
					
					$query = sprintf("SELECT * FROM visitors WHERE email = '%s'", $this->email); 
					$userDataList = $this->connection->fetchAssocRows($query );	
					if (count($userDataList )  == 1) {
						$this->errorMessage = "Uw email is al in gebruik";
					} else {
						$this->handleAddVisitor($fileResult, $password1);
					}							
				}				
			}								
		}
		
		$this->fetchUkgrLocations();
		$this->renderView(__FUNCTION__, 'loginTemplate');		
	}
	
	private function handleAddVisitor($fileResult, $password){
		$hasImage = false;
		$pwd_hashed = password_hash($password, PASSWORD_BCRYPT, $this->options);
		if ($fileResult['data'] != null  && $fileResult['size'] > 0 ) {
		   $query = "INSERT INTO visitors 
			(email,ukgr_code, gsm, password, birth_date, first_name, last_name, allow_use, 
			zipcode, house_number,extra_address, street, city, gender, picture, 	extension ) 
			values('%s', %d, '%s', '%s', '%s', '%s', '%s', TRUE, '%s',  %d, '%s',  '%s', '%s', '%s', '%s', '%s')";  
			$hasImage = true;
		} else {
			$query = "INSERT INTO visitors 
				(email, ukgr_code, gsm, password, birth_date, first_name, last_name, allow_use, 
				zipcode, house_number,extra_address, street, city, gender, picture, 	extension ) 
				values('%s', %d, '%s', '%s', '%s', '%s', '%s', TRUE, '%s',  %d, '%s',  '%s', '%s', '%s', NULL, NULL)";  	
		}
		
		if ($hasImage) {
			$queryExec = sprintf($query, $this->email, $this->ukgr_code, $this->gsm , $pwd_hashed, 
					$this->post['useDate'], $this->firstName, $this->lastName, 
					$this->postcode, $this->number, $this->numberAdd,
					 $this->street, $this->city, $this->gender, $fileResult['data'],	 $fileResult['ext']);				
		} else {
			$queryExec = sprintf($query, $this->email, $this->ukgr_code, $this->gsm , $pwd_hashed, 
					$this->post['useDate'], $this->firstName, $this->lastName, 
					$this->postcode, $this->number, $this->numberAdd,
					 $this->street, $this->city, $this->gender);					
		}	

		try {				
			$userId = $this->connection->handleInsert($queryExec);
			$query = sprintf("SELECT * FROM visitors WHERE email = '%s'", $this->email); 
			$userDataList = $this->connection->fetchAssocRows($query );			
			$userInfo = new UserInfo();
			$userInfo->init($userDataList[0]); 
			$_SESSION['userInfo']  = serialize($userInfo);			
			$this->redirect('start');
		} catch (Exception $e) {
			$extendedError = new ExtendedException($e);
			$this->errorMessage = $extendedError->message;			
		}	
	}
	
	public function validate(){		
		if (isset($this->post['email']) && isset($this->post['captcha']) && isset($this->post['password']) ) {
			$this->email = trim($this->post['email']);			
			$captchaSession = $this->session['digit'];
			$captchaPost = strtoupper($this->post['captcha']); 
			$capchaOK = false;

			$this->errorMessage = "";

			if($captchaSession === md5(trim($captchaPost)))  {
				$capchaOK = true;
			}	
						
			if(strlen(trim($this->email)) == 0 || !preg_match(self::EMAIL_EXPR, trim($this->email))) {
				$this->errorMessage = "Ongeldige email opgegeven";	
			} else  if (!$capchaOK) {
				$this->errorMessage = "Ongeldige Security opgegeven.";
			} else {
				$_SESSION['email'] = $this->email;
				$query = sprintf("SELECT * FROM visitors WHERE email = '%s'", $this->email); 
				$userDataList = $this->connection->fetchAssocRows($query );
				
				if (count($userDataList) == 0) {
					$this->errorMessage = "Gebruiker onbekend of wachtwoord niet correct";				
				} else {	
					$userData = $userDataList[0];	
					
					if (password_verify(trim($this->post['password']), $userData['password'] )) {					
						$userInfo = new UserInfo();
						$userInfo->init($userData); 
						$_SESSION['userInfo']  = serialize($userInfo);						
						$this->redirect('start');					
					} else {
						$this->errorMessage = "Gebruiker onbekend of wachtwoord niet correct";			
					}										

				}	
			}	
		}		
		$this->renderView('show', 'loginTemplate');		
	}
	

	public function logout(){
		session_unset();
		session_destroy();
		$_SESSION = array();
		$newUrl = $this->globals['mijnUkgr'];
		header("Location: $newUrl");
	}
	
	
	private function initRegistration(){
		
		if (isset($this->post['ukgr_code'])) {
			$this->ukgr_code = trim($this->post['ukgr_code']);
		}	
		if (isset($this->post['gender'])) {
			$this->gender = trim($this->post['gender']);
		}			
		if (isset($this->post['email'])) {
			$this->email = trim($this->post['email']);
		} else {
			$letters = 'ABCDFGHJKLMNPRSTVWZ';
			$len = strlen($letters);
			$digit = '';
			for($x = 0; $x < 2; $x++) {
				$letter = $letters[rand(0, $len-1)];
				$digit .= $letter;
			}			

			$lResult = $this->connection->fetchAssocRows(
				"SELECT IF( MAX(id)  IS NULL,1 ,  1+MAX(id) ) as nextId  FROM visitors");
			$nextID = $lResult[0]['nextId'];	
			$this->email = sprintf("lid%s%'.03d@ukgr.nl", $digit , $nextID);
		}	 	
		
		if (isset($this->post['first_name'])) {
			$this->firstName = trim($this->post['first_name']);
		}
		if (isset($this->post['last_name'])) {
			$this->lastName = trim($this->post['last_name']);
		}	
		if (isset($this->post['useDate'])) {
			$this->birthDate = trim($this->post['useDate']);
		}		
		if (isset($this->post['gsm'])) {
			$this->gsm = trim($this->post['gsm']);
		}
		
		if (isset($this->post['city'])) {
			$this->city = trim($this->post['city']);
		}			
		
		if (isset($this->post['street'])) {
			$this->street = trim($this->post['street']);
		}		
		
		if (isset($this->post['postcode'])) {
			$this->postcode = strtoupper(trim($this->post['postcode']));
		}		
		
		if (isset($this->post['number'])) {
			$this->number = trim($this->post['number']);
		}
		
		if (isset($this->post['number_add'])) {
			$this->numberAdd = trim($this->post['number_add']);
		}
		
		
		if (isset($this->post['birth_date'])) {
			$this->birthDate = trim($this->post['birth_date']);
		}
				
	}	
}
