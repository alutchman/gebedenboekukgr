<?php

abstract class MijnUkgrService {
	const     VIEW_ROOT = 'views';
	CONST     EMAIL_EXPR = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
	protected $post ;
	protected $view ;
	protected $head ;   
	protected $request = array();
	protected $session = array();
	protected $data    = array();
	protected $basedir;
	protected $globals;
	protected $mobile;
	protected $jsMobile;
	protected $paths;	
	protected $connection;
	protected $userRecord = array();
	protected $options      = [ 'cost' => 12,];
	protected $levels = array('admin', 'pastors', 'viewer', 'extern'); 
	protected $registered = array(
				'start' 		=> array('label'=>'Dashboard', 'image'=>'home.png'), 
				'location' 		=> array('label'=>'Kerk diensten', 'image'=>'church2.png'), 	
				'agenda' 		=> array('label'=>'Agenda', 'image'=>'agenda.png'), 
				'message' 		=> array('label'=>'Berichten', 'image'=>'email02.png'), 	
		
				);
	
	public  $redirecting = false;
	
	public function show() {
		$this->renderView(__FUNCTION__, 'baseTemplate');		
	}	

	
	function __construct($servicePart){
		global $GLOBALS;
		$this->globals = $GLOBALS;		
		$this->paths = $this->globals['paths'];
		
		$this->basedir = $servicePart;
		$this->request = isset($_REQUEST) ? $_REQUEST : array();  
		$this->session = isset($_SESSION) ? $_SESSION : array();
		$this->post    = isset($_POST)    ? $_POST    : array();
		$this->head    = getallheaders();

		$this->mobile =  preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
   
		$this->jsMobile = $this->mobile ? "true" :"false";	 
		
		if ($servicePart != "error") {
			$this->connection = new Connection();	
		}
		

		$hrefArray = array();
		if (isset($_SESSION['MijnUkgrService'])) {
			$hrefArray = unserialize($_SESSION['MijnUkgrService']);
		}		
		if ($_SERVER['REQUEST_METHOD'] === 'GET') {
			$hrefArray[$servicePart] = $_SERVER['REQUEST_URI'];				
		} else {
		  unset($hrefArray[$servicePart]);	
		}			
		$_SESSION['MijnUkgrService'] = serialize($hrefArray);				
		
		
		if (isset($this->session['userInfo'])) {
		  $userInfo = unserialize($this->session['userInfo']);	
		  $this->userRecord = $userInfo->userRecord;
		  $this->userRecord['fullName'] = sprintf("%s %s", $this->userRecord['first_name'], $this->userRecord['last_name']);
		}
	}
		 
		
	
	public function renderView($view, $template = null) {		
		$viewBase = $this->globals['root'].self::VIEW_ROOT;
		$pathToView = $this->file_build_path($viewBase,$this->basedir)."/$view.phtml";
		
		if (!file_exists($pathToView )) {
			$pathToView = $this->file_build_path($viewBase,"mijnUkgr")."/$view.phtml";
		}
		
		header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');		
		
		$this->data['referer']	   = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "" ;
		extract($this->data);
		
		if ($template != null) {
			$pathToTemplate = $this->file_build_path($viewBase)."/$template.phtml";
			include($pathToTemplate);	
		} else {
			include($pathToView);		
		}			
	}
	
	
	public function getStream($view, $template = null){		
		ob_start(); // begin collecting output

        $viewBase = $this->globals['root'].self::VIEW_ROOT;
		$pathToView = $this->file_build_path($viewBase,$this->basedir)."/$view.phtml";
		
		if (!file_exists($pathToView )) {
			$pathToView = $this->file_build_path($viewBase,"mijnUkgr")."/$view.phtml";
		}	
	
		extract($this->data);
		
		if ($template != null) {
			$pathToTemplate = $this->file_build_path($viewBase)."/$template.phtml";
			include($pathToTemplate);	
		} else {
			include($pathToView);		
		}			

		$result = ob_get_clean();
		
		return $result;
	}
	
	public function renderIframeView($view, $template = null) {	
		$this->renderView($view, 'frames');	
			
	    $hrefArray = array();
		if (isset($_SESSION['MijnUkgrService'])) {
			$hrefArray = unserialize($_SESSION['MijnUkgrService']);
		}		
		if (isset($hrefArray[$this->basedir])) {
		  unset($hrefArray[$this->basedir]);	
		}			
		$_SESSION['MijnUkgrService'] = serialize($hrefArray);						
		
	}
	
	public function includeScript($phpScript) {
		return $this->globals['root'].$phpScript;
	}
	
	public function printAction($action){	
		echo($this->globals['mijnUkgr'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action);
	}
	
	public function printActionFull($action){	
		echo($this->globals['htmlRoot'].$this->basedir.DIRECTORY_SEPARATOR.$action);
	}
	
	public function printServiceAction($service, $action = "show"){	
		if ( $action == "show") {
			echo($this->globals['mijnUkgr'].DIRECTORY_SEPARATOR.$service);
		} else {
			echo($this->globals['mijnUkgr'].DIRECTORY_SEPARATOR.$service.DIRECTORY_SEPARATOR.$action);	
		}
		
	}
	
	public function getNextAction($action, $postUrl = '/') {
		return $this->globals['mijnUkgr'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action.$postUrl;
	}

	
	public function redirect($service, $action = 'show', $postParams = '/'){
		$newUrl = $this->globals['mijnUkgr'].DIRECTORY_SEPARATOR.$service.DIRECTORY_SEPARATOR.$action.$postParams;
		header("Location: $newUrl");
		exit;
	}
	
	protected function file_build_path(...$segments) {
		return join(DIRECTORY_SEPARATOR, $segments);
	}
	
	public function printImageSrc($src){	
		echo($this->globals['mijnUkgr'].DIRECTORY_SEPARATOR.'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$src);
	}
	
	public function printImageSrcFull($src){	
		echo($this->globals['htmlRoot'].'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$src);
	}

	
	public function loadIdImage(){
		$data2Use  = null;
		$extension = 'png';
		if ($this->userRecord != null) {
		  
		  if ($this->userRecord['picture']  != null && $this->userRecord['extension'] != null)  {		
			  $data2Use = $this->userRecord['picture'];
			  $extension = $this->userRecord['extension'];
		  }
		}
		if ($data2Use == null)  {
			$data2UseRAW = file_get_contents($this->globals['root'].'cssJsLibs'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'basePhoto.png');
			$data2Use = base64_encode($data2UseRAW);
		}		
		
		return "data:image/$extension;base64,$data2Use" ;
	}
	
	
	public function loadFrame($action) {	
		$actionToLoad = $this->getNextAction($action);
		echo "<iframe src=\"$actionToLoad \" 
			style=\"width:100%; background:white;\" onload=\"this.style.height=(this.contentWindow.document.body.scrollHeight+20)+'px';\"
			  frameborder=\"0\"></iframe>
			";
	}
	
	public function addRadioGroupList($grouplist, $name, $currentValue = '', $labeClass = '') {
		foreach($grouplist as $item) {
			$itemClass = array_key_exists("class",$item) ? $item['class']  : '';
			$this->addRadioGroup($name, $item['value'], $item['title'], $currentValue, $labeClass, $itemClass);
		}
	}
	
	public function addRadioGroup($name, $value, $title, $currentValue = '', $labeClass = '', $itemClass = ''){
		$useClass = '';
		if ($labeClass !== '') {
			if ($itemClass !== '') {
				$useClass = "class=\"$labeClass $itemClass\"";				
			} else {
				$useClass = "class=\"$labeClass\"";
			}			
		}
		
		$checked = ($value === $currentValue) ? " checked" : "";
		
		?>
		<label <?php echo($useClass); ?>>
			<input type="radio"  name="<?php echo($name); ?>"  value="<?php echo($value); ?>" required <?php echo $checked; ?>/>
			<span style="float:left"><?php echo($title); ?></span>
		</label>		
		<?php		
	}
	
	
	protected function processPicture($fileObject){
		$filenaam = basename($fileObject["name"]);
		$ext      = pathinfo($fileObject["name"], PATHINFO_EXTENSION);
		
		if ($fileObject['error'] !== UPLOAD_ERR_OK) { 				
			$imageResponse = array();
			$imageResponse['data'] = null;
			$imageResponse['size'] = 0;
			$imageResponse['name'] = $filenaam;
			$imageResponse['ext']  = $ext;		
			$imageResponse['error']  = $this->codeToMessage($fileObject['error']);				
			return $imageResponse; 
		}
        
		if (intval($fileObject["size"]) > (1024*1024*10)) {
			$this->errorMessage =  "Uw plaatje vereist te veel geheugen: " . $fileObject["size"];
		    return false; 
		}		
		
		if ($ext !== 'gif' && $ext !== 'png' && $ext !== 'jpg' && $ext !== 'jpeg') {
			$this->errorMessage = "File met extensie $ext geupload (jpg,png of gif verwacht)!";
			return false; 
		}
		
		$fileData    = file_get_contents($fileObject['tmp_name']);
		$fileEncoded = base64_encode($fileData);
		
		$imageResponse = array();
		$imageResponse['data'] = $fileEncoded;
		$imageResponse['size'] = intval($fileObject["size"]);
		$imageResponse['name'] = $filenaam;
		$imageResponse['ext']  = $ext;
		
		//cleanup
		unlink($fileObject['tmp_name']);
		return 	$imageResponse ;
	}	
	
	
	protected function fetchDetailedData($queryRAW, $params = array()) {		
		return $this->connection->fetchDetailedData($queryRAW, $params);	
	}
	
	protected function fetchDetailedDataUnMap($queryRAW, $params = array()) {	
		$result = array();
		
		try {
			$rawResullt = $this->connection->fetchDetailedData($queryRAW, $params);	
			foreach($rawResullt['data'] as $mappedObject) {
				$result[] = $mappedObject->mapped;
			}
			return $result;
		} catch (Exception $e) {
			return $result;

		} catch (Error $error) {
			return $result;
		}

	}
	
	
	
	protected function codeToMessage($code)  {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "File upload stopped by extension";
                break;

            default:
                $message = "Unknown upload error";
                break;
        }
        return $message;
    }
    
    
    protected function invalidateUserRecord(&$newUserRecord){		
		if (isset($this->session['userInfo'])) {
			$userInfo = unserialize($this->session['userInfo']);	
			$newUserRecord['fullName'] = sprintf("%s %s", $newUserRecord['first_name'], $newUserRecord['last_name']);
			$userInfo->userRecord = $newUserRecord;
			$_SESSION['userInfo']  = serialize($userInfo);				
			$this->userRecord = $newUserRecord;
		}
	}
	
	
	protected function fetchUkgrLocations($searchUkgrCode = null){
		$where = "";
		if ($searchUkgrCode != null) {
			$where = "WHERE ukgr_code = $searchUkgrCode";
		}
		$this->data['locations'] = $this->connection->fetchAssocRows("SELECT * FROM location $where ORDER BY ukgr_code ASC");
	}
	
	protected function getRandom($lengte = 15){
		$letters = '#ABCDFGHJKLMNPRSTVWZ2345678@';
		$len = strlen($letters);
		$digit = '';
		for($x = 0; $x < $lengte; $x++) {
			$letter = $letters[rand(0, $len-1)];
			$digit .= $letter;
		}	
		return $digit;	
	}
	
	protected function sendMail($emailFrom, $fromName, $emailTo, $toName, $subject,  $message){
		$mail = new PHPMailer();			
		
		$mail->IsSMTP();                       // telling the class to use SMTP

		$mail->SMTPDebug = 0;                  
		// 0 = no output, 1 = errors and messages, 2 = messages only.

		$mail->SMTPAuth = true;                // enable SMTP authentication
		$mail->SMTPSecure = "tls";              // sets the prefix to the servier
		$mail->Host = $this->globals['smtpHost'] ;               // sets Gmail as the SMTP server
		$mail->Port = 587;                     // set the SMTP port for the GMAIL

		$mail->Username = $this->globals['smtpUser']; 
		$mail->Password = $this->globals['smtpPassword'];     

		$mail->CharSet = 'windows-1250';
		$mail->SetFrom ($emailFrom, $fromName);
		$mail->Subject = $subject;
		$mail->IsHTML(true);

		$mail->Body = $message ; 
		// you may also use $mail->Body = file_get_contents('your_mail_template.html');

		$mail->AddAddress ($emailTo, $toName);     
		// you may also use this format $mail->AddAddress ($recipient);
		
		return $mail->Send();		
		
	}
		
}
