<?php

class StartService  extends MijnUkgrService {
	CONST DEFAULT_PW = 'Welkom01';
	/*
	 * initial password = Welkom01
	 * $orgPassword = "Welkom01";
	 * $pwd_hashed = password_hash($passwordOrg,  PASSWORD_DEFAULT);
	 * if (password_verify($this->post['password'], $userData['password'])) {
	 *	   echo "Password verified!";
	 * 	}
	 * 
	 */
	
	protected $errorMessage  = "";
	


	public function show(){
		$verzen = array();
		$verzen['A'] = $this->connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='VERS_BOVEN_1'")[0];
		$verzen['B'] = $this->connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='VERS_BOVEN_2'")[0];
		$verzen['C'] = $this->connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='VERS_ONDER_1'")[0];

		$this->data['verzen'] = $verzen;		
		$this->renderView(__FUNCTION__, 'baseTemplate');
	}	

	
	public function mijnData() {
		$initialData = $this->userRecord;
		$this->data['updatePicture']  = false;
		$this->data['updateFullName']  = false;
		
		if (isset($this->post['first_name'])    && isset($this->post['last_name'])  
			 && isset($this->post['email'])     && isset($this->post['birth_date'])  
			 && isset($this->post['password1']) && isset($this->post['password2'])
			 && isset($this->post['ukgr_code'])
			 && isset($this->post['useDate']) && isset($_FILES['picture'])   ) {	
			$fileResult = $this->processPicture($_FILES['picture']);
	
			if(!preg_match(self::EMAIL_EXPR, trim($this->post['email']))) {
				$this->errorMessage .= "Ongeldige email opgegeven";
			} else if ($fileResult !== false) {
				$password1 = trim($this->post['password1']);
				$password2 = trim($this->post['password2']);
				if (strlen($password1) > 0 &&  strlen($password2) > 0 && $password1 !== $password2)  {
					$this->errorMessage .= "Uw wachtwoorden zijn niet gelijk.\n";
				} else {
					if (strlen($password1) > 0) {
						$this->updateUserWithPasswrdNoImage();	
					}	else if ($fileResult['data'] != null  && $fileResult['size'] > 0 ) {
						$this->updateUserNoPasswrdWithImage($fileResult);
						
					} else if ((strlen($password1) > 0) && $fileResult['data'] != null  && $fileResult['size'] > 0 ) {
						//update image update pw
						$this->updateUserWithPasswrdWithImage($fileResult);
					} else {
						$this->updateUserNoPasswrdOrImage();
					}
					$this->data['updateFullName']  = 
						trim($this->post['first_name']) != $initialData['first_name'] || trim($this->post['last_name']) != $initialData['last_name'];
					$this->updateUserInfoFromPost($initialData);
		
				}				
			}			
		}
		$this->fetchUkgrLocations();
		$this->data['userInfo'] = $initialData;
		$this->renderIframeView(__FUNCTION__);

	}
	
		
	public function mijnDataContainer() {
		$this->loadFrame("mijnData");
	}
	
	
	public function activeContent(){
		$tag = $this->paths[0];
		$dataValues = $this->connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='$tag'");
		$this->data['content'] = count($dataValues) > 0 ? $dataValues[0] : "&nbsp;"; 
		$this->renderView(__FUNCTION__);
	}
	
	
	private function updateUserWithPasswrdWithImage($fileResult) {
		$pwd_hashed = password_hash($password1, PASSWORD_BCRYPT, $this->options);
		$initialData['picture'] = $fileResult['data'];
		$initialData['extension'] = $fileResult['ext'];
		$this->data['updatePicture']  = true;
		
		$query = "UPDATE visitors SET
			ukgr_code = %d,
			password = '%s',
			email = '%s', 
			gsm   = '%s', 
			birth_date = '%s',
			first_name = '%s',
			last_name  = '%s',
			zipcode    = '%s',
			house_number  = %d,
			extra_address = '%s',
			street = '%s',
			city   = '%s',
			gender   = '%s',
			picture     = '%s',
			extension   = '%s'
			WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $pwd_hashed,
					$this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'], 
					$fileResult['data'], $fileResult['ext'], $this->userRecord['id']);		
									
		$this->connection->handleUpdate($queryExec);			
	}
	
	private function updateUserNoPasswrdWithImage($fileResult) {
		$initialData['picture'] = $fileResult['data'];
		$initialData['extension'] = $fileResult['ext'];
		$this->data['updatePicture']  = true;
		
		$query = "UPDATE visitors SET
			ukgr_code = %d,
			email = '%s', 
			gsm   = '%s', 
			birth_date = '%s',
			first_name = '%s',
			last_name  = '%s',
			zipcode    = '%s',
			house_number  = %d,
			extra_address = '%s',
			street = '%s',
			city   = '%s',
			gender   = '%s',
			picture     = '%s',
			extension   = '%s'
			WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'], 
					$fileResult['data'], $fileResult['ext'], $this->userRecord['id']);		
									
		$this->connection->handleUpdate($queryExec);	
	}
	
	private function updateUserWithPasswrdNoImage(){
		$pwd_hashed = password_hash(trim($this->post['password1']), PASSWORD_BCRYPT, $this->options);
		$query = "UPDATE visitors SET
						ukgr_code = %d,
						password = '%s',
						email = '%s', 
						gsm   = '%s', 
						birth_date = '%s',
						first_name = '%s',
						last_name  = '%s',
						zipcode    = '%s',
						house_number  = %d,
						extra_address = '%s',
						street = '%s',
						city   = '%s',
						gender   = '%s'
						WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $pwd_hashed,
					$this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'], $this->userRecord['id']);		
			
		$this->connection->handleUpdate($queryExec);		
		
	}	
	
	private function updateUserNoPasswrdOrImage(){
		$query = "UPDATE visitors SET
			ukgr_code = %d,
			email = '%s', 
			gsm   = '%s', 
			birth_date = '%s',
			first_name = '%s',
			last_name  = '%s',
			zipcode    = '%s',
			house_number  = %d,
			extra_address = '%s',
			street = '%s',
			city   = '%s',
			gender   = '%s'
			WHERE id = %d";
		$queryExec = sprintf($query, $this->post['ukgr_code'], $this->post['email'], $this->post['gsm'] , 
					$this->post['useDate'], $this->post['first_name'], $this->post['last_name'], 
					$this->post['zipcode'], $this->post['house_number'], $this->post['extra_address'],
					$this->post['street'], $this->post['city'], $this->post['gender'], $this->userRecord['id']);		
			
		$this->connection->handleUpdate($queryExec);		
		
	}
	

    private function updateUserInfoFromPost(&$initialData){
		$initialData['email'] = $this->post['email'];
		$initialData['gsm'] = $this->post['gsm'];
		$initialData['ukgr_code'] = $this->post['ukgr_code'];
		$initialData['birth_date'] = $this->post['useDate'];
		$initialData['first_name'] = $this->post['first_name'];
		$initialData['last_name'] = $this->post['last_name'];
		$initialData['zipcode'] = $this->post['zipcode'];
		$initialData['house_number'] = $this->post['house_number'];
		$initialData['extra_address'] = $this->post['extra_address'];
		$initialData['street'] = $this->post['street'];
		$initialData['city'] = $this->post['city'];
		$initialData['gender'] = $this->post['gender'];
		$this->invalidateUserRecord($initialData);		
	}
	
}
