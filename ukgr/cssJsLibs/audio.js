var playNext = false;

var allowContinousplay = false;

Audio.prototype.stop = function() {
	this.pause();
	this.currentTime = 0;
};


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    },
    Desktop :function() {
        return (!isMobile.Android() && isMobile.BlackBerry() && isMobile.iOS() && isMobile.Opera() && isMobile.Windows());
    }
};


function enablePlayNext(){
	playNext = true;
	allowContinousplay = true;
}

function handleSongSelected(started){
	var divOuter = $("div.audioOUTER");
	var selctInDiv = divOuter.find('select[name=selectSong]');
	
	var element = selctInDiv.find('option:selected'); 
	if (!element) {
	   return;	
	}
	var myUrl = element.attr("value"); 
	var href  = element.attr("user-href");
	var title = element.attr("user-title");
	var audio = $("div.audioOUTER").find("audio")[0];    
	
	var anchorx = divOuter.find("a.orgSite");
	anchorx.attr('href', href);
	if (href.length > 0) {
		anchorx.show();
	} else {
		anchorx.hide();
	}	

	var progress = $("div.audioOUTER").find("progress");
	if (progress.length > 0) {
		progress[0].value = 0;
	}

	var isM38u = myUrl.search(".m3u8") > 0;	
	if (isM38u && !isMobile.any() ) {
		if(Hls.isSupported())	{			
			var hls = new Hls();
			hls.loadSource(myUrl);
			hls.attachMedia(audio);
			hls.on(Hls.Events.MANIFEST_PARSED,function()
			{
				audio.play();
			});
		} else if (audio.canPlayType('application/vnd.apple.mpegurl')) 	{
			audio.src = myUrl;
			audio.addEventListener('canplay',function()
			{
				audio.play();
			});
		}	

	} else {
		divOuter.find("audio").attr("src", myUrl);
	}	

	/****************/
	audio.stop();
	audio.load();//suspends and restores all audio element]
	audio.oncanplaythrough = audio.play();	
	if (!started){
		audio.volume = 0.50;
	}	
	
	if (allowContinousplay){
		playNext = true;
	}
	selctInDiv.blur();
}

$("div.audioOUTER").find('select[name=selectSong]').on('change', function() {
	handleSongSelected(true);   		
});

$("div.audioOUTER").find("progress").on('click', function(e) {
	//var ratio = e.offsetX / this.offsetWidth;
	//var player = $("div.audioOUTER").find("audio")[0];    
   // player.currentTime = ratio * player.duration;
});

function getTimeLabel(timeValue) {
	if ( isNaN(timeValue)) {
		return "00:00";	
	}	

	var  minuten =  Math.floor(timeValue/60);
	var  seconds =  Math.floor(timeValue % 60);	
	if (seconds < 10) {
		return minuten + ":0" + seconds;
	} else {
		return minuten + ":" + seconds;
	}	
	
}

$("div.sliderProgressOUTER").find('button.audiorewind').on('click', function() {
	 var audioplayit =  $(this).parent().parent().find('audio')[0];		
     audioplayit.currentTime  = 0.0;
	 document.activeElement.blur();
});


$("div.audioOUTER").find('button.audiostate').on('click', function() {
	 var audioplayit =  $(this).parent().parent().find('audio')[0];		
	 if (audioplayit.paused){
		 audioplayit.play();
	 } else {
		 audioplayit.pause();
		 playNext = false;
	 }
	document.activeElement.blur();
});

function handlePlayNext(){
	var player = $("div.audioOUTER").find("audio")[0];    
	if (playNext && player.paused){			
		var aform = document.forms['audioForm'];
		var selectieItem = aform.elements['selectSong'];
		var currentIndex = selectieItem.selectedIndex;
		var maxIndex = selectieItem.length-1;	
		var nextIndex = currentIndex +1;			
		if (nextIndex > maxIndex ) {
			var nextIndex = Math.floor(Math.random() * (maxIndex+1));
		}
		selectieItem.selectedIndex = nextIndex;
		handleSongSelected(true);
		return true;			
	}
	return false;
}

$(document).ready(function(){	
	handleSongSelected(false);  

	$( "div.sliderVolumeINNER" ).slider({
		min: 0,
		max: 200,
		value : 100,
		range: "min",
		animate: true,

	  slide: function( event, ui ) {
			$("div.audioOUTER").find("audio")[0].volume = (ui.value / 200);				 
	  }
	});	
	
	$("div.sliderProgressINNER").slider({
		min: 0,
		max: 3600,
		value : 0,
		range: "min",
		animate: true,

	  slide: function( event, ui ) {
		  var ratio = ui.value  / 3600;
	      var player = $("div.audioOUTER").find("audio")[0];    
          player.currentTime = ratio * player.duration;
	  }
	});	
	
	var player = $("div.audioOUTER").find("audio")[0];    
	player.addEventListener("timeupdate", function() {
		if (!handlePlayNext()){
			$('#currentTime').html(getTimeLabel(player.currentTime) + "/" + getTimeLabel(player.duration));

			var newvl = 3600*player.currentTime/player.duration;			
			 $("div.sliderProgressINNER").slider("value", newvl);
			
		}
		var classbtn = player.paused ? "off"  : "on";	
		$( "div.sliderVolumeOUTER" ).find("button.audiostate").removeClass('off').removeClass('on').addClass(classbtn);
		
	});	
	
});

