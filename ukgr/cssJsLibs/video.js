var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
		return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
	},
	any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	},
	Desktop :function() {
		return (!isMobile.Android() && isMobile.BlackBerry() && isMobile.iOS() && isMobile.Opera() && isMobile.Windows());
	}
};		

		
function initLive() {
	var video       = $('#player-tv')[0];
	var videoSource = $('#player-tv').find('source')[0];
	video.autoplay  = true;
	var streamUrl   = videoSource.src;
	var isM38u      = streamUrl.search(".m3u8") > 0;	
	
	if (isM38u && Hls.isSupported() && !isMobile.any()) {		
		var hls = new Hls();
		hls.loadSource(streamUrl);
		hls.attachMedia(video);
		hls.on(Hls.Events.MANIFEST_PARSED, function () {
			video.play();
		});
	}  else {		
		video.addEventListener('loadedmetadata', function () {
		   video.play();
		});
		video.load();
	}
	
}		

$(document).ready(function () {
	initLive();					
})
