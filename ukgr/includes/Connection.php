<?php
class Connection {	
	private $conn ;
	
	function __construct() {
		require_once(dirname($_SERVER['DOCUMENT_ROOT']).'/private/defs.php');		
		
		$this->conn = new mysqli($SERVERNAME, $USERNAME, $PASSWORD, $DBNAME);
		
		// Check connection
		if ($this->conn->connect_error) {
		  $this->conn = null;
		} else {
			$this->conn->set_charset('utf8');
		}
	}
	
	function __destruct(){
		if ($this->conn != null) {			
			$this->conn->close();
			$this->conn = null;
		}		
	}
	
	public function fetchAssocRows($sql){
		$result = array();		
		$resultSql = $this->conn->query($sql);			
		while($row = $resultSql->fetch_assoc()) {
			$result[] = $row;
		}		
		return $result;
	}
	
	
	function handleInsert($sqlInsert) {
		 if ($this->conn->query($sqlInsert) === TRUE) { 
			 return $this->conn->insert_id;  
		 } else {
			 throw new Exception($this->conn->error);
		 }
	}
	
	function handleDelete($sql) {
		if ($this->conn->query($sql) === TRUE) {
		  return true;
		} else {
		  throw new Exception($this->conn->error ."\n for query $sql");
		}
	}
	
	function handleUpdate($sql){
		if ($this->conn->query($sql) === TRUE) {
		  return true;
		} else {
		  throw new Exception($this->conn->error ."\n for query $sql");
		}
	}
	
}
