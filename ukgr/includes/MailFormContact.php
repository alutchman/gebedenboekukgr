<?php 
class MailFormContact {
	public $email_to	 = "info@ukgr.nl";
	public $email_subject = "Hulp en ondersteuning gewenst";
	public $error_message ="";
	public $finalMessage = "";
	
	private $first_name;
	private	$last_name;
	private	$email_from;
	private	$telephone;
	private	$comments;
	private $captchaSession;
	private $captchaPost;
	
	
	public function hasFinalMessage(){
		return $this->finalMessage != null && strlen($this->finalMessage) > 0;
	}
	
	public function  getFinalMessage(){
		return trim($this->finalMessage);
	}
	
	private function died($error) {
		
		throw new Exception($error);
	}
	
	private function validate(){
		// validation expected data exists
		if(!isset($_POST['first_name']) ||
			!isset($_POST['last_name']) ||
			!isset($_POST['email']) ||
			!isset($_POST['telephone']) ||
			!isset($_POST['comments'])) {
			$this->died('We are sorry, but there appears to be a problem with the form you submitted.');       
		}
		
		$this->captchaSession = $_SESSION['digit'];
		$this->captchaPost = strtoupper($_POST['captcha']); // required
		$this->first_name = trim($_POST['first_name']); // required
		$this->last_name = trim($_POST['last_name']); // required
		$this->email_from = trim($_POST['email']); //  required
		$this->telephone = trim($_POST['telephone']); // required
		$this->comments = trim($_POST['comments']); // required
		
		$capchaOK = false;
		
		$this->error_message = "";
		if($this->captchaSession === md5(trim($this->captchaPost)))  {
			$capchaOK = true;
		}		
	
		if(strlen($this->first_name) == 0) {
			$this->error_message .= 'Uw voornaam ontbreekt.<br />';
		}

		if(strlen($this->last_name) == 0) {
			$this->error_message .= 'Uw achternaam ontbreekt.<br />';
		}

		if(strlen($this->telephone) == 0) {
			$this->error_message .= 'Uw telefoonnummer ontbreekt.<br />';
		}

		if(strlen($this->comments) < 2) {
			$this->error_message .= 'Uw bericht is ongeldig.<br />';
		}	

		if(strlen($this->error_message) > 0) {
			$this->died($this->error_message);
		}	
		
		$this->error_message = "";
		$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
		 
		if(strlen($this->email_from)>0 && !preg_match($email_exp,$this->email_from)) {
			$this->error_message .= 'The Email Address you entered does not appear to be valid.<br />';
		}

		$string_exp = "/^[A-Za-z .'-]+$/";

		if(!preg_match($string_exp,$this->first_name)) {
			$this->error_message .= 'The First Name you entered does not appear to be valid.<br />';
		}

		if(!preg_match($string_exp,$this->last_name)) {
			$this->error_message .= 'The Last Name you entered does not appear to be valid.<br />';
		}

		if(strlen($this->comments) < 2) {
			$this->error_message .= 'The Comments you entered do not appear to be valid.<br />';
		}
				
	
		if (!$capchaOK) {
			$this->error_message .= 'Invalid Retype <br />';			
		}

		if(strlen($this->error_message) > 0) {
			$this->died($this->error_message);
		}
		

		 
	}

	
	public function sendMail(){
		if(!isset($_POST['email']))  {
		   return;
		}
		// validation expected data exists
		$this->validate();
 		
		$email_message = "Form details below.\n\n";

		$email_message .= "First Name: ".$this->clean_string($this->first_name)."\n";
		$email_message .= "Last Name: ".$this->clean_string($this->last_name)."\n";
		$email_message .= "Email: ".$this->clean_string($this->email_from)."\n";
		$email_message .= "Telephone: ".$this->clean_string($this->telephone)."\n";
		$email_message .= "Comments: ".$this->clean_string($this->comments)."\n";
		 
		// create email headers
		$headers = 'From: '.$this->email_from."\r\n".
		'Reply-To: '.$this->email_from."\r\n" .
		'X-Mailer: PHP/' . phpversion();
		@mail($this->email_to, $this->email_subject, $email_message, $headers);  
		
		$this->finalMessage = "Thank you for contacting us. 
		We will be in touch with you very soon.";
		
	}
	
	function clean_string($string) {
		$bad = array("content-type","bcc:","to:","cc:","href");
		return str_replace($bad,"",$string);
	}	 
	
}


