<?php session_start();?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/ukgr/antiCache.php');?>
<form method="POST"  onsubmit="return false" name="audioForm" >	
	<div class="audioOUTER">		
		
		<a class="orgSite" href="https://www.google.nl"  target="_blank"></a>
		<select name="selectSong" id="selectSong"  >
		<?php foreach ($songs as $value) {  ?>
			<option value="<?php echo $value['source'] ?>"  
					user-title="<?php echo $value['title'] ?>"  
					user-href="<?php echo $value['a'] ?>"><?php echo $value['title'] ?></option>	
		<?php  } ?>	
		</select>	
		<audio />
		<div class="sliderVolumeOUTER">		
			<button class="audiostate" />
			<div class="sliderVolumeINNER" />	
		</div>
		
		<div class="sliderProgressOUTER">
		    <label id="currentTime" style="float:right;text-align:right;margin-left:5px;margin-top:5px;">....</label>	
		    <button class="audiorewind" />
			<div class="sliderProgressINNER" />		
		</div>
	</div>		
</form>
<!--  https://cdn.jsdelivr.net/npm/hls.js@latest -->
<?php 
	$mobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	if(!$mobile)  {  ?><script src="https://<?php echo($_SERVER['SERVER_NAME']);  ?>/ukgr/cssJsLibs/hls.js"></script>	
<?php } ?>  
<script  src="<?php echo($_SESSION['jsBase']);  ?>audio.js" ></script>	

