<?php 
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	include($_SERVER['DOCUMENT_ROOT'].'/ukgr/antiCache.php');
?>
<?php
	session_start();
	require_once('MailFormContact.php');
	$excecpMsg = null;
	try {				
		$mailItem = new MailFormContact();
		$mailItem->sendMail();	
		if ($mailItem->hasFinalMessage()) { 
			header('Location: https://www.weesgezegend.nl/');
			exit;
		}	
	} catch (Exception $e) {
		$excecpMsg = $e->getMessage();
	}		
?>
<?php
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');	
?>
<!DOCTYPE html>
<html lang="nl-NL">

<head>
  <meta charset="UTF-8">
  <title>Wees Gezegend</title>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/ukgr/cssJsLibs/cssinit.php'); ?>
  
</head>
<body>
	
<div class="innerBody">
	<h1>Stuur ons een bericht</h1>
	<div class="container">		
		<iframe class="video" src="https://www.youtube.com/embed/6Sli0eqd1uA?rel=0&autoplay=1"  autoplay="autoplay"  frameborder="0" allow="autoplay;" ></iframe>
	</div>
	<p>
	U kunt onderaan een bericht sturen, die wij spoedig zullen beantwoorden. Als uw gegevens correct zijn ingevuld kunnen we u mailen of bellen.
	Vul daarom minstens uw telefoonnummer in.
	</p>

	<form name="contactform" method="post" >
		<table>
			<tr>
				<td valign="top" width="35%">
					<label for="first_name">Voornaam</label>
				</td>
				<td valign="top">
					<input  type="text" name="first_name" maxlength="50"  
						value="<?php echo(isset($_POST['first_name']) ? $_POST['first_name']: ""); ?>"  required="required" size="25">
				</td>
			</tr>
			<tr>
				<td valign="top">
					<label for="last_name">Achternaam</label>
				</td>
				<td valign="top">
					<input  type="text" name="last_name" maxlength="50"
					value="<?php echo(isset($_POST['last_name']) ? $_POST['last_name']: ""); ?>"  required="required" size="25">
				</td>
			</tr>
			<tr>
				<td valign="top">
					<label for="email">Email</label>
				</td>
				<td valign="top">
					<input  type="text" name="email" maxlength="80" 
					value="<?php echo(isset($_POST['email']) ? $_POST['email']: ""); ?>" size="25">
				</td>
			</tr>
			<tr>
				<td valign="top">
					<label for="telephone">Telefoon</label>
				</td>
				<td valign="top">
					<input  type="text" name="telephone" maxlength="30"  
					value="<?php echo(isset($_POST['telephone']) ? $_POST['telephone']: ""); ?>"  required="required" size="25">
				</td>
			</tr>
			<tr>
				<td valign="top">
					<label for="comments">Comments</label>
				</td>
				<td valign="top">
					<textarea  name="comments" maxlength="1000"  required="required" placeholder="Uw bericht komt hier"
					<?php  if ($mobile) { ?>cols="25" rows="4"<?php  } else { ?>cols="35" rows="4"  <?php  }  ?>     ><?php				 
					echo(isset($_POST['comments']) ? $_POST['comments']: "");
					?></textarea>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<label for="comments">Retype</label>
				</td>
				<td valign="top">
					<img src="/ukgr/includes/captcha.php" width="100" height="25" border="1" alt="CAPTCHA" 
					style="float:left;margin-right:10px;">
					<input  type="text" name="captcha" maxlength="8" size="8" style="text-transform: uppercase;">
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center">
					<input type="submit" value="Submit">  
				</td>
			</tr>
		</table>
	</form>
	<?php if ($excecpMsg != null) {  ?>
		<p style="color:red;">
			<?php echo($excecpMsg); ?>
		</p>	
	<?php } ?>	
	<a href="/">U kunt teruggaan naar de vorige pagina.</a> 
	</div>
</div>	
</body>
</html>
