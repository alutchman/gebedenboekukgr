<?php include('../prehtml.php'); ?>
<head>
	<meta charset="UTF-8">
	<title>Wees Gezegend</title>
	<?php require('../css/cssinit.php'); ?>  
	<style>		
		#googleMap {
			height: 800px;  /* The height is 400 pixels */
			width: 95%;  /* The width is the width of the web page */
		}
	</style>
   </head>
<body >
<h1>Onze Locaties in Nederland</h1>
<div id="googleMap" style="margin:10px; position: relative;"></div>
<script>
	<?php 
		$locatiesMarkers = array();
		$xml=simplexml_load_file("https://www.weesgezegend.nl/includes/locaties.xml");
		foreach ($xml->locatie as $locatie)  {
			$php_item = array('name'=>(string)$locatie->name,
			'id'=>(int)$locatie->id,
			'lat'=>(float)$locatie->lat,
			'lng'=>(float)$locatie->lng,
			'description'=>(string)$locatie->description,
			'type'=>(int)$locatie->type);
			$locatiesMarkers[] = $php_item ;
		}	
		echo "var isMobile = ".($mobile ? "true" : "false").";\n";	
	 ?>
	var locatiesMarkers = <?php echo json_encode($locatiesMarkers) ?>;
	var lastWindow = null; 
	
	function detectBrowser() {
		var useragent = navigator.userAgent;
		var mapdiv = document.getElementById("googleMap");
		if (isMobile) {
			mapdiv.style.height = '500px';
		} 
		google.maps.event.trigger(mapdiv, "resize");
	}	

	function myMap() {
		var infowindow = new google.maps.InfoWindow();
		var mapProp= {
		  center:new google.maps.LatLng(52.1561113, 5.3878266),
		  zoom:7.4
		};
		var mapExternal =  new google.maps.Map(document.getElementById("googleMap"),mapProp);	
	  
		  
	    var minLatitude = 400.0;
        var maxLatitude = -400;
        var minLongitude = 400.0;
        var maxLongitude = -400.0;

		var bounds = new google.maps.LatLngBounds();
		
		for (i = 0; i < locatiesMarkers.length; i++) {
			minLatitude = Math.min(minLatitude, locatiesMarkers[i].lat);
			maxLatitude = Math.max(maxLatitude, locatiesMarkers[i].lat);
			minLongitude = Math.min(minLongitude, locatiesMarkers[i].lng);
			maxLongitude = Math.max(maxLongitude, locatiesMarkers[i].lng);
			
			var positionX = new google.maps.LatLng(locatiesMarkers[i].lat, locatiesMarkers[i].lng);
			var icon2use = 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';
			if (locatiesMarkers[i].type ==3) {
				icon2use = 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
			} else if (locatiesMarkers[i].type ==2) {
				icon2use = 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
			}else if (locatiesMarkers[i].type ==1  && locatiesMarkers[i].id ==1) {
				icon2use = 'https://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
			}
			var marker = new google.maps.Marker({
				position: positionX,
				map: mapExternal,
				title: locatiesMarkers[i].name,
				snippet : locatiesMarkers[i].description,
				icon : icon2use
			});
			google.maps.event.addListener(marker, "click", function(evt) {  
				infowindow.setContent(this.get('title')+"<br />"+ this.get('snippet'));
				infowindow.open(this.map,this);
				lastWindow = infowindow;
			});  
						
			bounds.extend(positionX);
		} 
      
        if (locatiesMarkers.length >= 3) {
			 mapExternal.fitBounds(bounds);                 
        }
	}
	detectBrowser() ;

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRxbC1WqVlH0lJZddgurz-h7E9ta3-VzI&callback=myMap"  async defer></script>
</body>
</html>
