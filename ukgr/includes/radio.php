<?php 
	require_once('Connection.php');
	extract($_GET);
	
	$connection = new Connection();	
	$programs = $connection->fetchAssocRows("SELECT * FROM media_data WHERE active=true AND media_group='$group' ORDER BY sequence ASC ");
	
	$songs = array();
	foreach ($programs as $song)  {
		$songs[] = array(
			'title'=>$song['title'],
			'source'=>(string)$song['source'],
			'a'=>(string)$song['anchor']);
	};
	include("baseAudioPlayer.php");	
?>
<script>
$(document).ready(function(){	
	$('div.sliderProgressOUTER').remove();	
});
</script>
