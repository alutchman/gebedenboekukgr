<?php 	
	require_once('Connection.php');
	extract($_GET);
	
	$connection = new Connection();
	
	$programs = $connection->fetchAssocRows("SELECT * FROM media_data WHERE active=true AND media_group='$group'  ORDER BY sequence ASC ");
	
	$root = (dirname($_SERVER['PHP_SELF'])).'/media/';
	
	$songs = array();
	foreach ($programs as $song)  {
		$songs[] = array(
			'title'=>(string)$song['title'],
			'source'=>  $root.( (string) $song['source']),
			'a'=>(string)$song['anchor']);
	};
	include("baseAudioPlayer.php");
	
?>
<script>
$(document).ready(function(){	
	$('div.audioOUTER').find('a.orgSite').hide();	
	var aform = document.forms['audioForm'];
	var selectieItem = aform.elements['selectSong'];
	var aantal = selectieItem.length;	
	var randomIndex = Math.floor(Math.random() * aantal);
	selectieItem.selectedIndex = randomIndex;
	handleSongSelected(true);
	enablePlayNext();
});
</script>

