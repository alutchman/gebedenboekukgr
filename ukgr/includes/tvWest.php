<?php
	require_once('Connection.php');	
	
	$connection = new Connection();	
	$programs = $connection->fetchAssocRows("SELECT * FROM media_data WHERE active=true AND media_group='$group'  ORDER BY sequence ASC ");
 
 
  $rubriek = str_replace(' ', '_', $title );
  $refUrl = "https://www.omroepwest.nl/tv/programma/170276075/Hoop-Leven";
  $refInfo = "Elke zondag om 9:00u op TV Omroep West";
?>
<div class="container">
	<iframe src="" 
	 id="<?php echo($rubriek);  ?>Iframe" 
	 webkitallowfullscreen mozallowFullscreen oallowFullscreen msallowFullscreen allowfullscreen
	 frameborder="0" allowfullscreen class="video" ></iframe>
</div>
<div style="display:block;overfkow:hidden;text-align:center;padding:2px;">
	<a href="<?php echo($refUrl);  ?>"  style="color:red;"><?php echo($refInfo);  ?></a><br />
	<hr />
	<form name="<?php echo($rubriek);  ?>Form">
      <select name="select<?php echo($rubriek);  ?>">
		<?php
			function remove_bs($Str) {  
			  $StrArr = str_split($Str); 
			  $NewStr = '';
			  foreach ($StrArr as $Char) {    
				$CharNo = ord($Char);
				if ($CharNo == 163) { $NewStr .= $Char; continue; } 
				if ($CharNo > 31 && $CharNo < 127) {
				  $NewStr .= $Char;    
				}
			  }  
			  return $NewStr;
			}
				
			foreach($programs as $hoopValue) {				
				$value = remove_bs($hoopValue['source'].trim());	
				$label = $hoopValue['title'].trim();					
				echo "<option  value=\"$value\">$label</option>";				
							
			}		
		?>
      </select>	
	</form>
</div>
<script>
	function change<?php echo($rubriek);  ?>(){
		var aform = document.forms['<?php echo($rubriek);  ?>Form'];
		var selectieItem = aform.elements['select<?php echo($rubriek);  ?>'];
		
		var newUrl = selectieItem.value.trim()
		newUrl  += '&referrer='+encodeURIComponent(location.href)+"&realReferrer="+encodeURIComponent("https://www.weesgezegend.nl");
		$("#<?php echo($rubriek);  ?>Iframe").attr("src", newUrl)
	}
		
	$("form[name=<?php echo($rubriek);  ?>Form]").find('select').on('change', function() {
		change<?php echo($rubriek);  ?>();	
	});
	$(document).ready(function(){	
		change<?php echo($rubriek);  ?>();			
	});		
</script>
