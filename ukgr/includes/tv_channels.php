<video id="player-tv" autoplay="autoplay" preload="metadata" poster="/cssJsLibs/images/iurdposter.jpg" controls >
</video>
<?php
	require_once('Connection.php');
	extract($_GET);
	
	$connection = new Connection();	
	$programs = $connection->fetchAssocRows("SELECT * FROM media_data WHERE active=true AND media_group='$group'  ORDER BY sequence ASC ");
?>
<form onsubmit="return false" name="videoForm" >	
	<div class="audioOUTER">				
		<a class="orgSite" href="https://www.google.nl"  target="_blank"></a>
		<select name="selectChannel"  id="selectChannel"  >
		<?php foreach ($programs as $song) {  ?>
			<option value="<?php echo (string)$song['source']; ?>"  
					user-title="<?php echo (string)$song['title']; ?>"  
					user-type="mpeg"  
					user-href="<?php echo (string)$song['anchor']; ?>"><?php echo (string)$song['title']; ?></option>	
		<?php } ?>	
		</select>
	</div>		
</form>
<script src="<?php echo(dirname(dirname($_SERVER['PHP_SELF'])));  ?>/cssJsLibs/videoX.js"></script>
<script >
	$(document).ready(function () {		
		getPageSize();
	    updateMobileSize();	
		changeVideoSource();				
	});
</script>	
