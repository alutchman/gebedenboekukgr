<?php 	
	require_once('Connection.php');
	extract($_GET);
	$rubriek = str_replace(' ', '_', $title );
	$connection = new Connection();
	$programs = $connection->fetchAssocRows("SELECT * FROM media_data WHERE active=true AND media_group='$group'  ORDER BY sequence ASC ");	
	if (count($programs) > 0)  {
?>

<div class="container">		
	<iframe class="video" id="<?php echo($rubriek);  ?>Iframe"  src="https://www.youtube.com/embed/RXY2hxCmpJE"  autoplay="autoplay"  
	frameborder="0" 
	allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<div style="display:block;overfkow:hidden;text-align:center;padding:2px;">
	<hr />
	<form name="<?php echo($rubriek);  ?>Form">
      <select name="select<?php echo($rubriek);  ?>">
		<?php		
			foreach($programs as $hoopValue) {				
				$value = $hoopValue['source'].trim();	
				$label = $hoopValue['title'].trim();					
				echo "<option  value=\"$value\">$label</option>";		
			}		
		?>
      </select>	
	</form>
</div>
<script>
	function change<?php echo($rubriek);  ?>(){
		var aform = document.forms['<?php echo($rubriek);  ?>Form'];
		var selectieItem = aform.elements['select<?php echo($rubriek);  ?>'];		
		var newUrl = selectieItem.value.trim() + '?autoplay=1;start=2';
		$("#<?php echo($rubriek);  ?>Iframe").attr("src", newUrl)
	}
		
	$("form[name=<?php echo($rubriek);  ?>Form]").find('select').on('change', function() {
		change<?php echo($rubriek);  ?>();	
	});
	$(document).ready(function(){	
		change<?php echo($rubriek);  ?>();			
	});		
</script>
<?php  } else { ?>
	
	
<?php  }  ?>
