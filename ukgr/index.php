<?php
session_start();

$GLOBALS['bible'] = "https://".$_SERVER['SERVER_NAME']."/Bible/";
$GLOBALS['mainUKGR']  = dirname($_SERVER['PHP_SELF']);    
$GLOBALS['root']   = $_SERVER['DOCUMENT_ROOT'].$GLOBALS['mainUKGR'].'/';
$GLOBALS['htmlRoot']   = 'https://'.$_SERVER['SERVER_NAME'].$GLOBALS['mainUKGR'].'/';
$paths0 =  explode('?',substr($_SERVER['REQUEST_URI'],1+strlen( $GLOBALS['mainUKGR'] )));
$paths1 =  explode('/',$paths0[0]);

$initialExtra = strlen($paths0[0]) > 1 ? implode('/', $paths1) : '';
 
$callURL = $GLOBALS['mainUKGR'].'/'. $initialExtra ;

if(!isset($_SESSION['screen_width']) && !isset($_SESSION['screen_height'])  && 
   !isset($_REQUEST['width']) && !isset($_REQUEST['height'])){
	echo '<script type="text/javascript">window.location = "' . $callURL . '/?width="+screen.width+"&height="+screen.height;</script>';
	exit;   
} else if(isset($_GET['width']) && isset($_GET['height'])) {
	$_SESSION['screen_width'] = $_GET['width'];
	$_SESSION['screen_height'] = $_GET['height'];
	header('Location: ' . $callURL);
	exit;
} 

include('prehtml.php'); 
require_once('includes/Connection.php');	
$connection = new Connection();		
?>
<head>
  <meta charset="UTF-8">
  <title>Wees Gezegend</title>  
  <?php include($GLOBALS['root'].'/cssJsLibs/cssinit.php'); ?> 
</head>
<body>
<?php
	$options = $connection->fetchAssocRows("SELECT * FROM load_items WHERE active=true  ORDER BY sequence ASC");
	$titles  = $connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='TITLE'");
	$intros  = $connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='INTRO'");
	$versesa = $connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='VERS_BOVEN_1'");
	$versesb = $connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='VERS_BOVEN_2'");
	$versesc = $connection->fetchAssocRows("SELECT * FROM pageparts WHERE tag='VERS_ONDER_1'");
?>
<div class="innerBody">
	<h1 ><?php echo($titles[0]['data'] ); ?></h1>

	<article id="main-article1">    
	<?php echo($intros[0]['data'] ); ?>
	<blockquote><?php echo($versesa[0]['data'] ); ?></blockquote> 
	<center>
		<img src="<?php echo dirname($_SERVER['PHP_SELF']); ?>/cssJsLibs/images/holy-spirit-dove-symbol-38.gif" width="50px" align="center"/>
	</center>
	<blockquote><?php echo($versesb[0]['data'] ); ?></blockquote> 
	</article>
	&nbsp; <br />
	<div id="accordion" class="accordion">
		<?php		
		foreach ($options as $tabOption) {	
		?>
			<h3 data-url="includes/loadFromDb.php?group=<?php echo($tabOption['load_group']); ?>&type=<?php echo($tabOption['load_type']); ?>&title=<?php echo($tabOption['title']); ?>"><?php echo($tabOption['title']); ?></h3>
			<div class="containerDrop">Loading...</div>
		<?php  } ?>		
		<h3 data-url="<?php echo($GLOBALS['bible']); ?>">Holy Bible / Bíblia Sagrada</h3>
		<div class="containerDrop">Loading...</div>	
		<h3 data-url="includes/loadContentFromDb.php?tag=CONTEMPLATIE" >Contemplatie</h3>
		<div class="containerDrop">Loading...</div>	
		<h3 data-url="includes/revelationTV.php" style="overflow:hidden;">Revelation TV</h3>
		<div class="containerDrop">Loading...</div>					
	</div>
	&nbsp; <br />	
	<div>
		<blockquote><?php echo($versesc[0]['data'] ); ?></blockquote> 
	</div>
	&nbsp; <br />
	<?php include("footer.php"); ?>
	
	
</div>	

<script>
var mobile = <?php echo $jsMobile; ?>;

$(document).ready(function(){
	  $( function() {
			$( "#accordion" ).accordion({
				active: false,
				heightStyle: "content",
				collapsible: true,
				beforeActivate: function (e, ui) {
					if ($(ui.newHeader[0]) != null) {
						$url = $(ui.newHeader[0]).attr('data-url');
						$(ui.newHeader[0]).next().css({"background" : "transparent", "overflow"  : "hidden", "border": "none", "font-size": "10pt"});
						$.get($url, function (data) {
							$(ui.newHeader[0]).next().html(data);
						});
					}
					if ($(ui.oldHeader[0]) != null) {
						$(ui.oldHeader[0]).next().html("Even geduld... ");
					}
				},
			});
	  });
});

$(window).on('resize', function() {		
	return false;
});
</script>
</body>
</html>

