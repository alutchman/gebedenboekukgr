<?php
	$type = $_GET['type'];
	$CKEditor = $_GET['CKEditor'];
	$funcNum = $_GET['CKEditorFuncNum'];
	$message = '';
	$url = '';
	
	$success = false;
	if(isset($_FILES['upload'])) {   
		$file = $_FILES['upload']['tmp_name']; 
		$file_name = $_FILES['upload']['name']; 
		$extension = pathinfo($_FILES["upload"]["name"], PATHINFO_EXTENSION);
		$new_image_name = rand().'.'.$extension;
		chmod('uploads', 0777);
		$allowed_extension = array("png","jpg","jpeg","gif");
		if(in_array(strtolower($extension),$allowed_extension)){		
			$fileDest = "uploads/$new_image_name";
			move_uploaded_file($file, $fileDest );
			chmod($fileDest, 0755);
			$function_number = $_GET['CKEditorFuncNum'];
			$url = '/'.$fileDest;
			$message= '';
			echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$url','$message');</script> ";
		}		
	}
	
